# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [3.7.7](https://github.com/zurb/tribute/compare/@knowbly/tributejs@3.7.6...@knowbly/tributejs@3.7.7) (2019-08-14)

**Note:** Version bump only for package @knowbly/tributejs





## 3.7.6 (2019-08-12)


### Bug Fixes

* **getTriggerInfo:** handle leading space ([#49](https://github.com/zurb/tribute/issues/49)) ([2ddca16](https://github.com/zurb/tribute/commit/2ddca16))
* **tribute:** only reposition menu when enough space is available ([#177](https://github.com/zurb/tribute/issues/177)) ([25b717d](https://github.com/zurb/tribute/commit/25b717d)), closes [#175](https://github.com/zurb/tribute/issues/175)
* **tribute:** use mouseup events instead of mousedown events ([#176](https://github.com/zurb/tribute/issues/176)) ([e1a88b0](https://github.com/zurb/tribute/commit/e1a88b0)), closes [#172](https://github.com/zurb/tribute/issues/172)
