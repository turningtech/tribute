module.exports = {
    presets: [
        [
            "@babel/preset-env",
            {
                useBuiltIns: false,
            },
        ],
    ],
    plugins: ["add-module-exports"],
};
