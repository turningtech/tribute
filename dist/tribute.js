(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.Tribute = f()}})(function(){var define,module,exports;return (function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _utils = _interopRequireDefault(require("./utils"));

var _TributeEvents = _interopRequireDefault(require("./TributeEvents"));

var _TributeMenuEvents = _interopRequireDefault(require("./TributeMenuEvents"));

var _TributeRange = _interopRequireDefault(require("./TributeRange"));

var _TributeSearch = _interopRequireDefault(require("./TributeSearch"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Tribute = /*#__PURE__*/function () {
  function Tribute(_ref) {
    var _this = this;

    var _ref$values = _ref.values,
        values = _ref$values === void 0 ? null : _ref$values,
        _ref$iframe = _ref.iframe,
        iframe = _ref$iframe === void 0 ? null : _ref$iframe,
        _ref$selectClass = _ref.selectClass,
        selectClass = _ref$selectClass === void 0 ? "highlight" : _ref$selectClass,
        _ref$trigger = _ref.trigger,
        trigger = _ref$trigger === void 0 ? "@" : _ref$trigger,
        _ref$autocompleteMode = _ref.autocompleteMode,
        autocompleteMode = _ref$autocompleteMode === void 0 ? false : _ref$autocompleteMode,
        _ref$selectTemplate = _ref.selectTemplate,
        selectTemplate = _ref$selectTemplate === void 0 ? null : _ref$selectTemplate,
        _ref$menuItemTemplate = _ref.menuItemTemplate,
        menuItemTemplate = _ref$menuItemTemplate === void 0 ? null : _ref$menuItemTemplate,
        _ref$lookup = _ref.lookup,
        lookup = _ref$lookup === void 0 ? "key" : _ref$lookup,
        _ref$fillAttr = _ref.fillAttr,
        fillAttr = _ref$fillAttr === void 0 ? "value" : _ref$fillAttr,
        _ref$collection = _ref.collection,
        collection = _ref$collection === void 0 ? null : _ref$collection,
        _ref$menuContainer = _ref.menuContainer,
        menuContainer = _ref$menuContainer === void 0 ? null : _ref$menuContainer,
        _ref$scrollContainer = _ref.scrollContainer,
        scrollContainer = _ref$scrollContainer === void 0 ? null : _ref$scrollContainer,
        _ref$noMatchTemplate = _ref.noMatchTemplate,
        noMatchTemplate = _ref$noMatchTemplate === void 0 ? null : _ref$noMatchTemplate,
        _ref$headerTemplate = _ref.headerTemplate,
        headerTemplate = _ref$headerTemplate === void 0 ? null : _ref$headerTemplate,
        _ref$requireLeadingSp = _ref.requireLeadingSpace,
        requireLeadingSpace = _ref$requireLeadingSp === void 0 ? true : _ref$requireLeadingSp,
        _ref$allowSpaces = _ref.allowSpaces,
        allowSpaces = _ref$allowSpaces === void 0 ? false : _ref$allowSpaces,
        _ref$replaceTextSuffi = _ref.replaceTextSuffix,
        replaceTextSuffix = _ref$replaceTextSuffi === void 0 ? null : _ref$replaceTextSuffi,
        _ref$positionMenu = _ref.positionMenu,
        positionMenu = _ref$positionMenu === void 0 ? true : _ref$positionMenu,
        _ref$spaceSelectsMatc = _ref.spaceSelectsMatch,
        spaceSelectsMatch = _ref$spaceSelectsMatc === void 0 ? false : _ref$spaceSelectsMatc,
        _ref$selectWithComma = _ref.selectWithComma,
        selectWithComma = _ref$selectWithComma === void 0 ? false : _ref$selectWithComma,
        _ref$searchOpts = _ref.searchOpts,
        searchOpts = _ref$searchOpts === void 0 ? {} : _ref$searchOpts,
        _ref$editor = _ref.editor,
        editor = _ref$editor === void 0 ? {} : _ref$editor,
        _ref$isValidSelection = _ref.isValidSelection,
        isValidSelection = _ref$isValidSelection === void 0 ? null : _ref$isValidSelection,
        _ref$menuItemLimit = _ref.menuItemLimit,
        menuItemLimit = _ref$menuItemLimit === void 0 ? null : _ref$menuItemLimit;

    _classCallCheck(this, Tribute);

    this.autocompleteMode = autocompleteMode;
    this.menuSelected = 0;
    this.current = {};
    this.inputEvent = false;
    this.isActive = false;
    this.menuContainer = menuContainer;
    this.scrollContainer = scrollContainer;
    this.allowSpaces = allowSpaces;
    this.replaceTextSuffix = replaceTextSuffix;
    this.positionMenu = positionMenu;
    this.hasTrailingSpace = false;
    this.spaceSelectsMatch = spaceSelectsMatch;
    this.selectWithComma = selectWithComma;
    this.invalidEvent = document.createEvent("Event");
    this.invalidEvent.initEvent("invalid", true, true);

    if (this.autocompleteMode) {
      trigger = "";
      allowSpaces = false;
    }

    if (values) {
      this.collection = [{
        // symbol that starts the lookup
        trigger: trigger,
        // is it wrapped in an iframe
        iframe: iframe,
        // class applied to selected item
        selectClass: selectClass,
        // function called on select that retuns the content to insert
        selectTemplate: (selectTemplate || Tribute.defaultSelectTemplate).bind(this),
        // function called that returns content for an item
        menuItemTemplate: (menuItemTemplate || Tribute.defaultMenuItemTemplate).bind(this),
        // function called when menu is empty, disables hiding of menu.
        noMatchTemplate: function (t) {
          if (typeof t === "function") {
            return t.bind(_this);
          }

          return noMatchTemplate || function () {
            return "";
          }.bind(_this);
        }(noMatchTemplate),
        headerTemplate: function (t) {
          if (typeof t === "function") {
            return t.bind(_this);
          }

          return headerTemplate || function () {
            return "";
          }.bind(_this);
        }(headerTemplate),
        // column to search against in the object
        lookup: lookup,
        // column that contains the content to insert by default
        fillAttr: fillAttr,
        // array of objects or a function returning an array of objects
        values: values,
        requireLeadingSpace: requireLeadingSpace,
        searchOpts: searchOpts,
        editor: editor,
        selectWithComma: selectWithComma,
        isValidSelection: function (t) {
          if (typeof t === "function") {
            return t.bind(_this);
          }

          return isValidSelection || function () {
            return "";
          }.bind(_this);
        }(isValidSelection),
        menuItemLimit: menuItemLimit
      }];
    } else if (collection) {
      if (this.autocompleteMode) console.warn("Tribute in autocomplete mode does not work for collections");
      this.collection = collection.map(function (item) {
        return {
          trigger: item.trigger || trigger,
          iframe: item.iframe || iframe,
          selectClass: item.selectClass || selectClass,
          selectTemplate: (item.selectTemplate || Tribute.defaultSelectTemplate).bind(_this),
          menuItemTemplate: (item.menuItemTemplate || Tribute.defaultMenuItemTemplate).bind(_this),
          // function called when menu is empty, disables hiding of menu.
          noMatchTemplate: function (t) {
            if (typeof t === "function") {
              return t.bind(_this);
            }

            return null;
          }(noMatchTemplate),
          headerTemplate: function (t) {
            if (typeof t === "function") {
              return t.bind(_this);
            }

            return null;
          }(headerTemplate),
          lookup: item.lookup || lookup,
          fillAttr: item.fillAttr || fillAttr,
          values: item.values,
          requireLeadingSpace: item.requireLeadingSpace,
          searchOpts: item.searchOpts || searchOpts,
          editor: item.editor || editor,
          isValidSelection: function (t) {
            if (typeof t === "function") {
              return t.bind(_this);
            }

            return null;
          }(isValidSelection),
          menuItemLimit: item.menuItemLimit || menuItemLimit
        };
      });
    } else {
      throw new Error("[Tribute] No collection specified.");
    }

    new _TributeRange["default"](this);
    new _TributeEvents["default"](this);
    new _TributeMenuEvents["default"](this);
    new _TributeSearch["default"](this);
  }

  _createClass(Tribute, [{
    key: "triggers",
    value: function triggers() {
      return this.collection.map(function (config) {
        return config.trigger;
      });
    }
  }, {
    key: "attach",
    value: function attach(el, editor) {
      if (!el) {
        throw new Error("[Tribute] Must pass in a DOM node or NodeList.");
      } // Check if it is a jQuery collection


      if (typeof jQuery !== "undefined" && el instanceof window.jQuery) {
        el = el.get();
      } // Is el an Array/Array-like object?


      if (el.constructor === NodeList || el.constructor === HTMLCollection || el.constructor === Array) {
        var length = el.length;

        for (var i = 0; i < length; ++i) {
          this._attach(el[i]);
        }
      } else {
        this._attach(el, editor);
      }
    }
  }, {
    key: "_attach",
    value: function _attach(el, editor) {
      if (el.hasAttribute("data-tribute")) {
        console.warn("Tribute was already bound to " + el.nodeName);
      }

      this.ensureEditable(el);
      this.events.bind(el, editor);

      if (this.scrollContainer) {
        this.scrollContainer.addEventListener("scroll", this.scrollEvent.bind(this));
      }

      el.setAttribute("data-tribute", true);
    }
  }, {
    key: "scrollEvent",
    value: function scrollEvent(e) {
      this.events.scroll(this, e);
    }
  }, {
    key: "ensureEditable",
    value: function ensureEditable(element) {
      if (Tribute.inputTypes().indexOf(element.nodeName) === -1) {
        if (element.contentEditable) {
          element.contentEditable = true;
        } else {
          throw new Error("[Tribute] Cannot bind to " + element.nodeName);
        }
      }
    }
  }, {
    key: "createMenu",
    value: function createMenu() {
      var wrapper = this.range.getDocument().createElement("div"),
          ul = this.range.getDocument().createElement("ul");
      wrapper.className = "tribute-container";
      wrapper.appendChild(ul);

      if (this.menuContainer) {
        return this.menuContainer.appendChild(wrapper);
      }

      return this.range.getDocument().body.appendChild(wrapper);
    }
  }, {
    key: "showMenuFor",
    value: function showMenuFor(element, scrollTo) {
      var _this2 = this;

      // Only proceed if menu isn't already shown for the current element & mentionText
      if (this.isActive && this.current.element === element && this.current.mentionText === this.currentMentionTextSnapshot) {
        return;
      }

      this.currentMentionTextSnapshot = this.current.mentionText; // create the menu if it doesn't exist.

      if (!this.menu) {
        this.menu = this.createMenu();
        element.tributeMenu = this.menu;
        this.menuEvents.bind(this.menu);
      }

      this.isActive = true;
      this.menuSelected = 0;

      if (!this.current.mentionText) {
        this.current.mentionText = "";
      }

      var processValues = function processValues(values, text) {
        // Tribute may not be active any more by the time the value callback returns
        if (!_this2.isActive) {
          return;
        }

        var items = _this2.search.filter(_this2.current.mentionText, values, {
          pre: _this2.current.collection.searchOpts.pre || "<span>",
          post: _this2.current.collection.searchOpts.post || "</span>",
          extract: function extract(el) {
            if (typeof _this2.current.collection.lookup === "string") {
              return el[_this2.current.collection.lookup];
            } else if (typeof _this2.current.collection.lookup === "function") {
              return _this2.current.collection.lookup(el, _this2.current.mentionText);
            } else {
              throw new Error("Invalid lookup attribute, lookup must be string or function.");
            }
          }
        });

        _this2.current.filteredItems = items;

        var ul = _this2.menu.querySelector("ul");

        _this2.range.positionMenuAtCaret(scrollTo);

        if (_this2.current.collection.headerTemplate && _this2.current.collection.headerTemplate(text)) {
          var header = document.createElement("div");
          header.setAttribute("class", "header");
          header.innerHTML = _this2.current.collection.headerTemplate(text);

          var oldHeader = _this2.menu.querySelector(".header");

          if (oldHeader && oldHeader.remove) {
            oldHeader.remove();
          } else if (oldHeader && !oldHeader.remove) {
            oldHeader.parentNode.removeChild(oldHeader);
          }

          _this2.menu.insertBefore(header, _this2.menu.childNodes[0]);
        }

        if (!items.length) {
          var noMatchEvent = new CustomEvent("tribute-no-match", {
            detail: _this2.menu
          });

          _this2.current.element.dispatchEvent(noMatchEvent);

          if (!_this2.current.collection.noMatchTemplate) {
            _this2.hideMenu();
          } else {
            ul.innerHTML = _this2.current.collection.noMatchTemplate();
          }

          return;
        }

        if (_this2.current.collection.menuItemLimit) {
          items = items.slice(0, _this2.current.collection.menuItemLimit);
        }

        ul.innerHTML = "";

        var fragment = _this2.range.getDocument().createDocumentFragment();

        items.forEach(function (item, index) {
          var li = _this2.range.getDocument().createElement("li");

          li.setAttribute("data-index", index);
          li.addEventListener("mousemove", function (e) {
            var li = e.target;
            var index = li.getAttribute("data-index") || li.parentNode && li.parentNode.getAttribute("data-index");

            if (e.movementY !== 0) {
              _this2.events.setActiveLi(index);
            }
          });

          if (_this2.menuSelected === index) {
            li.className = _this2.current.collection.selectClass;
          }

          li.innerHTML = _this2.current.collection.menuItemTemplate(item);
          fragment.appendChild(li);
        });
        ul.appendChild(fragment);
      };

      if (typeof this.current.collection.values === "function") {
        this.current.collection.values(this.current.mentionText, processValues);
      } else {
        processValues(this.current.collection.values, this.current.mentionText);
      }

      this.menu.querySelector("ul").scrollTop = 0;
    }
  }, {
    key: "showMenuForCollection",
    value: function showMenuForCollection(element, collectionIndex) {
      if (element !== document.activeElement) {
        this.placeCaretAtEnd(element);
      }

      this.current.collection = this.collection[collectionIndex || 0];
      this.current.externalTrigger = true;
      this.current.element = element;
      if (element.isContentEditable) this.insertTextAtCursor(this.current.collection.trigger);else this.insertAtCaret(element, this.current.collection.trigger);
      this.showMenuFor(element);
    } // TODO: make sure this works for inputs/textareas

  }, {
    key: "placeCaretAtEnd",
    value: function placeCaretAtEnd(el) {
      el.focus();

      if (typeof window.getSelection != "undefined" && typeof document.createRange != "undefined") {
        var range = document.createRange();
        range.selectNodeContents(el);
        range.collapse(false);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
      } else if (typeof document.body.createTextRange != "undefined") {
        var textRange = document.body.createTextRange();
        textRange.moveToElementText(el);
        textRange.collapse(false);
        textRange.select();
      }
    } // for contenteditable

  }, {
    key: "insertTextAtCursor",
    value: function insertTextAtCursor(text) {
      // eslint-disable-next-line
      var sel, range, html;
      sel = window.getSelection();
      range = sel.getRangeAt(0);
      range.deleteContents();
      var textNode = document.createTextNode(text);
      range.insertNode(textNode);
      range.selectNodeContents(textNode);
      range.collapse(false);
      sel.removeAllRanges();
      sel.addRange(range);
    } // for regular inputs

  }, {
    key: "insertAtCaret",
    value: function insertAtCaret(textarea, text) {
      var scrollPos = textarea.scrollTop;
      var caretPos = textarea.selectionStart;
      var front = textarea.value.substring(0, caretPos);
      var back = textarea.value.substring(textarea.selectionEnd, textarea.value.length);
      textarea.value = front + text + back;
      caretPos = caretPos + text.length;
      textarea.selectionStart = caretPos;
      textarea.selectionEnd = caretPos;
      textarea.focus();
      textarea.scrollTop = scrollPos;
    }
  }, {
    key: "hideMenu",
    value: function hideMenu() {
      if (this.menu) {
        this.menu.style.cssText = "display: none;";
        this.isActive = false;
        this.menuSelected = 0;
        this.current = {};
      }
    }
  }, {
    key: "selectItemAtIndex",
    value: function selectItemAtIndex(index, originalEvent) {
      index = parseInt(index);
      if (typeof index !== "number" || isNaN(index)) return;
      var item = this.current.filteredItems[index];

      if (typeof this.current.collection.isValidSelection === "function" && this.current.collection.isValidSelection(item, this.current.collection.editor) === false) {
        this.current.collection.editor.el.dispatchEvent(this.invalidEvent);
        return;
      }

      var content = this.current.collection.selectTemplate(item);
      if (content !== null) this.replaceText(content, originalEvent, item);
    }
  }, {
    key: "replaceText",
    value: function replaceText(content, originalEvent, item) {
      this.range.replaceTriggerText(content, true, true, originalEvent, item);
    }
  }, {
    key: "_append",
    value: function _append(collection, newValues, replace) {
      if (typeof collection.values === "function") {
        throw new Error("Unable to append to values, as it is a function.");
      } else if (!replace) {
        collection.values = collection.values.concat(newValues);
      } else {
        collection.values = newValues;
      }
    }
  }, {
    key: "append",
    value: function append(collectionIndex, newValues, replace) {
      var index = parseInt(collectionIndex);
      if (typeof index !== "number") throw new Error("please provide an index for the collection to update.");
      var collection = this.collection[index];

      this._append(collection, newValues, replace);
    }
  }, {
    key: "appendCurrent",
    value: function appendCurrent(newValues, replace) {
      if (this.isActive) {
        this._append(this.current.collection, newValues, replace);
      } else {
        throw new Error("No active state. Please use append instead and pass an index.");
      }
    }
  }, {
    key: "detach",
    value: function detach(el) {
      if (!el) {
        throw new Error("[Tribute] Must pass in a DOM node or NodeList.");
      } // Check if it is a jQuery collection


      if (typeof jQuery !== "undefined" && el instanceof window.jQuery) {
        el = el.get();
      } // Is el an Array/Array-like object?


      if (el.constructor === NodeList || el.constructor === HTMLCollection || el.constructor === Array) {
        var length = el.length;

        for (var i = 0; i < length; ++i) {
          this._detach(el[i]);
        }
      } else {
        this._detach(el);
      }
    }
  }, {
    key: "_detach",
    value: function _detach(el) {
      var _this3 = this;

      this.events.unbind(el);

      if (el.tributeMenu) {
        this.menuEvents.unbind(el.tributeMenu);
      }

      if (this.scrollContainer) {
        this.scrollContainer.removeEventListener("scroll", this.scrollEvent);
      }

      setTimeout(function () {
        el.removeAttribute("data-tribute");
        _this3.isActive = false;

        if (el.tributeMenu) {
          el.tributeMenu.remove();
        }
      });
    }
  }], [{
    key: "defaultSelectTemplate",
    value: function defaultSelectTemplate(item) {
      if (typeof item === "undefined") return null;

      if (this.range.isContentEditable(this.current.element)) {
        return '<span class="tribute-mention">' + (this.current.collection.trigger + item.original[this.current.collection.fillAttr]) + "</span>";
      }

      return this.current.collection.trigger + item.original[this.current.collection.fillAttr];
    }
  }, {
    key: "defaultMenuItemTemplate",
    value: function defaultMenuItemTemplate(matchItem) {
      return matchItem.string;
    }
  }, {
    key: "inputTypes",
    value: function inputTypes() {
      return ["TEXTAREA", "INPUT"];
    }
  }]);

  return Tribute;
}();

var _default = Tribute;
exports["default"] = _default;
module.exports = exports.default;

},{"./TributeEvents":2,"./TributeMenuEvents":3,"./TributeRange":4,"./TributeSearch":5,"./utils":7}],2:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var TributeEvents = /*#__PURE__*/function () {
  function TributeEvents(tribute) {
    _classCallCheck(this, TributeEvents);

    this.tribute = tribute;
    this.tribute.events = this;
  }

  _createClass(TributeEvents, [{
    key: "bind",
    value: function bind(element, editor) {
      element.boundKeydown = this.keydown.bind(element, this, editor);
      element.boundKeyup = this.keyup.bind(element, this, editor);
      element.boundInput = this.input.bind(element, this, editor);
      element.addEventListener("keydown", element.boundKeydown, false);
      element.addEventListener("keyup", element.boundKeyup, false);
      element.addEventListener("input", element.boundInput, false);
    }
  }, {
    key: "unbind",
    value: function unbind(element) {
      element.removeEventListener("keydown", element.boundKeydown, false);
      element.removeEventListener("keyup", element.boundKeyup, false);
      element.removeEventListener("input", element.boundInput, false);
      delete element.boundKeydown;
      delete element.boundKeyup;
      delete element.boundInput;
    } // eslint-disable-next-line

  }, {
    key: "scroll",
    value: function scroll(instance, e) {
      instance.isActive = false;
      instance.hideMenu();
    }
  }, {
    key: "keydown",
    value: function keydown(instance, editor, event) {
      if (instance.tribute.isActive && [16, 17, 18, 20].includes(event.keyCode)) {
        return;
      }

      if (instance.shouldDeactivate(event)) {
        instance.tribute.isActive = false;
        instance.tribute.hideMenu();
      }

      var element = this;
      instance.commandEvent = false;
      TributeEvents.keys().forEach(function (o) {
        if (o.key === event.keyCode) {
          instance.commandEvent = true;
          instance.callbacks()[o.value.toLowerCase()](event, element, editor);
        }
      });
    }
  }, {
    key: "input",
    value: function input(instance, event, editor) {
      instance.inputEvent = true;
      instance.keyup.call(this, instance, event, editor);
    }
  }, {
    key: "click",
    value: function click(instance, event) {
      var tribute = instance.tribute;

      if (tribute.menu && tribute.menu.contains(event.target)) {
        event.preventDefault();
        event.stopPropagation();

        if (event.target.getAttribute("class") === "header" || event.target.tagName === "UL") {
          return;
        }

        var li = event.target;

        while (li.nodeName.toLowerCase() !== "li") {
          li = li.parentNode;

          if (!li || li === tribute.menu) {
            throw new Error("cannot find the <li> container for the click");
          }
        }

        tribute.selectItemAtIndex(li.getAttribute("data-index"), event);
        tribute.hideMenu(); // TODO: should fire with externalTrigger and target is outside of menu
      } else if (tribute.current.element && !tribute.current.externalTrigger) {
        tribute.current.externalTrigger = false;
        setTimeout(function () {
          return tribute.hideMenu();
        });
      }
    }
  }, {
    key: "keyup",
    value: function keyup(instance, editor, event) {
      if (instance.tribute.isActive && [16, 17, 18, 20].includes(event.keyCode)) {
        return;
      }

      if (instance.inputEvent) {
        instance.inputEvent = false;
      }

      instance.updateSelection(this);
      if (event.keyCode === 27) return;

      if (editor && editor.charCounter && editor.charCounter.count() === 0) {
        instance.tribute.isActive = false;
        instance.tribute.hideMenu();
        return;
      }

      if (!instance.tribute.allowSpaces && instance.tribute.hasTrailingSpace) {
        instance.tribute.hasTrailingSpace = false;
        instance.commandEvent = true;
        instance.callbacks()["space"](event, this);
        return;
      }

      if (!instance.tribute.isActive) {
        if (instance.tribute.autocompleteMode) {
          instance.callbacks().triggerChar(event, this, "");
        } else {
          var keyCode = instance.getKeyCode(instance, this, event);
          if (isNaN(keyCode) || !keyCode) return;
          var trigger = instance.tribute.triggers().find(function (trigger) {
            return trigger.charCodeAt(0) === keyCode;
          });

          if (typeof trigger !== "undefined") {
            instance.callbacks().triggerChar(event, this, trigger, true);
          }
        }
      }

      if ((instance.tribute.current.trigger || instance.tribute.autocompleteMode) && instance.commandEvent === false || instance.tribute.isActive && [8, 46].includes(event.keyCode)) {
        if (event.keyCode === 81) {
          var text = instance.tribute.range.getTextPrecedingCurrentSelection();

          if (text.length > 1) {
            instance.tribute.hideMenu();
            return;
          } else {
            instance.tribute.showMenuFor(this, true);
          }
        } else {
          instance.tribute.showMenuFor(this, true);
        }
      }
    }
  }, {
    key: "shouldDeactivate",
    value: function shouldDeactivate(event) {
      if (!this.tribute.isActive) return false;

      if (this.tribute.current.mentionText.length === 0) {
        var eventKeyPressed = false;
        TributeEvents.keys().forEach(function (o) {
          if (event.keyCode === o.key) eventKeyPressed = true;
        });
        return !eventKeyPressed;
      }

      return false;
    } // eslint-disable-next-line

  }, {
    key: "getKeyCode",
    value: function getKeyCode(instance, el, event) {
      // eslint-disable-next-line
      var _char;

      var tribute = instance.tribute;
      var info = tribute.range.getTriggerInfo(false, tribute.hasTrailingSpace, true, tribute.allowSpaces, tribute.autocompleteMode);

      if (info) {
        return info.mentionTriggerChar.charCodeAt(0);
      } else {
        return false;
      }
    }
  }, {
    key: "updateSelection",
    value: function updateSelection(el) {
      this.tribute.current.element = el;
      var info = this.tribute.range.getTriggerInfo(false, this.tribute.hasTrailingSpace, true, this.tribute.allowSpaces, this.tribute.autocompleteMode);

      if (info) {
        this.tribute.current.selectedPath = info.mentionSelectedPath;
        this.tribute.current.mentionText = info.mentionText;
        this.tribute.current.selectedOffset = info.mentionSelectedOffset;
      }
    }
  }, {
    key: "callbacks",
    value: function callbacks() {
      var _this = this;

      return {
        triggerChar: function triggerChar(e, el, trigger) {
          var showMenu = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;

          var text = _this.tribute.range.getTextPrecedingCurrentSelection();

          var words = text.split(/\s/);
          var lastWord = words[words.length - 1];

          if (lastWord.split("@").length - 1 > 1) {
            return;
          }

          if (lastWord.trim()[0] !== "@") {
            return;
          }

          var tribute = _this.tribute;
          tribute.current.trigger = trigger;
          var collectionItem = tribute.collection.find(function (item) {
            return item.trigger === trigger;
          });
          tribute.current.collection = collectionItem;
          if (tribute.inputEvent || showMenu) tribute.showMenuFor(el, true);
        },
        // eslint-disable-next-line
        enter: function enter(e, el) {
          // choose selection
          if (_this.tribute.isActive && _this.tribute.current.filteredItems) {
            e.preventDefault();
            e.stopPropagation();
            setTimeout(function () {
              _this.tribute.selectItemAtIndex(_this.tribute.menuSelected, e);

              _this.tribute.hideMenu();
            }, 0);
          }
        },
        comma: function comma(e, el) {
          if (_this.tribute.isActive) {
            if (_this.tribute.selectWithComma) {
              _this.callbacks().enter(e, el);
            }
          }
        },
        // eslint-disable-next-line
        escape: function escape(e, el) {
          if (_this.tribute.isActive) {
            e.preventDefault();
            e.stopPropagation();
            _this.tribute.isActive = false;

            _this.tribute.hideMenu();
          }
        },
        tab: function tab(e, el) {
          // choose first match
          _this.callbacks().enter(e, el);
        },
        // eslint-disable-next-line
        space: function space(e, el, editor) {
          if (_this.tribute.isActive) {
            if (_this.tribute.spaceSelectsMatch) {
              _this.callbacks().enter(e, el);
            } else if (!_this.tribute.allowSpaces) {
              e.stopPropagation();
              setTimeout(function () {
                _this.tribute.hideMenu();

                _this.tribute.isActive = false;
              }, 0);
            }
          }

          var text = _this.tribute.range.getTextPrecedingCurrentSelection().trim();

          if (text.lastIndexOf(_this.tribute.current.trigger) === text.length - 1) {
            e.stopPropagation();
            setTimeout(function () {
              _this.tribute.hideMenu();

              _this.tribute.isActive = false;
            }, 0);
          }
        },
        // eslint-disable-next-line
        up: function up(e, el) {
          // navigate up ul
          if (_this.tribute.isActive && _this.tribute.current.filteredItems) {
            e.preventDefault();
            e.stopPropagation();
            var count = _this.tribute.current.filteredItems.length,
                selected = _this.tribute.menuSelected;

            if (count > selected && selected > 0) {
              _this.tribute.menuSelected--;

              _this.setActiveLi();
            } else if (selected === 0) {
              _this.tribute.menuSelected = count - 1;

              _this.setActiveLi();

              _this.tribute.menu.querySelector("ul").scrollTop = _this.tribute.menu.querySelector("ul").offsetHeight;
            }
          }
        },
        // eslint-disable-next-line
        down: function down(e, el) {
          // navigate down ul
          if (_this.tribute.isActive && _this.tribute.current.filteredItems) {
            e.preventDefault();
            e.stopPropagation();
            var count = _this.tribute.current.filteredItems.length - 1,
                selected = _this.tribute.menuSelected;

            if (count > selected) {
              _this.tribute.menuSelected++;

              _this.setActiveLi();
            } else if (count === selected) {
              _this.tribute.menuSelected = 0;

              _this.setActiveLi();

              _this.tribute.menu.querySelector("ul").scrollTop = 0;
            }
          }
        },
        "delete": function _delete(e, el) {
          if (_this.tribute.isActive && _this.tribute.current.mentionText.length < 1) {
            _this.tribute.hideMenu();
          } else if (_this.tribute.isActive) {
            _this.tribute.showMenuFor(el);
          } else if (!_this.tribute.isActive) {
            var text = _this.tribute.range.getTextPrecedingCurrentSelection();

            var words = text.split(" ");

            if (words[words.length - 1].split("@").length - 1 > 1) {
              return;
            }

            if (words[words.length - 1].trim()[0] !== "@") {
              return;
            }

            _this.tribute.inputEvent = true;

            _this.callbacks().triggerChar(e, el, _this.tribute.current.trigger || "@", true);
          }
        }
      };
    }
  }, {
    key: "setActiveLi",
    value: function setActiveLi(index) {
      var lis = this.tribute.menu.querySelectorAll("li"),
          length = lis.length >>> 0;
      if (index) this.tribute.menuSelected = parseInt(index);

      for (var i = 0; i < length; i++) {
        var li = lis[i];

        if (i === this.tribute.menuSelected) {
          li.classList.add(this.tribute.current.collection.selectClass);
          var liClientRect = li.getBoundingClientRect();
          var menuClientRect = this.tribute.menu.querySelector("ul").getBoundingClientRect();

          if (liClientRect.bottom > menuClientRect.bottom) {
            var scrollDistance = liClientRect.bottom - menuClientRect.bottom;
            this.tribute.menu.querySelector("ul").scrollTop += scrollDistance;
          } else if (liClientRect.top < menuClientRect.top) {
            var _scrollDistance = menuClientRect.top - liClientRect.top;

            this.tribute.menu.querySelector("ul").scrollTop -= _scrollDistance;
          }
        } else {
          li.classList.remove(this.tribute.current.collection.selectClass);
        }
      }
    }
  }, {
    key: "getFullHeight",
    value: function getFullHeight(elem, includeMargin) {
      var height = elem.getBoundingClientRect().height;

      if (includeMargin) {
        var style = elem.currentStyle || window.getComputedStyle(elem);
        return height + parseFloat(style.marginTop) + parseFloat(style.marginBottom);
      }

      return height;
    }
  }], [{
    key: "keys",
    value: function keys() {
      return [{
        key: 9,
        value: "TAB"
      }, {
        key: 8,
        value: "DELETE"
      }, {
        key: 13,
        value: "ENTER"
      }, {
        key: 27,
        value: "ESCAPE"
      }, {
        key: 32,
        value: "SPACE"
      }, {
        key: 38,
        value: "UP"
      }, {
        key: 40,
        value: "DOWN"
      }, {
        key: 188,
        value: "COMMA"
      }];
    }
  }, {
    key: "remove",
    value: function remove(elem) {
      if (elem && elem.remove) {
        elem.remove();
        return;
      }

      if (elem && !elem.remove) {
        elem.parentNode.removeChild(elem);
      }
    }
  }]);

  return TributeEvents;
}();

var _default = TributeEvents;
exports["default"] = _default;
module.exports = exports.default;

},{}],3:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var TributeMenuEvents = /*#__PURE__*/function () {
  function TributeMenuEvents(tribute) {
    _classCallCheck(this, TributeMenuEvents);

    this.tribute = tribute;
    this.tribute.menuEvents = this;
    this.menu = this.tribute.menu;
  } // eslint-disable-next-line


  _createClass(TributeMenuEvents, [{
    key: "bind",
    value: function bind(menu) {
      var _this = this;

      this.menuClickEvent = this.tribute.events.click.bind(null, this);
      this.menuContainerScrollEvent = this.debounce(function () {
        if (_this.tribute.isActive) {
          _this.tribute.showMenuFor(_this.tribute.current.element, false);
        }
      }, 300, false);
      this.windowResizeEvent = this.debounce(function () {
        if (_this.tribute.isActive) {
          _this.tribute.range.positionMenuAtCaret(true);
        }
      }, 300, false); // fixes IE11 issues with mousedown

      this.tribute.range.getDocument().addEventListener("MSPointerDown", this.menuClickEvent, false);
      this.tribute.range.getDocument().addEventListener("mousedown", this.menuClickEvent, false);
      window.addEventListener("resize", this.windowResizeEvent);

      if (this.menuContainer) {
        this.menuContainer.addEventListener("scroll", this.menuContainerScrollEvent, false);
      } else {
        window.addEventListener("scroll", this.menuContainerScrollEvent);
      }
    } // eslint-disable-next-line

  }, {
    key: "unbind",
    value: function unbind(menu) {
      this.tribute.range.getDocument().removeEventListener("mousedown", this.menuClickEvent, false);
      this.tribute.range.getDocument().removeEventListener("MSPointerDown", this.menuClickEvent, false);
      window.removeEventListener("resize", this.windowResizeEvent);

      if (this.menuContainer) {
        this.menuContainer.removeEventListener("scroll", this.menuContainerScrollEvent, false);
      } else {
        window.removeEventListener("scroll", this.menuContainerScrollEvent);
      }
    }
  }, {
    key: "debounce",
    value: function debounce(func, wait, immediate) {
      var _arguments = arguments,
          _this2 = this;

      var timeout;
      return function () {
        var context = _this2,
            args = _arguments;

        var later = function later() {
          timeout = null;
          if (!immediate) func.apply(context, args);
        };

        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
      };
    }
  }]);

  return TributeMenuEvents;
}();

var _default = TributeMenuEvents;
exports["default"] = _default;
module.exports = exports.default;

},{}],4:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

// Thanks to https://github.com/jeff-collins/ment.io
var TributeRange = /*#__PURE__*/function () {
  function TributeRange(tribute) {
    _classCallCheck(this, TributeRange);

    this.tribute = tribute;
    this.tribute.range = this;
  }

  _createClass(TributeRange, [{
    key: "getDocument",
    value: function getDocument() {
      var iframe;

      if (this.tribute.current.collection) {
        iframe = this.tribute.current.collection.iframe;
      }

      if (!iframe) {
        return document;
      }

      return iframe.contentWindow.document;
    }
  }, {
    key: "positionMenuAtCaret",
    value: function positionMenuAtCaret(scrollTo) {
      var _this = this;

      var context = this.tribute.current,
          coordinates;
      var info = this.getTriggerInfo(false, this.tribute.hasTrailingSpace, true, this.tribute.allowSpaces, this.tribute.autocompleteMode);

      if (typeof info !== "undefined") {
        if (!this.tribute.positionMenu) {
          this.tribute.menu.style.cssText = "display: block;";
          return;
        }

        if (!this.isContentEditable(context.element)) {
          coordinates = this.getTextAreaOrInputUnderlinePosition(this.tribute.current.element, info.mentionPosition);
        } else {
          coordinates = this.getContentEditableCaretPosition(info.mentionPosition);
        }

        this.tribute.menu.style.cssText = "top: ".concat(coordinates.top, "px;\n                                     left: ").concat(coordinates.left, "px;\n                                     right: ").concat(coordinates.right, "px;\n                                     bottom: ").concat(coordinates.bottom, "px;\n                                     position: absolute;\n                                     z-index: 10000;\n                                     display: block;");

        if (coordinates.left === "auto") {
          this.tribute.menu.style.left = "auto";
        }

        if (coordinates.top === "auto") {
          this.tribute.menu.style.top = "auto";
        }

        if (scrollTo) this.scrollIntoView();
        window.setTimeout(function () {
          var menuDimensions = {
            width: _this.tribute.menu.offsetWidth,
            height: _this.tribute.menu.offsetHeight
          };

          var menuIsOffScreen = _this.isMenuOffScreen(coordinates, menuDimensions);

          var menuIsOffScreenHorizontally = window.innerWidth > menuDimensions.width && (menuIsOffScreen.left || menuIsOffScreen.right);
          var menuIsOffScreenVertically = window.innerHeight > menuDimensions.height && (menuIsOffScreen.top || menuIsOffScreen.bottom);

          if (menuIsOffScreenHorizontally || menuIsOffScreenVertically) {
            _this.tribute.menu.style.cssText = "display: none";

            _this.positionMenuAtCaret(scrollTo);
          }
        }, 0);
      } else {
        this.tribute.menu.style.cssText = "display: none";
      }
    }
  }, {
    key: "selectElement",
    value: function selectElement(targetElement, path, offset) {
      var range;
      var elem = targetElement;

      if (path) {
        for (var i = 0; i < path.length; i++) {
          elem = elem.childNodes[path[i]];

          if (elem === undefined) {
            return;
          }

          while (elem.length < offset) {
            offset -= elem.length;
            elem = elem.nextSibling;
          }

          if (elem.childNodes.length === 0 && !elem.length) {
            elem = elem.previousSibling;
          }
        }
      }

      var sel = this.getWindowSelection();
      range = this.getDocument().createRange();
      range.setStart(elem, offset);
      range.setEnd(elem, offset);
      range.collapse(true);

      try {
        sel.removeAllRanges(); // eslint-disable-next-line
      } catch (error) {}

      sel.addRange(range);
      targetElement.focus();
    }
  }, {
    key: "replaceTriggerText",
    value: function replaceTriggerText(text, requireLeadingSpace, hasTrailingSpace, originalEvent, item) {
      var info = this.getTriggerInfo(true, hasTrailingSpace, requireLeadingSpace, this.tribute.allowSpaces, this.tribute.autocompleteMode);

      if (info !== undefined) {
        var context = this.tribute.current;
        var replaceEvent = new CustomEvent("tribute-replaced", {
          detail: {
            item: item,
            instance: context,
            context: info,
            event: originalEvent
          }
        });

        if (!this.isContentEditable(context.element)) {
          var myField = this.tribute.current.element;
          var textSuffix = typeof this.tribute.replaceTextSuffix == "string" ? this.tribute.replaceTextSuffix : " ";
          text += textSuffix;
          var startPos = info.mentionPosition;
          var endPos = info.mentionPosition + info.mentionText.length + textSuffix.length;
          myField.value = myField.value.substring(0, startPos) + text + myField.value.substring(endPos, myField.value.length);
          myField.selectionStart = startPos + text.length;
          myField.selectionEnd = startPos + text.length;
        } else {
          // add a space to the end of the pasted text
          var _textSuffix = typeof this.tribute.replaceTextSuffix == "string" ? this.tribute.replaceTextSuffix : "\xA0";

          if (originalEvent.keyCode === 188) {
            _textSuffix = "," + _textSuffix;
          }

          text += _textSuffix;
          this.pasteHtml(text, info.mentionPosition, info.mentionPosition + info.mentionText.length + !this.tribute.autocompleteMode);
        }

        context.element.dispatchEvent(replaceEvent);
      }
    }
  }, {
    key: "pasteHtml",
    value: function pasteHtml(html, startPos, endPos) {
      var range, sel, selEl;
      sel = this.getWindowSelection();
      var _sel = sel,
          anchorOffset = _sel.anchorOffset,
          anchorNode = _sel.anchorNode;

      if (anchorNode.childNodes && anchorNode.childNodes[anchorOffset]) {
        var child = anchorNode.childNodes[anchorOffset];

        if (child.nodeType === Node.TEXT_NODE) {
          selEl = child;
        } else if (anchorOffset > 0 && child.nodeType === Node.ELEMENT_NODE) {
          selEl = anchorNode.childNodes[anchorOffset - 1];
        }
      } else {
        selEl = sel.anchorNode;
      }

      range = this.getDocument().createRange();
      range.setStart(selEl, startPos);
      range.setEnd(selEl, endPos);
      range.deleteContents();
      var el = this.getDocument().createElement("div");
      el.innerHTML = html;
      var frag = this.getDocument().createDocumentFragment(),
          node,
          lastNode;

      while (node = el.firstChild) {
        lastNode = frag.appendChild(node);
      }

      range.insertNode(frag); // Preserve the selection

      if (lastNode) {
        range = range.cloneRange();
        range.setStartAfter(lastNode);
        range.collapse(true);
        sel.removeAllRanges();
        sel.addRange(range);
      }
    }
  }, {
    key: "getWindowSelection",
    value: function getWindowSelection() {
      if (this.tribute.collection.iframe) {
        return this.tribute.collection.iframe.contentWindow.getSelection();
      }

      return window.getSelection();
    }
  }, {
    key: "getNodePositionInParent",
    value: function getNodePositionInParent(element) {
      if (element.parentNode === null) {
        return 0;
      }

      for (var i = 0; i < element.parentNode.childNodes.length; i++) {
        var node = element.parentNode.childNodes[i];

        if (node === element) {
          return i;
        }
      }
    } // eslint-disable-next-line

  }, {
    key: "getContentEditableSelectedPath",
    value: function getContentEditableSelectedPath(ctx) {
      var sel = this.getWindowSelection();
      var selected = sel.anchorNode;
      var path = [];
      var offset;

      if (selected != null) {
        var i;
        var ce = selected.contentEditable;

        while (selected !== null && ce !== "true") {
          i = this.getNodePositionInParent(selected);
          path.push(i);
          selected = selected.parentNode;

          if (selected !== null) {
            ce = selected.contentEditable;
          }
        }

        path.reverse(); // getRangeAt may not exist, need alternative

        offset = sel.getRangeAt(0).startOffset;
        return {
          selected: selected,
          path: path,
          offset: offset
        };
      }
    }
  }, {
    key: "getTextPrecedingCurrentSelection",
    value: function getTextPrecedingCurrentSelection() {
      var context = this.tribute.current,
          text = "";

      if (!this.isContentEditable(context.element)) {
        var textComponent = this.tribute.current.element;

        if (textComponent) {
          var startPos = textComponent.selectionStart;

          if (textComponent.value && startPos >= 0) {
            text = textComponent.value.substring(0, startPos);
          }
        }
      } else {
        var selectedElem = this.getWindowSelection().anchorNode;

        if (selectedElem != null) {
          var anchorOffset = this.getWindowSelection().anchorOffset;

          if (selectedElem.childNodes && selectedElem.childNodes[anchorOffset]) {
            var child = selectedElem.childNodes[anchorOffset];

            if (child.nodeType === Node.TEXT_NODE) {
              text = child.textContent;
            } else if (anchorOffset > 0 && child.nodeType === Node.ELEMENT_NODE) {
              text += selectedElem.childNodes[anchorOffset - 1].textContent;
            }
          } else {
            var workingNodeContent = selectedElem.textContent;
            var selectStartOffset = this.getWindowSelection().getRangeAt(0).startOffset;

            if (workingNodeContent && selectStartOffset >= 0) {
              text = workingNodeContent.substring(0, selectStartOffset);
            }
          }
        }
      }

      return text;
    }
  }, {
    key: "getLastWordInText",
    value: function getLastWordInText(text) {
      text = text.replace(/\u00A0/g, " "); // https://stackoverflow.com/questions/29850407/how-do-i-replace-unicode-character-u00a0-with-a-space-in-javascript

      var wordsArray = text.split(" ");
      var worldsCount = wordsArray.length - 1;
      return wordsArray[worldsCount].trim();
    }
  }, {
    key: "getTriggerInfo",
    value: function getTriggerInfo(menuAlreadyActive, hasTrailingSpace, requireLeadingSpace, allowSpaces, isAutocomplete) {
      var _this2 = this;

      var ctx = this.tribute.current;
      var selected, path, offset;

      if (!this.isContentEditable(ctx.element)) {
        selected = this.tribute.current.element;
      } else {
        var selectionInfo = this.getContentEditableSelectedPath(ctx);

        if (selectionInfo) {
          selected = selectionInfo.selected;
          path = selectionInfo.path;
          offset = selectionInfo.offset;
        }
      }

      var effectiveRange = this.getTextPrecedingCurrentSelection();
      var lastWordOfEffectiveRange = this.getLastWordInText(effectiveRange);

      if (isAutocomplete) {
        return {
          mentionPosition: effectiveRange.length - lastWordOfEffectiveRange.length,
          mentionText: lastWordOfEffectiveRange,
          mentionSelectedElement: selected,
          mentionSelectedPath: path,
          mentionSelectedOffset: offset
        };
      }

      if (effectiveRange !== undefined && effectiveRange !== null) {
        var mostRecentTriggerCharPos = -1;
        var triggerChar;
        this.tribute.collection.forEach(function (config) {
          var c = config.trigger;
          var idx = config.requireLeadingSpace ? _this2.lastIndexWithLeadingSpace(effectiveRange, c) : effectiveRange.lastIndexOf(c);

          if (idx > mostRecentTriggerCharPos) {
            mostRecentTriggerCharPos = idx;
            triggerChar = c;
            requireLeadingSpace = config.requireLeadingSpace;
          }
        });

        if (mostRecentTriggerCharPos >= 0 && (mostRecentTriggerCharPos === 0 || !requireLeadingSpace || /[\xA0\s]/g.test(effectiveRange.substring(mostRecentTriggerCharPos - 1, mostRecentTriggerCharPos)))) {
          var currentTriggerSnippet = effectiveRange.substring(mostRecentTriggerCharPos + 1, effectiveRange.length);
          triggerChar = effectiveRange.substring(mostRecentTriggerCharPos, mostRecentTriggerCharPos + 1);
          var firstSnippetChar = currentTriggerSnippet.substring(0, 1);
          var leadingSpace = currentTriggerSnippet.length > 0 && (firstSnippetChar === " " || firstSnippetChar === "\xA0");

          if (hasTrailingSpace) {
            currentTriggerSnippet = currentTriggerSnippet.trim();
          }

          var regex = allowSpaces ? /[^\S ]/g : /[\xA0\s]/g;
          this.tribute.hasTrailingSpace = regex.test(currentTriggerSnippet);

          if (!leadingSpace && (menuAlreadyActive || !regex.test(currentTriggerSnippet))) {
            return {
              mentionPosition: mostRecentTriggerCharPos,
              mentionText: currentTriggerSnippet,
              mentionSelectedElement: selected,
              mentionSelectedPath: path,
              mentionSelectedOffset: offset,
              mentionTriggerChar: triggerChar
            };
          }
        }
      }
    }
  }, {
    key: "lastIndexWithLeadingSpace",
    value: function lastIndexWithLeadingSpace(str, _char) {
      var reversedStr = str.split("").reverse().join("");
      var index = -1;

      for (var cidx = 0, len = str.length; cidx < len; cidx++) {
        var firstChar = cidx === str.length - 1;
        var leadingSpace = /\s/.test(reversedStr[cidx + 1]);
        var match = _char === reversedStr[cidx];

        if (match && (firstChar || leadingSpace)) {
          index = str.length - 1 - cidx;
          break;
        }
      }

      return index;
    }
  }, {
    key: "isContentEditable",
    value: function isContentEditable(element) {
      if (!element) {
        return false;
      }

      return element.nodeName !== "INPUT" && element.nodeName !== "TEXTAREA";
    }
  }, {
    key: "isMenuOffScreen",
    value: function isMenuOffScreen(coordinates, menuDimensions) {
      var windowWidth = window.innerWidth;
      var windowHeight = window.innerHeight;
      var doc = document.documentElement;
      var windowLeft = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
      var windowTop = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
      var menuTop = typeof coordinates.top === "number" ? coordinates.top : windowTop + windowHeight - coordinates.bottom - menuDimensions.height;
      var menuRight = typeof coordinates.right === "number" ? coordinates.right : coordinates.left + menuDimensions.width;
      var menuBottom = typeof coordinates.bottom === "number" ? coordinates.bottom : coordinates.top + menuDimensions.height;
      var menuLeft = typeof coordinates.left === "number" ? coordinates.left : windowLeft + windowWidth - coordinates.right - menuDimensions.width;
      return {
        top: menuTop < Math.floor(windowTop),
        right: menuRight > Math.ceil(windowLeft + windowWidth),
        bottom: menuBottom > Math.ceil(windowTop + windowHeight),
        left: menuLeft < Math.floor(windowLeft)
      };
    }
  }, {
    key: "getMenuDimensions",
    value: function getMenuDimensions() {
      // Width of the menu depends of its contents and position
      // We must check what its width would be without any obstruction
      // This way, we can achieve good positioning for flipping the menu
      var dimensions = {
        width: null,
        height: null
      };
      this.tribute.menu.style.cssText = "top: 0px;\n                                 left: 0px;\n                                 position: fixed;\n                                 zIndex: 10000;\n                                 display: block;\n                                 visibility; hidden;";
      dimensions.width = this.tribute.menu.offsetWidth;
      dimensions.height = this.tribute.menu.offsetHeight;
      this.tribute.menu.style.cssText = "display: none;";
      return dimensions;
    } // eslint-disable-next-line

  }, {
    key: "getTextAreaOrInputUnderlinePosition",
    value: function getTextAreaOrInputUnderlinePosition(element, position, flipped) {
      var properties = ["direction", "boxSizing", "width", "height", "overflowX", "overflowY", "borderTopWidth", "borderRightWidth", "borderBottomWidth", "borderLeftWidth", "paddingTop", "paddingRight", "paddingBottom", "paddingLeft", "fontStyle", "fontVariant", "fontWeight", "fontStretch", "fontSize", "fontSizeAdjust", "lineHeight", "fontFamily", "textAlign", "textTransform", "textIndent", "textDecoration", "letterSpacing", "wordSpacing"];
      var isFirefox = window.mozInnerScreenX !== null;
      var div = this.getDocument().createElement("div");
      div.id = "input-textarea-caret-position-mirror-div";
      this.getDocument().body.appendChild(div);
      var style = div.style;
      var computed = window.getComputedStyle ? getComputedStyle(element) : element.currentStyle;
      style.whiteSpace = "pre-wrap";

      if (element.nodeName !== "INPUT") {
        style.wordWrap = "break-word";
      } // position off-screen


      style.position = "absolute";
      style.visibility = "hidden"; // transfer the element's properties to the div

      properties.forEach(function (prop) {
        style[prop] = computed[prop];
      });

      if (isFirefox) {
        style.width = "".concat(parseInt(computed.width) - 2, "px");
        if (element.scrollHeight > parseInt(computed.height)) style.overflowY = "scroll";
      } else {
        style.overflow = "hidden";
      }

      div.textContent = element.value.substring(0, position);

      if (element.nodeName === "INPUT") {
        div.textContent = div.textContent.replace(/\s/g, " ");
      }

      var span = this.getDocument().createElement("span");
      span.textContent = element.value.substring(position) || ".";
      div.appendChild(span);
      var rect = element.getBoundingClientRect();
      var doc = document.documentElement;
      var windowLeft = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
      var windowTop = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
      var coordinates = {
        top: rect.top + windowTop + span.offsetTop + parseInt(computed.borderTopWidth) + parseInt(computed.fontSize) - element.scrollTop,
        left: rect.left + windowLeft + span.offsetLeft + parseInt(computed.borderLeftWidth)
      };
      var windowWidth = window.innerWidth;
      var windowHeight = window.innerHeight;
      var menuDimensions = this.getMenuDimensions();
      var menuIsOffScreen = this.isMenuOffScreen(coordinates, menuDimensions);

      if (menuIsOffScreen.right) {
        coordinates.right = windowWidth - coordinates.left;
        coordinates.left = "auto";
      }

      var parentHeight = this.tribute.menuContainer ? this.tribute.menuContainer.offsetHeight : this.getDocument().body.offsetHeight;

      if (menuIsOffScreen.bottom) {
        var parentRect = this.tribute.menuContainer ? this.tribute.menuContainer.getBoundingClientRect() : this.getDocument().body.getBoundingClientRect();
        var scrollStillAvailable = parentHeight - (windowHeight - parentRect.top);
        coordinates.bottom = scrollStillAvailable + (windowHeight - rect.top - span.offsetTop);
        coordinates.top = "auto";
      }

      menuIsOffScreen = this.isMenuOffScreen(coordinates, menuDimensions);

      if (menuIsOffScreen.left) {
        coordinates.left = windowWidth > menuDimensions.width ? windowLeft + windowWidth - menuDimensions.width : windowLeft;
        delete coordinates.right;
      }

      if (menuIsOffScreen.top) {
        coordinates.top = windowHeight > menuDimensions.height ? windowTop + windowHeight - menuDimensions.height : windowTop;
        delete coordinates.bottom;
      }

      this.getDocument().body.removeChild(div);
      return coordinates;
    }
  }, {
    key: "getContentEditableCaretPosition",
    value: function getContentEditableCaretPosition(selectedNodePosition) {
      var markerTextChar = "﻿";
      var markerEl,
          markerId = "sel_".concat(new Date().getTime(), "_").concat(Math.random().toString().substr(2));
      var range;
      var sel = this.getWindowSelection();
      var prevRange = sel.getRangeAt(0);
      range = this.getDocument().createRange();
      range.setStart(sel.anchorNode, selectedNodePosition);
      range.setEnd(sel.anchorNode, selectedNodePosition);
      range.collapse(false); // Create the marker element containing a single invisible character using DOM methods and insert it

      markerEl = this.getDocument().createElement("span");
      markerEl.id = markerId;
      markerEl.appendChild(this.getDocument().createTextNode(markerTextChar));
      range.insertNode(markerEl);
      sel.removeAllRanges();
      sel.addRange(prevRange);
      var rect = markerEl.getBoundingClientRect();
      var doc = document.documentElement;
      var windowLeft = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
      var windowTop = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
      var coordinates = {
        left: rect.left + windowLeft,
        top: rect.top + markerEl.offsetHeight + windowTop
      };
      var windowWidth = window.innerWidth;
      var windowHeight = window.innerHeight;
      var menuDimensions = this.getMenuDimensions();
      var menuIsOffScreen = this.isMenuOffScreen(coordinates, menuDimensions);

      if (menuIsOffScreen.right) {
        coordinates.left = "auto";
        coordinates.right = windowWidth - rect.left - windowLeft;
      }

      var parentHeight = this.tribute.menuContainer ? this.tribute.menuContainer.offsetHeight : this.getDocument().body.offsetHeight;

      if (menuIsOffScreen.bottom) {
        var parentRect = this.tribute.menuContainer ? this.tribute.menuContainer.getBoundingClientRect() : this.getDocument().body.getBoundingClientRect();
        var scrollStillAvailable = parentHeight - (windowHeight - parentRect.top);
        coordinates.top = "auto";
        coordinates.bottom = scrollStillAvailable + (windowHeight - rect.top);
      }

      menuIsOffScreen = this.isMenuOffScreen(coordinates, menuDimensions);

      if (menuIsOffScreen.left) {
        coordinates.left = windowWidth > menuDimensions.width ? windowLeft + windowWidth - menuDimensions.width : windowLeft;
        delete coordinates.right;
      }

      if (menuIsOffScreen.top) {
        coordinates.top = windowHeight > menuDimensions.height ? windowTop + windowHeight - menuDimensions.height : windowTop;
        delete coordinates.bottom;
      }

      markerEl.parentNode.removeChild(markerEl);
      return coordinates;
    } // eslint-disable-next-line

  }, {
    key: "scrollIntoView",
    value: function scrollIntoView(elem) {
      var reasonableBuffer = 20,
          clientRect;
      var maxScrollDisplacement = 100;
      var e = this.menu;
      if (typeof e === "undefined") return;

      while (clientRect === undefined || clientRect.height === 0) {
        clientRect = e.getBoundingClientRect();

        if (clientRect.height === 0) {
          e = e.childNodes[0];

          if (e === undefined || !e.getBoundingClientRect) {
            return;
          }
        }
      }

      var elemTop = clientRect.top;
      var elemBottom = elemTop + clientRect.height;

      if (elemTop < 0) {
        window.scrollTo(0, window.pageYOffset + clientRect.top - reasonableBuffer);
      } else if (elemBottom > window.innerHeight) {
        var maxY = window.pageYOffset + clientRect.top - reasonableBuffer;

        if (maxY - window.pageYOffset > maxScrollDisplacement) {
          maxY = window.pageYOffset + maxScrollDisplacement;
        }

        var targetY = window.pageYOffset - (window.innerHeight - elemBottom);

        if (targetY > maxY) {
          targetY = maxY;
        }

        window.scrollTo(0, targetY);
      }
    }
  }]);

  return TributeRange;
}();

var _default = TributeRange;
exports["default"] = _default;
module.exports = exports.default;

},{}],5:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

// Thanks to https://github.com/mattyork/fuzzy
var TributeSearch = /*#__PURE__*/function () {
  function TributeSearch(tribute) {
    _classCallCheck(this, TributeSearch);

    this.tribute = tribute;
    this.tribute.search = this;
  }

  _createClass(TributeSearch, [{
    key: "simpleFilter",
    value: function simpleFilter(pattern, array) {
      var _this = this;

      return array.filter(function (string) {
        return _this.test(pattern, string);
      });
    }
  }, {
    key: "test",
    value: function test(pattern, string) {
      return this.match(pattern, string) !== null;
    }
  }, {
    key: "match",
    value: function match(pattern, string, opts) {
      opts = opts || {}; // eslint-disable-next-line

      var patternIdx = 0,
          // eslint-disable-next-line
      result = [],
          // eslint-disable-next-line
      len = string.length,
          // eslint-disable-next-line
      totalScore = 0,
          // eslint-disable-next-line
      currScore = 0,
          pre = opts.pre || "",
          post = opts.post || "",
          compareString = opts.caseSensitive && string || string.toLowerCase(),
          // eslint-disable-next-line
      ch,
          // eslint-disable-next-line
      compareChar;
      pattern = opts.caseSensitive && pattern || pattern.toLowerCase();
      var patternCache = this.traverse(compareString, pattern, 0, 0, []);

      if (!patternCache) {
        return null;
      }

      return {
        rendered: this.render(string, patternCache.cache, pre, post),
        score: patternCache.score
      };
    }
  }, {
    key: "traverse",
    value: function traverse(string, pattern, stringIndex, patternIndex, patternCache) {
      // if the pattern search at end
      if (pattern.length === patternIndex) {
        // calculate score and copy the cache containing the indices where it's found
        return {
          score: this.calculateScore(patternCache),
          cache: patternCache.slice()
        };
      } // if string at end or remaining pattern > remaining string


      if (string.length === stringIndex || pattern.length - patternIndex > string.length - stringIndex) {
        return undefined;
      }

      var c = pattern[patternIndex];
      var index = string.indexOf(c, stringIndex);
      var best, temp;

      while (index > -1) {
        patternCache.push(index);
        temp = this.traverse(string, pattern, index + 1, patternIndex + 1, patternCache);
        patternCache.pop(); // if downstream traversal failed, return best answer so far

        if (!temp) {
          return best;
        }

        if (!best || best.score < temp.score) {
          best = temp;
        }

        index = string.indexOf(c, index + 1);
      }

      return best;
    }
  }, {
    key: "calculateScore",
    value: function calculateScore(patternCache) {
      var score = 0;
      var temp = 1;
      patternCache.forEach(function (index, i) {
        if (i > 0) {
          if (patternCache[i - 1] + 1 === index) {
            temp += temp + 1;
          } else {
            temp = 1;
          }
        }

        score += temp;
      });
      return score;
    }
  }, {
    key: "render",
    value: function render(string, indices, pre, post) {
      var rendered = string.substring(0, indices[0]);
      indices.forEach(function (index, i) {
        rendered += pre + string[index] + post + string.substring(index + 1, indices[i + 1] ? indices[i + 1] : string.length);
      });
      return rendered;
    }
  }, {
    key: "filter",
    value: function filter(pattern, arr, opts) {
      var _this2 = this;

      opts = opts || {};
      return arr // eslint-disable-next-line
      .reduce(function (prev, element, idx, arr) {
        var str = element;

        if (opts.extract) {
          str = opts.extract(element);

          if (!str) {
            // take care of undefineds / nulls / etc.
            str = "";
          }
        }

        var rendered = _this2.match(pattern, str, opts);

        if (rendered != null) {
          prev[prev.length] = {
            string: rendered.rendered,
            score: rendered.score,
            index: idx,
            original: element
          };
        }

        return prev;
      }, []).sort(function (a, b) {
        var compare = b.score - a.score;
        if (compare) return compare;
        return a.index - b.index;
      });
    }
  }]);

  return TributeSearch;
}();

var _default = TributeSearch;
exports["default"] = _default;
module.exports = exports.default;

},{}],6:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _Tribute = _interopRequireDefault(require("./Tribute"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/**
 * Tribute.js
 * Native ES6 JavaScript @mention Plugin
 **/
var _default = _Tribute["default"];
exports["default"] = _default;
module.exports = exports.default;

},{"./Tribute":1}],7:[function(require,module,exports){
"use strict";

if (!Array.prototype.find) {
  Array.prototype.find = function (predicate) {
    if (this === null) {
      throw new TypeError("Array.prototype.find called on null or undefined");
    }

    if (typeof predicate !== "function") {
      throw new TypeError("predicate must be a function");
    }

    var list = Object(this);
    var length = list.length >>> 0;
    var thisArg = arguments[1];
    var value;

    for (var i = 0; i < length; i++) {
      value = list[i];

      if (predicate.call(thisArg, value, i, list)) {
        return value;
      }
    }

    return undefined;
  };
}

if (window && typeof window.CustomEvent !== "function") {
  // eslint-disable-next-line
  var CustomEvent = function CustomEvent(event, params) {
    params = params || {
      bubbles: false,
      cancelable: false,
      detail: undefined
    };
    var evt = document.createEvent("CustomEvent");
    evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
    return evt;
  };

  if (typeof window.Event !== "undefined") {
    CustomEvent.prototype = window.Event.prototype;
  }

  window.CustomEvent = CustomEvent;
}

},{}]},{},[6])(6)
});

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvVHJpYnV0ZS5qcyIsInNyYy9UcmlidXRlRXZlbnRzLmpzIiwic3JjL1RyaWJ1dGVNZW51RXZlbnRzLmpzIiwic3JjL1RyaWJ1dGVSYW5nZS5qcyIsInNyYy9UcmlidXRlU2VhcmNoLmpzIiwic3JjL2luZGV4LmpzIiwic3JjL3V0aWxzLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7OztBQ0NBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOzs7Ozs7Ozs7O0lBRU0sTztBQUNGLHlCQXlCRztBQUFBOztBQUFBLDJCQXhCQyxNQXdCRDtBQUFBLFFBeEJDLE1Bd0JELDRCQXhCVSxJQXdCVjtBQUFBLDJCQXZCQyxNQXVCRDtBQUFBLFFBdkJDLE1BdUJELDRCQXZCVSxJQXVCVjtBQUFBLGdDQXRCQyxXQXNCRDtBQUFBLFFBdEJDLFdBc0JELGlDQXRCZSxXQXNCZjtBQUFBLDRCQXJCQyxPQXFCRDtBQUFBLFFBckJDLE9BcUJELDZCQXJCVyxHQXFCWDtBQUFBLHFDQXBCQyxnQkFvQkQ7QUFBQSxRQXBCQyxnQkFvQkQsc0NBcEJvQixLQW9CcEI7QUFBQSxtQ0FuQkMsY0FtQkQ7QUFBQSxRQW5CQyxjQW1CRCxvQ0FuQmtCLElBbUJsQjtBQUFBLHFDQWxCQyxnQkFrQkQ7QUFBQSxRQWxCQyxnQkFrQkQsc0NBbEJvQixJQWtCcEI7QUFBQSwyQkFqQkMsTUFpQkQ7QUFBQSxRQWpCQyxNQWlCRCw0QkFqQlUsS0FpQlY7QUFBQSw2QkFoQkMsUUFnQkQ7QUFBQSxRQWhCQyxRQWdCRCw4QkFoQlksT0FnQlo7QUFBQSwrQkFmQyxVQWVEO0FBQUEsUUFmQyxVQWVELGdDQWZjLElBZWQ7QUFBQSxrQ0FkQyxhQWNEO0FBQUEsUUFkQyxhQWNELG1DQWRpQixJQWNqQjtBQUFBLG9DQWJDLGVBYUQ7QUFBQSxRQWJDLGVBYUQscUNBYm1CLElBYW5CO0FBQUEsb0NBWkMsZUFZRDtBQUFBLFFBWkMsZUFZRCxxQ0FabUIsSUFZbkI7QUFBQSxtQ0FYQyxjQVdEO0FBQUEsUUFYQyxjQVdELG9DQVhrQixJQVdsQjtBQUFBLHFDQVZDLG1CQVVEO0FBQUEsUUFWQyxtQkFVRCxzQ0FWdUIsSUFVdkI7QUFBQSxnQ0FUQyxXQVNEO0FBQUEsUUFUQyxXQVNELGlDQVRlLEtBU2Y7QUFBQSxxQ0FSQyxpQkFRRDtBQUFBLFFBUkMsaUJBUUQsc0NBUnFCLElBUXJCO0FBQUEsaUNBUEMsWUFPRDtBQUFBLFFBUEMsWUFPRCxrQ0FQZ0IsSUFPaEI7QUFBQSxxQ0FOQyxpQkFNRDtBQUFBLFFBTkMsaUJBTUQsc0NBTnFCLEtBTXJCO0FBQUEsb0NBTEMsZUFLRDtBQUFBLFFBTEMsZUFLRCxxQ0FMbUIsS0FLbkI7QUFBQSwrQkFKQyxVQUlEO0FBQUEsUUFKQyxVQUlELGdDQUpjLEVBSWQ7QUFBQSwyQkFIQyxNQUdEO0FBQUEsUUFIQyxNQUdELDRCQUhVLEVBR1Y7QUFBQSxxQ0FGQyxnQkFFRDtBQUFBLFFBRkMsZ0JBRUQsc0NBRm9CLElBRXBCO0FBQUEsa0NBREMsYUFDRDtBQUFBLFFBREMsYUFDRCxtQ0FEaUIsSUFDakI7O0FBQUE7O0FBQ0MsU0FBSyxnQkFBTCxHQUF3QixnQkFBeEI7QUFDQSxTQUFLLFlBQUwsR0FBb0IsQ0FBcEI7QUFDQSxTQUFLLE9BQUwsR0FBZSxFQUFmO0FBQ0EsU0FBSyxVQUFMLEdBQWtCLEtBQWxCO0FBQ0EsU0FBSyxRQUFMLEdBQWdCLEtBQWhCO0FBQ0EsU0FBSyxhQUFMLEdBQXFCLGFBQXJCO0FBQ0EsU0FBSyxlQUFMLEdBQXVCLGVBQXZCO0FBQ0EsU0FBSyxXQUFMLEdBQW1CLFdBQW5CO0FBQ0EsU0FBSyxpQkFBTCxHQUF5QixpQkFBekI7QUFDQSxTQUFLLFlBQUwsR0FBb0IsWUFBcEI7QUFDQSxTQUFLLGdCQUFMLEdBQXdCLEtBQXhCO0FBQ0EsU0FBSyxpQkFBTCxHQUF5QixpQkFBekI7QUFDQSxTQUFLLGVBQUwsR0FBdUIsZUFBdkI7QUFDQSxTQUFLLFlBQUwsR0FBb0IsUUFBUSxDQUFDLFdBQVQsQ0FBcUIsT0FBckIsQ0FBcEI7QUFDQSxTQUFLLFlBQUwsQ0FBa0IsU0FBbEIsQ0FBNEIsU0FBNUIsRUFBdUMsSUFBdkMsRUFBNkMsSUFBN0M7O0FBRUEsUUFBSSxLQUFLLGdCQUFULEVBQTJCO0FBQ3ZCLE1BQUEsT0FBTyxHQUFHLEVBQVY7QUFDQSxNQUFBLFdBQVcsR0FBRyxLQUFkO0FBQ0g7O0FBRUQsUUFBSSxNQUFKLEVBQVk7QUFDUixXQUFLLFVBQUwsR0FBa0IsQ0FDZDtBQUNJO0FBQ0EsUUFBQSxPQUFPLEVBQUUsT0FGYjtBQUlJO0FBQ0EsUUFBQSxNQUFNLEVBQUUsTUFMWjtBQU9JO0FBQ0EsUUFBQSxXQUFXLEVBQUUsV0FSakI7QUFVSTtBQUNBLFFBQUEsY0FBYyxFQUFFLENBQUMsY0FBYyxJQUFJLE9BQU8sQ0FBQyxxQkFBM0IsRUFBa0QsSUFBbEQsQ0FBdUQsSUFBdkQsQ0FYcEI7QUFhSTtBQUNBLFFBQUEsZ0JBQWdCLEVBQUUsQ0FBQyxnQkFBZ0IsSUFBSSxPQUFPLENBQUMsdUJBQTdCLEVBQXNELElBQXRELENBQTJELElBQTNELENBZHRCO0FBZ0JJO0FBQ0EsUUFBQSxlQUFlLEVBQUcsVUFBQSxDQUFDLEVBQUk7QUFDbkIsY0FBSSxPQUFPLENBQVAsS0FBYSxVQUFqQixFQUE2QjtBQUN6QixtQkFBTyxDQUFDLENBQUMsSUFBRixDQUFPLEtBQVAsQ0FBUDtBQUNIOztBQUVELGlCQUNJLGVBQWUsSUFDZixZQUFXO0FBQ1AsbUJBQU8sRUFBUDtBQUNILFdBRkQsQ0FFRSxJQUZGLENBRU8sS0FGUCxDQUZKO0FBTUgsU0FYZ0IsQ0FXZCxlQVhjLENBakJyQjtBQThCSSxRQUFBLGNBQWMsRUFBRyxVQUFBLENBQUMsRUFBSTtBQUNsQixjQUFJLE9BQU8sQ0FBUCxLQUFhLFVBQWpCLEVBQTZCO0FBQ3pCLG1CQUFPLENBQUMsQ0FBQyxJQUFGLENBQU8sS0FBUCxDQUFQO0FBQ0g7O0FBRUQsaUJBQ0ksY0FBYyxJQUNkLFlBQVc7QUFDUCxtQkFBTyxFQUFQO0FBQ0gsV0FGRCxDQUVFLElBRkYsQ0FFTyxLQUZQLENBRko7QUFNSCxTQVhlLENBV2IsY0FYYSxDQTlCcEI7QUEyQ0k7QUFDQSxRQUFBLE1BQU0sRUFBRSxNQTVDWjtBQThDSTtBQUNBLFFBQUEsUUFBUSxFQUFFLFFBL0NkO0FBaURJO0FBQ0EsUUFBQSxNQUFNLEVBQUUsTUFsRFo7QUFvREksUUFBQSxtQkFBbUIsRUFBRSxtQkFwRHpCO0FBc0RJLFFBQUEsVUFBVSxFQUFFLFVBdERoQjtBQXdESSxRQUFBLE1BQU0sRUFBRSxNQXhEWjtBQTBESSxRQUFBLGVBQWUsRUFBRSxlQTFEckI7QUE0REksUUFBQSxnQkFBZ0IsRUFBRyxVQUFBLENBQUMsRUFBSTtBQUNwQixjQUFJLE9BQU8sQ0FBUCxLQUFhLFVBQWpCLEVBQTZCO0FBQ3pCLG1CQUFPLENBQUMsQ0FBQyxJQUFGLENBQU8sS0FBUCxDQUFQO0FBQ0g7O0FBRUQsaUJBQ0ksZ0JBQWdCLElBQ2hCLFlBQVc7QUFDUCxtQkFBTyxFQUFQO0FBQ0gsV0FGRCxDQUVFLElBRkYsQ0FFTyxLQUZQLENBRko7QUFNSCxTQVhpQixDQVdmLGdCQVhlLENBNUR0QjtBQXlFSSxRQUFBLGFBQWEsRUFBRTtBQXpFbkIsT0FEYyxDQUFsQjtBQTZFSCxLQTlFRCxNQThFTyxJQUFJLFVBQUosRUFBZ0I7QUFDbkIsVUFBSSxLQUFLLGdCQUFULEVBQTJCLE9BQU8sQ0FBQyxJQUFSLENBQWEsNERBQWI7QUFDM0IsV0FBSyxVQUFMLEdBQWtCLFVBQVUsQ0FBQyxHQUFYLENBQWUsVUFBQSxJQUFJLEVBQUk7QUFDckMsZUFBTztBQUNILFVBQUEsT0FBTyxFQUFFLElBQUksQ0FBQyxPQUFMLElBQWdCLE9BRHRCO0FBRUgsVUFBQSxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQUwsSUFBZSxNQUZwQjtBQUdILFVBQUEsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFMLElBQW9CLFdBSDlCO0FBSUgsVUFBQSxjQUFjLEVBQUUsQ0FBQyxJQUFJLENBQUMsY0FBTCxJQUF1QixPQUFPLENBQUMscUJBQWhDLEVBQXVELElBQXZELENBQTRELEtBQTVELENBSmI7QUFLSCxVQUFBLGdCQUFnQixFQUFFLENBQUMsSUFBSSxDQUFDLGdCQUFMLElBQXlCLE9BQU8sQ0FBQyx1QkFBbEMsRUFBMkQsSUFBM0QsQ0FBZ0UsS0FBaEUsQ0FMZjtBQU1IO0FBQ0EsVUFBQSxlQUFlLEVBQUcsVUFBQSxDQUFDLEVBQUk7QUFDbkIsZ0JBQUksT0FBTyxDQUFQLEtBQWEsVUFBakIsRUFBNkI7QUFDekIscUJBQU8sQ0FBQyxDQUFDLElBQUYsQ0FBTyxLQUFQLENBQVA7QUFDSDs7QUFFRCxtQkFBTyxJQUFQO0FBQ0gsV0FOZ0IsQ0FNZCxlQU5jLENBUGQ7QUFjSCxVQUFBLGNBQWMsRUFBRyxVQUFBLENBQUMsRUFBSTtBQUNsQixnQkFBSSxPQUFPLENBQVAsS0FBYSxVQUFqQixFQUE2QjtBQUN6QixxQkFBTyxDQUFDLENBQUMsSUFBRixDQUFPLEtBQVAsQ0FBUDtBQUNIOztBQUVELG1CQUFPLElBQVA7QUFDSCxXQU5lLENBTWIsY0FOYSxDQWRiO0FBcUJILFVBQUEsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFMLElBQWUsTUFyQnBCO0FBc0JILFVBQUEsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFMLElBQWlCLFFBdEJ4QjtBQXVCSCxVQUFBLE1BQU0sRUFBRSxJQUFJLENBQUMsTUF2QlY7QUF3QkgsVUFBQSxtQkFBbUIsRUFBRSxJQUFJLENBQUMsbUJBeEJ2QjtBQXlCSCxVQUFBLFVBQVUsRUFBRSxJQUFJLENBQUMsVUFBTCxJQUFtQixVQXpCNUI7QUEwQkgsVUFBQSxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQUwsSUFBZSxNQTFCcEI7QUEyQkgsVUFBQSxnQkFBZ0IsRUFBRyxVQUFBLENBQUMsRUFBSTtBQUNwQixnQkFBSSxPQUFPLENBQVAsS0FBYSxVQUFqQixFQUE2QjtBQUN6QixxQkFBTyxDQUFDLENBQUMsSUFBRixDQUFPLEtBQVAsQ0FBUDtBQUNIOztBQUVELG1CQUFPLElBQVA7QUFDSCxXQU5pQixDQU1mLGdCQU5lLENBM0JmO0FBa0NILFVBQUEsYUFBYSxFQUFFLElBQUksQ0FBQyxhQUFMLElBQXNCO0FBbENsQyxTQUFQO0FBb0NILE9BckNpQixDQUFsQjtBQXNDSCxLQXhDTSxNQXdDQTtBQUNILFlBQU0sSUFBSSxLQUFKLENBQVUsb0NBQVYsQ0FBTjtBQUNIOztBQUVELFFBQUksd0JBQUosQ0FBaUIsSUFBakI7QUFDQSxRQUFJLHlCQUFKLENBQWtCLElBQWxCO0FBQ0EsUUFBSSw2QkFBSixDQUFzQixJQUF0QjtBQUNBLFFBQUkseUJBQUosQ0FBa0IsSUFBbEI7QUFDSDs7OzsrQkF1QlU7QUFDUCxhQUFPLEtBQUssVUFBTCxDQUFnQixHQUFoQixDQUFvQixVQUFBLE1BQU0sRUFBSTtBQUNqQyxlQUFPLE1BQU0sQ0FBQyxPQUFkO0FBQ0gsT0FGTSxDQUFQO0FBR0g7OzsyQkFFTSxFLEVBQUksTSxFQUFRO0FBQ2YsVUFBSSxDQUFDLEVBQUwsRUFBUztBQUNMLGNBQU0sSUFBSSxLQUFKLENBQVUsZ0RBQVYsQ0FBTjtBQUNILE9BSGMsQ0FLZjs7O0FBQ0EsVUFBSSxPQUFPLE1BQVAsS0FBa0IsV0FBbEIsSUFBaUMsRUFBRSxZQUFZLE1BQU0sQ0FBQyxNQUExRCxFQUFrRTtBQUM5RCxRQUFBLEVBQUUsR0FBRyxFQUFFLENBQUMsR0FBSCxFQUFMO0FBQ0gsT0FSYyxDQVVmOzs7QUFDQSxVQUFJLEVBQUUsQ0FBQyxXQUFILEtBQW1CLFFBQW5CLElBQStCLEVBQUUsQ0FBQyxXQUFILEtBQW1CLGNBQWxELElBQW9FLEVBQUUsQ0FBQyxXQUFILEtBQW1CLEtBQTNGLEVBQWtHO0FBQzlGLFlBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQyxNQUFoQjs7QUFDQSxhQUFLLElBQUksQ0FBQyxHQUFHLENBQWIsRUFBZ0IsQ0FBQyxHQUFHLE1BQXBCLEVBQTRCLEVBQUUsQ0FBOUIsRUFBaUM7QUFDN0IsZUFBSyxPQUFMLENBQWEsRUFBRSxDQUFDLENBQUQsQ0FBZjtBQUNIO0FBQ0osT0FMRCxNQUtPO0FBQ0gsYUFBSyxPQUFMLENBQWEsRUFBYixFQUFpQixNQUFqQjtBQUNIO0FBQ0o7Ozs0QkFFTyxFLEVBQUksTSxFQUFRO0FBQ2hCLFVBQUksRUFBRSxDQUFDLFlBQUgsQ0FBZ0IsY0FBaEIsQ0FBSixFQUFxQztBQUNqQyxRQUFBLE9BQU8sQ0FBQyxJQUFSLENBQWEsa0NBQWtDLEVBQUUsQ0FBQyxRQUFsRDtBQUNIOztBQUVELFdBQUssY0FBTCxDQUFvQixFQUFwQjtBQUNBLFdBQUssTUFBTCxDQUFZLElBQVosQ0FBaUIsRUFBakIsRUFBcUIsTUFBckI7O0FBRUEsVUFBSSxLQUFLLGVBQVQsRUFBMEI7QUFDdEIsYUFBSyxlQUFMLENBQXFCLGdCQUFyQixDQUFzQyxRQUF0QyxFQUFnRCxLQUFLLFdBQUwsQ0FBaUIsSUFBakIsQ0FBc0IsSUFBdEIsQ0FBaEQ7QUFDSDs7QUFFRCxNQUFBLEVBQUUsQ0FBQyxZQUFILENBQWdCLGNBQWhCLEVBQWdDLElBQWhDO0FBQ0g7OztnQ0FFVyxDLEVBQUc7QUFDWCxXQUFLLE1BQUwsQ0FBWSxNQUFaLENBQW1CLElBQW5CLEVBQXlCLENBQXpCO0FBQ0g7OzttQ0FFYyxPLEVBQVM7QUFDcEIsVUFBSSxPQUFPLENBQUMsVUFBUixHQUFxQixPQUFyQixDQUE2QixPQUFPLENBQUMsUUFBckMsTUFBbUQsQ0FBQyxDQUF4RCxFQUEyRDtBQUN2RCxZQUFJLE9BQU8sQ0FBQyxlQUFaLEVBQTZCO0FBQ3pCLFVBQUEsT0FBTyxDQUFDLGVBQVIsR0FBMEIsSUFBMUI7QUFDSCxTQUZELE1BRU87QUFDSCxnQkFBTSxJQUFJLEtBQUosQ0FBVSw4QkFBOEIsT0FBTyxDQUFDLFFBQWhELENBQU47QUFDSDtBQUNKO0FBQ0o7OztpQ0FFWTtBQUNULFVBQUksT0FBTyxHQUFHLEtBQUssS0FBTCxDQUFXLFdBQVgsR0FBeUIsYUFBekIsQ0FBdUMsS0FBdkMsQ0FBZDtBQUFBLFVBQ0ksRUFBRSxHQUFHLEtBQUssS0FBTCxDQUFXLFdBQVgsR0FBeUIsYUFBekIsQ0FBdUMsSUFBdkMsQ0FEVDtBQUdBLE1BQUEsT0FBTyxDQUFDLFNBQVIsR0FBb0IsbUJBQXBCO0FBQ0EsTUFBQSxPQUFPLENBQUMsV0FBUixDQUFvQixFQUFwQjs7QUFFQSxVQUFJLEtBQUssYUFBVCxFQUF3QjtBQUNwQixlQUFPLEtBQUssYUFBTCxDQUFtQixXQUFuQixDQUErQixPQUEvQixDQUFQO0FBQ0g7O0FBRUQsYUFBTyxLQUFLLEtBQUwsQ0FBVyxXQUFYLEdBQXlCLElBQXpCLENBQThCLFdBQTlCLENBQTBDLE9BQTFDLENBQVA7QUFDSDs7O2dDQUVXLE8sRUFBUyxRLEVBQVU7QUFBQTs7QUFDM0I7QUFDQSxVQUNJLEtBQUssUUFBTCxJQUNBLEtBQUssT0FBTCxDQUFhLE9BQWIsS0FBeUIsT0FEekIsSUFFQSxLQUFLLE9BQUwsQ0FBYSxXQUFiLEtBQTZCLEtBQUssMEJBSHRDLEVBSUU7QUFDRTtBQUNIOztBQUNELFdBQUssMEJBQUwsR0FBa0MsS0FBSyxPQUFMLENBQWEsV0FBL0MsQ0FUMkIsQ0FXM0I7O0FBQ0EsVUFBSSxDQUFDLEtBQUssSUFBVixFQUFnQjtBQUNaLGFBQUssSUFBTCxHQUFZLEtBQUssVUFBTCxFQUFaO0FBQ0EsUUFBQSxPQUFPLENBQUMsV0FBUixHQUFzQixLQUFLLElBQTNCO0FBQ0EsYUFBSyxVQUFMLENBQWdCLElBQWhCLENBQXFCLEtBQUssSUFBMUI7QUFDSDs7QUFFRCxXQUFLLFFBQUwsR0FBZ0IsSUFBaEI7QUFDQSxXQUFLLFlBQUwsR0FBb0IsQ0FBcEI7O0FBRUEsVUFBSSxDQUFDLEtBQUssT0FBTCxDQUFhLFdBQWxCLEVBQStCO0FBQzNCLGFBQUssT0FBTCxDQUFhLFdBQWIsR0FBMkIsRUFBM0I7QUFDSDs7QUFFRCxVQUFNLGFBQWEsR0FBRyxTQUFoQixhQUFnQixDQUFDLE1BQUQsRUFBUyxJQUFULEVBQWtCO0FBQ3BDO0FBQ0EsWUFBSSxDQUFDLE1BQUksQ0FBQyxRQUFWLEVBQW9CO0FBQ2hCO0FBQ0g7O0FBRUQsWUFBSSxLQUFLLEdBQUcsTUFBSSxDQUFDLE1BQUwsQ0FBWSxNQUFaLENBQW1CLE1BQUksQ0FBQyxPQUFMLENBQWEsV0FBaEMsRUFBNkMsTUFBN0MsRUFBcUQ7QUFDN0QsVUFBQSxHQUFHLEVBQUUsTUFBSSxDQUFDLE9BQUwsQ0FBYSxVQUFiLENBQXdCLFVBQXhCLENBQW1DLEdBQW5DLElBQTBDLFFBRGM7QUFFN0QsVUFBQSxJQUFJLEVBQUUsTUFBSSxDQUFDLE9BQUwsQ0FBYSxVQUFiLENBQXdCLFVBQXhCLENBQW1DLElBQW5DLElBQTJDLFNBRlk7QUFHN0QsVUFBQSxPQUFPLEVBQUUsaUJBQUEsRUFBRSxFQUFJO0FBQ1gsZ0JBQUksT0FBTyxNQUFJLENBQUMsT0FBTCxDQUFhLFVBQWIsQ0FBd0IsTUFBL0IsS0FBMEMsUUFBOUMsRUFBd0Q7QUFDcEQscUJBQU8sRUFBRSxDQUFDLE1BQUksQ0FBQyxPQUFMLENBQWEsVUFBYixDQUF3QixNQUF6QixDQUFUO0FBQ0gsYUFGRCxNQUVPLElBQUksT0FBTyxNQUFJLENBQUMsT0FBTCxDQUFhLFVBQWIsQ0FBd0IsTUFBL0IsS0FBMEMsVUFBOUMsRUFBMEQ7QUFDN0QscUJBQU8sTUFBSSxDQUFDLE9BQUwsQ0FBYSxVQUFiLENBQXdCLE1BQXhCLENBQStCLEVBQS9CLEVBQW1DLE1BQUksQ0FBQyxPQUFMLENBQWEsV0FBaEQsQ0FBUDtBQUNILGFBRk0sTUFFQTtBQUNILG9CQUFNLElBQUksS0FBSixDQUFVLDhEQUFWLENBQU47QUFDSDtBQUNKO0FBWDRELFNBQXJELENBQVo7O0FBY0EsUUFBQSxNQUFJLENBQUMsT0FBTCxDQUFhLGFBQWIsR0FBNkIsS0FBN0I7O0FBRUEsWUFBSSxFQUFFLEdBQUcsTUFBSSxDQUFDLElBQUwsQ0FBVSxhQUFWLENBQXdCLElBQXhCLENBQVQ7O0FBRUEsUUFBQSxNQUFJLENBQUMsS0FBTCxDQUFXLG1CQUFYLENBQStCLFFBQS9COztBQUVBLFlBQUksTUFBSSxDQUFDLE9BQUwsQ0FBYSxVQUFiLENBQXdCLGNBQXhCLElBQTBDLE1BQUksQ0FBQyxPQUFMLENBQWEsVUFBYixDQUF3QixjQUF4QixDQUF1QyxJQUF2QyxDQUE5QyxFQUE0RjtBQUN4RixjQUFNLE1BQU0sR0FBRyxRQUFRLENBQUMsYUFBVCxDQUF1QixLQUF2QixDQUFmO0FBQ0EsVUFBQSxNQUFNLENBQUMsWUFBUCxDQUFvQixPQUFwQixFQUE2QixRQUE3QjtBQUNBLFVBQUEsTUFBTSxDQUFDLFNBQVAsR0FBbUIsTUFBSSxDQUFDLE9BQUwsQ0FBYSxVQUFiLENBQXdCLGNBQXhCLENBQXVDLElBQXZDLENBQW5COztBQUNBLGNBQU0sU0FBUyxHQUFHLE1BQUksQ0FBQyxJQUFMLENBQVUsYUFBVixDQUF3QixTQUF4QixDQUFsQjs7QUFDQSxjQUFJLFNBQVMsSUFBSSxTQUFTLENBQUMsTUFBM0IsRUFBbUM7QUFDL0IsWUFBQSxTQUFTLENBQUMsTUFBVjtBQUNILFdBRkQsTUFFTyxJQUFJLFNBQVMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUE1QixFQUFvQztBQUN2QyxZQUFBLFNBQVMsQ0FBQyxVQUFWLENBQXFCLFdBQXJCLENBQWlDLFNBQWpDO0FBQ0g7O0FBQ0QsVUFBQSxNQUFJLENBQUMsSUFBTCxDQUFVLFlBQVYsQ0FBdUIsTUFBdkIsRUFBK0IsTUFBSSxDQUFDLElBQUwsQ0FBVSxVQUFWLENBQXFCLENBQXJCLENBQS9CO0FBQ0g7O0FBRUQsWUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFYLEVBQW1CO0FBQ2YsY0FBSSxZQUFZLEdBQUcsSUFBSSxXQUFKLENBQWdCLGtCQUFoQixFQUFvQztBQUFFLFlBQUEsTUFBTSxFQUFFLE1BQUksQ0FBQztBQUFmLFdBQXBDLENBQW5COztBQUNBLFVBQUEsTUFBSSxDQUFDLE9BQUwsQ0FBYSxPQUFiLENBQXFCLGFBQXJCLENBQW1DLFlBQW5DOztBQUNBLGNBQUksQ0FBQyxNQUFJLENBQUMsT0FBTCxDQUFhLFVBQWIsQ0FBd0IsZUFBN0IsRUFBOEM7QUFDMUMsWUFBQSxNQUFJLENBQUMsUUFBTDtBQUNILFdBRkQsTUFFTztBQUNILFlBQUEsRUFBRSxDQUFDLFNBQUgsR0FBZSxNQUFJLENBQUMsT0FBTCxDQUFhLFVBQWIsQ0FBd0IsZUFBeEIsRUFBZjtBQUNIOztBQUVEO0FBQ0g7O0FBRUQsWUFBSSxNQUFJLENBQUMsT0FBTCxDQUFhLFVBQWIsQ0FBd0IsYUFBNUIsRUFBMkM7QUFDdkMsVUFBQSxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQU4sQ0FBWSxDQUFaLEVBQWUsTUFBSSxDQUFDLE9BQUwsQ0FBYSxVQUFiLENBQXdCLGFBQXZDLENBQVI7QUFDSDs7QUFFRCxRQUFBLEVBQUUsQ0FBQyxTQUFILEdBQWUsRUFBZjs7QUFDQSxZQUFJLFFBQVEsR0FBRyxNQUFJLENBQUMsS0FBTCxDQUFXLFdBQVgsR0FBeUIsc0JBQXpCLEVBQWY7O0FBRUEsUUFBQSxLQUFLLENBQUMsT0FBTixDQUFjLFVBQUMsSUFBRCxFQUFPLEtBQVAsRUFBaUI7QUFDM0IsY0FBSSxFQUFFLEdBQUcsTUFBSSxDQUFDLEtBQUwsQ0FBVyxXQUFYLEdBQXlCLGFBQXpCLENBQXVDLElBQXZDLENBQVQ7O0FBQ0EsVUFBQSxFQUFFLENBQUMsWUFBSCxDQUFnQixZQUFoQixFQUE4QixLQUE5QjtBQUNBLFVBQUEsRUFBRSxDQUFDLGdCQUFILENBQW9CLFdBQXBCLEVBQWlDLFVBQUEsQ0FBQyxFQUFJO0FBQ2xDLGdCQUFJLEVBQUUsR0FBRyxDQUFDLENBQUMsTUFBWDtBQUNBLGdCQUFJLEtBQUssR0FDTCxFQUFFLENBQUMsWUFBSCxDQUFnQixZQUFoQixLQUFrQyxFQUFFLENBQUMsVUFBSCxJQUFpQixFQUFFLENBQUMsVUFBSCxDQUFjLFlBQWQsQ0FBMkIsWUFBM0IsQ0FEdkQ7O0FBRUEsZ0JBQUksQ0FBQyxDQUFDLFNBQUYsS0FBZ0IsQ0FBcEIsRUFBdUI7QUFDbkIsY0FBQSxNQUFJLENBQUMsTUFBTCxDQUFZLFdBQVosQ0FBd0IsS0FBeEI7QUFDSDtBQUNKLFdBUEQ7O0FBUUEsY0FBSSxNQUFJLENBQUMsWUFBTCxLQUFzQixLQUExQixFQUFpQztBQUM3QixZQUFBLEVBQUUsQ0FBQyxTQUFILEdBQWUsTUFBSSxDQUFDLE9BQUwsQ0FBYSxVQUFiLENBQXdCLFdBQXZDO0FBQ0g7O0FBQ0QsVUFBQSxFQUFFLENBQUMsU0FBSCxHQUFlLE1BQUksQ0FBQyxPQUFMLENBQWEsVUFBYixDQUF3QixnQkFBeEIsQ0FBeUMsSUFBekMsQ0FBZjtBQUNBLFVBQUEsUUFBUSxDQUFDLFdBQVQsQ0FBcUIsRUFBckI7QUFDSCxTQWhCRDtBQWlCQSxRQUFBLEVBQUUsQ0FBQyxXQUFILENBQWUsUUFBZjtBQUNILE9BNUVEOztBQThFQSxVQUFJLE9BQU8sS0FBSyxPQUFMLENBQWEsVUFBYixDQUF3QixNQUEvQixLQUEwQyxVQUE5QyxFQUEwRDtBQUN0RCxhQUFLLE9BQUwsQ0FBYSxVQUFiLENBQXdCLE1BQXhCLENBQStCLEtBQUssT0FBTCxDQUFhLFdBQTVDLEVBQXlELGFBQXpEO0FBQ0gsT0FGRCxNQUVPO0FBQ0gsUUFBQSxhQUFhLENBQUMsS0FBSyxPQUFMLENBQWEsVUFBYixDQUF3QixNQUF6QixFQUFpQyxLQUFLLE9BQUwsQ0FBYSxXQUE5QyxDQUFiO0FBQ0g7O0FBQ0QsV0FBSyxJQUFMLENBQVUsYUFBVixDQUF3QixJQUF4QixFQUE4QixTQUE5QixHQUEwQyxDQUExQztBQUNIOzs7MENBRXFCLE8sRUFBUyxlLEVBQWlCO0FBQzVDLFVBQUksT0FBTyxLQUFLLFFBQVEsQ0FBQyxhQUF6QixFQUF3QztBQUNwQyxhQUFLLGVBQUwsQ0FBcUIsT0FBckI7QUFDSDs7QUFFRCxXQUFLLE9BQUwsQ0FBYSxVQUFiLEdBQTBCLEtBQUssVUFBTCxDQUFnQixlQUFlLElBQUksQ0FBbkMsQ0FBMUI7QUFDQSxXQUFLLE9BQUwsQ0FBYSxlQUFiLEdBQStCLElBQS9CO0FBQ0EsV0FBSyxPQUFMLENBQWEsT0FBYixHQUF1QixPQUF2QjtBQUVBLFVBQUksT0FBTyxDQUFDLGlCQUFaLEVBQStCLEtBQUssa0JBQUwsQ0FBd0IsS0FBSyxPQUFMLENBQWEsVUFBYixDQUF3QixPQUFoRCxFQUEvQixLQUNLLEtBQUssYUFBTCxDQUFtQixPQUFuQixFQUE0QixLQUFLLE9BQUwsQ0FBYSxVQUFiLENBQXdCLE9BQXBEO0FBRUwsV0FBSyxXQUFMLENBQWlCLE9BQWpCO0FBQ0gsSyxDQUVEOzs7O29DQUNnQixFLEVBQUk7QUFDaEIsTUFBQSxFQUFFLENBQUMsS0FBSDs7QUFDQSxVQUFJLE9BQU8sTUFBTSxDQUFDLFlBQWQsSUFBOEIsV0FBOUIsSUFBNkMsT0FBTyxRQUFRLENBQUMsV0FBaEIsSUFBK0IsV0FBaEYsRUFBNkY7QUFDekYsWUFBSSxLQUFLLEdBQUcsUUFBUSxDQUFDLFdBQVQsRUFBWjtBQUNBLFFBQUEsS0FBSyxDQUFDLGtCQUFOLENBQXlCLEVBQXpCO0FBQ0EsUUFBQSxLQUFLLENBQUMsUUFBTixDQUFlLEtBQWY7QUFDQSxZQUFJLEdBQUcsR0FBRyxNQUFNLENBQUMsWUFBUCxFQUFWO0FBQ0EsUUFBQSxHQUFHLENBQUMsZUFBSjtBQUNBLFFBQUEsR0FBRyxDQUFDLFFBQUosQ0FBYSxLQUFiO0FBQ0gsT0FQRCxNQU9PLElBQUksT0FBTyxRQUFRLENBQUMsSUFBVCxDQUFjLGVBQXJCLElBQXdDLFdBQTVDLEVBQXlEO0FBQzVELFlBQUksU0FBUyxHQUFHLFFBQVEsQ0FBQyxJQUFULENBQWMsZUFBZCxFQUFoQjtBQUNBLFFBQUEsU0FBUyxDQUFDLGlCQUFWLENBQTRCLEVBQTVCO0FBQ0EsUUFBQSxTQUFTLENBQUMsUUFBVixDQUFtQixLQUFuQjtBQUNBLFFBQUEsU0FBUyxDQUFDLE1BQVY7QUFDSDtBQUNKLEssQ0FFRDs7Ozt1Q0FDbUIsSSxFQUFNO0FBQ3JCO0FBQ0EsVUFBSSxHQUFKLEVBQVMsS0FBVCxFQUFnQixJQUFoQjtBQUNBLE1BQUEsR0FBRyxHQUFHLE1BQU0sQ0FBQyxZQUFQLEVBQU47QUFDQSxNQUFBLEtBQUssR0FBRyxHQUFHLENBQUMsVUFBSixDQUFlLENBQWYsQ0FBUjtBQUNBLE1BQUEsS0FBSyxDQUFDLGNBQU47QUFDQSxVQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsY0FBVCxDQUF3QixJQUF4QixDQUFmO0FBQ0EsTUFBQSxLQUFLLENBQUMsVUFBTixDQUFpQixRQUFqQjtBQUNBLE1BQUEsS0FBSyxDQUFDLGtCQUFOLENBQXlCLFFBQXpCO0FBQ0EsTUFBQSxLQUFLLENBQUMsUUFBTixDQUFlLEtBQWY7QUFDQSxNQUFBLEdBQUcsQ0FBQyxlQUFKO0FBQ0EsTUFBQSxHQUFHLENBQUMsUUFBSixDQUFhLEtBQWI7QUFDSCxLLENBRUQ7Ozs7a0NBQ2MsUSxFQUFVLEksRUFBTTtBQUMxQixVQUFJLFNBQVMsR0FBRyxRQUFRLENBQUMsU0FBekI7QUFDQSxVQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsY0FBeEI7QUFFQSxVQUFJLEtBQUssR0FBRyxRQUFRLENBQUMsS0FBVCxDQUFlLFNBQWYsQ0FBeUIsQ0FBekIsRUFBNEIsUUFBNUIsQ0FBWjtBQUNBLFVBQUksSUFBSSxHQUFHLFFBQVEsQ0FBQyxLQUFULENBQWUsU0FBZixDQUF5QixRQUFRLENBQUMsWUFBbEMsRUFBZ0QsUUFBUSxDQUFDLEtBQVQsQ0FBZSxNQUEvRCxDQUFYO0FBQ0EsTUFBQSxRQUFRLENBQUMsS0FBVCxHQUFpQixLQUFLLEdBQUcsSUFBUixHQUFlLElBQWhDO0FBQ0EsTUFBQSxRQUFRLEdBQUcsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUEzQjtBQUNBLE1BQUEsUUFBUSxDQUFDLGNBQVQsR0FBMEIsUUFBMUI7QUFDQSxNQUFBLFFBQVEsQ0FBQyxZQUFULEdBQXdCLFFBQXhCO0FBQ0EsTUFBQSxRQUFRLENBQUMsS0FBVDtBQUNBLE1BQUEsUUFBUSxDQUFDLFNBQVQsR0FBcUIsU0FBckI7QUFDSDs7OytCQUVVO0FBQ1AsVUFBSSxLQUFLLElBQVQsRUFBZTtBQUNYLGFBQUssSUFBTCxDQUFVLEtBQVYsQ0FBZ0IsT0FBaEIsR0FBMEIsZ0JBQTFCO0FBQ0EsYUFBSyxRQUFMLEdBQWdCLEtBQWhCO0FBQ0EsYUFBSyxZQUFMLEdBQW9CLENBQXBCO0FBQ0EsYUFBSyxPQUFMLEdBQWUsRUFBZjtBQUNIO0FBQ0o7OztzQ0FFaUIsSyxFQUFPLGEsRUFBZTtBQUNwQyxNQUFBLEtBQUssR0FBRyxRQUFRLENBQUMsS0FBRCxDQUFoQjtBQUNBLFVBQUksT0FBTyxLQUFQLEtBQWlCLFFBQWpCLElBQTZCLEtBQUssQ0FBQyxLQUFELENBQXRDLEVBQStDO0FBQy9DLFVBQUksSUFBSSxHQUFHLEtBQUssT0FBTCxDQUFhLGFBQWIsQ0FBMkIsS0FBM0IsQ0FBWDs7QUFDQSxVQUNJLE9BQU8sS0FBSyxPQUFMLENBQWEsVUFBYixDQUF3QixnQkFBL0IsS0FBb0QsVUFBcEQsSUFDQSxLQUFLLE9BQUwsQ0FBYSxVQUFiLENBQXdCLGdCQUF4QixDQUF5QyxJQUF6QyxFQUErQyxLQUFLLE9BQUwsQ0FBYSxVQUFiLENBQXdCLE1BQXZFLE1BQW1GLEtBRnZGLEVBR0U7QUFDRSxhQUFLLE9BQUwsQ0FBYSxVQUFiLENBQXdCLE1BQXhCLENBQStCLEVBQS9CLENBQWtDLGFBQWxDLENBQWdELEtBQUssWUFBckQ7QUFDQTtBQUNIOztBQUNELFVBQUksT0FBTyxHQUFHLEtBQUssT0FBTCxDQUFhLFVBQWIsQ0FBd0IsY0FBeEIsQ0FBdUMsSUFBdkMsQ0FBZDtBQUNBLFVBQUksT0FBTyxLQUFLLElBQWhCLEVBQXNCLEtBQUssV0FBTCxDQUFpQixPQUFqQixFQUEwQixhQUExQixFQUF5QyxJQUF6QztBQUN6Qjs7O2dDQUVXLE8sRUFBUyxhLEVBQWUsSSxFQUFNO0FBQ3RDLFdBQUssS0FBTCxDQUFXLGtCQUFYLENBQThCLE9BQTlCLEVBQXVDLElBQXZDLEVBQTZDLElBQTdDLEVBQW1ELGFBQW5ELEVBQWtFLElBQWxFO0FBQ0g7Ozs0QkFFTyxVLEVBQVksUyxFQUFXLE8sRUFBUztBQUNwQyxVQUFJLE9BQU8sVUFBVSxDQUFDLE1BQWxCLEtBQTZCLFVBQWpDLEVBQTZDO0FBQ3pDLGNBQU0sSUFBSSxLQUFKLENBQVUsa0RBQVYsQ0FBTjtBQUNILE9BRkQsTUFFTyxJQUFJLENBQUMsT0FBTCxFQUFjO0FBQ2pCLFFBQUEsVUFBVSxDQUFDLE1BQVgsR0FBb0IsVUFBVSxDQUFDLE1BQVgsQ0FBa0IsTUFBbEIsQ0FBeUIsU0FBekIsQ0FBcEI7QUFDSCxPQUZNLE1BRUE7QUFDSCxRQUFBLFVBQVUsQ0FBQyxNQUFYLEdBQW9CLFNBQXBCO0FBQ0g7QUFDSjs7OzJCQUVNLGUsRUFBaUIsUyxFQUFXLE8sRUFBUztBQUN4QyxVQUFJLEtBQUssR0FBRyxRQUFRLENBQUMsZUFBRCxDQUFwQjtBQUNBLFVBQUksT0FBTyxLQUFQLEtBQWlCLFFBQXJCLEVBQStCLE1BQU0sSUFBSSxLQUFKLENBQVUsdURBQVYsQ0FBTjtBQUUvQixVQUFJLFVBQVUsR0FBRyxLQUFLLFVBQUwsQ0FBZ0IsS0FBaEIsQ0FBakI7O0FBRUEsV0FBSyxPQUFMLENBQWEsVUFBYixFQUF5QixTQUF6QixFQUFvQyxPQUFwQztBQUNIOzs7a0NBRWEsUyxFQUFXLE8sRUFBUztBQUM5QixVQUFJLEtBQUssUUFBVCxFQUFtQjtBQUNmLGFBQUssT0FBTCxDQUFhLEtBQUssT0FBTCxDQUFhLFVBQTFCLEVBQXNDLFNBQXRDLEVBQWlELE9BQWpEO0FBQ0gsT0FGRCxNQUVPO0FBQ0gsY0FBTSxJQUFJLEtBQUosQ0FBVSwrREFBVixDQUFOO0FBQ0g7QUFDSjs7OzJCQUVNLEUsRUFBSTtBQUNQLFVBQUksQ0FBQyxFQUFMLEVBQVM7QUFDTCxjQUFNLElBQUksS0FBSixDQUFVLGdEQUFWLENBQU47QUFDSCxPQUhNLENBS1A7OztBQUNBLFVBQUksT0FBTyxNQUFQLEtBQWtCLFdBQWxCLElBQWlDLEVBQUUsWUFBWSxNQUFNLENBQUMsTUFBMUQsRUFBa0U7QUFDOUQsUUFBQSxFQUFFLEdBQUcsRUFBRSxDQUFDLEdBQUgsRUFBTDtBQUNILE9BUk0sQ0FVUDs7O0FBQ0EsVUFBSSxFQUFFLENBQUMsV0FBSCxLQUFtQixRQUFuQixJQUErQixFQUFFLENBQUMsV0FBSCxLQUFtQixjQUFsRCxJQUFvRSxFQUFFLENBQUMsV0FBSCxLQUFtQixLQUEzRixFQUFrRztBQUM5RixZQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBaEI7O0FBQ0EsYUFBSyxJQUFJLENBQUMsR0FBRyxDQUFiLEVBQWdCLENBQUMsR0FBRyxNQUFwQixFQUE0QixFQUFFLENBQTlCLEVBQWlDO0FBQzdCLGVBQUssT0FBTCxDQUFhLEVBQUUsQ0FBQyxDQUFELENBQWY7QUFDSDtBQUNKLE9BTEQsTUFLTztBQUNILGFBQUssT0FBTCxDQUFhLEVBQWI7QUFDSDtBQUNKOzs7NEJBRU8sRSxFQUFJO0FBQUE7O0FBQ1IsV0FBSyxNQUFMLENBQVksTUFBWixDQUFtQixFQUFuQjs7QUFDQSxVQUFJLEVBQUUsQ0FBQyxXQUFQLEVBQW9CO0FBQ2hCLGFBQUssVUFBTCxDQUFnQixNQUFoQixDQUF1QixFQUFFLENBQUMsV0FBMUI7QUFDSDs7QUFDRCxVQUFJLEtBQUssZUFBVCxFQUEwQjtBQUN0QixhQUFLLGVBQUwsQ0FBcUIsbUJBQXJCLENBQXlDLFFBQXpDLEVBQW1ELEtBQUssV0FBeEQ7QUFDSDs7QUFFRCxNQUFBLFVBQVUsQ0FBQyxZQUFNO0FBQ2IsUUFBQSxFQUFFLENBQUMsZUFBSCxDQUFtQixjQUFuQjtBQUNBLFFBQUEsTUFBSSxDQUFDLFFBQUwsR0FBZ0IsS0FBaEI7O0FBQ0EsWUFBSSxFQUFFLENBQUMsV0FBUCxFQUFvQjtBQUNoQixVQUFBLEVBQUUsQ0FBQyxXQUFILENBQWUsTUFBZjtBQUNIO0FBQ0osT0FOUyxDQUFWO0FBT0g7OzswQ0FyVzRCLEksRUFBTTtBQUMvQixVQUFJLE9BQU8sSUFBUCxLQUFnQixXQUFwQixFQUFpQyxPQUFPLElBQVA7O0FBQ2pDLFVBQUksS0FBSyxLQUFMLENBQVcsaUJBQVgsQ0FBNkIsS0FBSyxPQUFMLENBQWEsT0FBMUMsQ0FBSixFQUF3RDtBQUNwRCxlQUNJLG9DQUNDLEtBQUssT0FBTCxDQUFhLFVBQWIsQ0FBd0IsT0FBeEIsR0FBa0MsSUFBSSxDQUFDLFFBQUwsQ0FBYyxLQUFLLE9BQUwsQ0FBYSxVQUFiLENBQXdCLFFBQXRDLENBRG5DLElBRUEsU0FISjtBQUtIOztBQUVELGFBQU8sS0FBSyxPQUFMLENBQWEsVUFBYixDQUF3QixPQUF4QixHQUFrQyxJQUFJLENBQUMsUUFBTCxDQUFjLEtBQUssT0FBTCxDQUFhLFVBQWIsQ0FBd0IsUUFBdEMsQ0FBekM7QUFDSDs7OzRDQUU4QixTLEVBQVc7QUFDdEMsYUFBTyxTQUFTLENBQUMsTUFBakI7QUFDSDs7O2lDQUVtQjtBQUNoQixhQUFPLENBQUMsVUFBRCxFQUFhLE9BQWIsQ0FBUDtBQUNIOzs7Ozs7ZUFxVlUsTzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDL2hCVCxhO0FBQ0YseUJBQVksT0FBWixFQUFxQjtBQUFBOztBQUNqQixTQUFLLE9BQUwsR0FBZSxPQUFmO0FBQ0EsU0FBSyxPQUFMLENBQWEsTUFBYixHQUFzQixJQUF0QjtBQUNIOzs7O3lCQWlESSxPLEVBQVMsTSxFQUFRO0FBQ2xCLE1BQUEsT0FBTyxDQUFDLFlBQVIsR0FBdUIsS0FBSyxPQUFMLENBQWEsSUFBYixDQUFrQixPQUFsQixFQUEyQixJQUEzQixFQUFpQyxNQUFqQyxDQUF2QjtBQUNBLE1BQUEsT0FBTyxDQUFDLFVBQVIsR0FBcUIsS0FBSyxLQUFMLENBQVcsSUFBWCxDQUFnQixPQUFoQixFQUF5QixJQUF6QixFQUErQixNQUEvQixDQUFyQjtBQUNBLE1BQUEsT0FBTyxDQUFDLFVBQVIsR0FBcUIsS0FBSyxLQUFMLENBQVcsSUFBWCxDQUFnQixPQUFoQixFQUF5QixJQUF6QixFQUErQixNQUEvQixDQUFyQjtBQUVBLE1BQUEsT0FBTyxDQUFDLGdCQUFSLENBQXlCLFNBQXpCLEVBQW9DLE9BQU8sQ0FBQyxZQUE1QyxFQUEwRCxLQUExRDtBQUNBLE1BQUEsT0FBTyxDQUFDLGdCQUFSLENBQXlCLE9BQXpCLEVBQWtDLE9BQU8sQ0FBQyxVQUExQyxFQUFzRCxLQUF0RDtBQUNBLE1BQUEsT0FBTyxDQUFDLGdCQUFSLENBQXlCLE9BQXpCLEVBQWtDLE9BQU8sQ0FBQyxVQUExQyxFQUFzRCxLQUF0RDtBQUNIOzs7MkJBRU0sTyxFQUFTO0FBQ1osTUFBQSxPQUFPLENBQUMsbUJBQVIsQ0FBNEIsU0FBNUIsRUFBdUMsT0FBTyxDQUFDLFlBQS9DLEVBQTZELEtBQTdEO0FBQ0EsTUFBQSxPQUFPLENBQUMsbUJBQVIsQ0FBNEIsT0FBNUIsRUFBcUMsT0FBTyxDQUFDLFVBQTdDLEVBQXlELEtBQXpEO0FBQ0EsTUFBQSxPQUFPLENBQUMsbUJBQVIsQ0FBNEIsT0FBNUIsRUFBcUMsT0FBTyxDQUFDLFVBQTdDLEVBQXlELEtBQXpEO0FBRUEsYUFBTyxPQUFPLENBQUMsWUFBZjtBQUNBLGFBQU8sT0FBTyxDQUFDLFVBQWY7QUFDQSxhQUFPLE9BQU8sQ0FBQyxVQUFmO0FBQ0gsSyxDQUVEOzs7OzJCQUNPLFEsRUFBVSxDLEVBQUc7QUFDaEIsTUFBQSxRQUFRLENBQUMsUUFBVCxHQUFvQixLQUFwQjtBQUNBLE1BQUEsUUFBUSxDQUFDLFFBQVQ7QUFDSDs7OzRCQUVPLFEsRUFBVSxNLEVBQVEsSyxFQUFPO0FBQzdCLFVBQUksUUFBUSxDQUFDLE9BQVQsQ0FBaUIsUUFBakIsSUFBNkIsQ0FBQyxFQUFELEVBQUssRUFBTCxFQUFTLEVBQVQsRUFBYSxFQUFiLEVBQWlCLFFBQWpCLENBQTBCLEtBQUssQ0FBQyxPQUFoQyxDQUFqQyxFQUEyRTtBQUN2RTtBQUNIOztBQUNELFVBQUksUUFBUSxDQUFDLGdCQUFULENBQTBCLEtBQTFCLENBQUosRUFBc0M7QUFDbEMsUUFBQSxRQUFRLENBQUMsT0FBVCxDQUFpQixRQUFqQixHQUE0QixLQUE1QjtBQUNBLFFBQUEsUUFBUSxDQUFDLE9BQVQsQ0FBaUIsUUFBakI7QUFDSDs7QUFFRCxVQUFJLE9BQU8sR0FBRyxJQUFkO0FBQ0EsTUFBQSxRQUFRLENBQUMsWUFBVCxHQUF3QixLQUF4QjtBQUVBLE1BQUEsYUFBYSxDQUFDLElBQWQsR0FBcUIsT0FBckIsQ0FBNkIsVUFBQSxDQUFDLEVBQUk7QUFDOUIsWUFBSSxDQUFDLENBQUMsR0FBRixLQUFVLEtBQUssQ0FBQyxPQUFwQixFQUE2QjtBQUN6QixVQUFBLFFBQVEsQ0FBQyxZQUFULEdBQXdCLElBQXhCO0FBQ0EsVUFBQSxRQUFRLENBQUMsU0FBVCxHQUFxQixDQUFDLENBQUMsS0FBRixDQUFRLFdBQVIsRUFBckIsRUFBNEMsS0FBNUMsRUFBbUQsT0FBbkQsRUFBNEQsTUFBNUQ7QUFDSDtBQUNKLE9BTEQ7QUFNSDs7OzBCQUVLLFEsRUFBVSxLLEVBQU8sTSxFQUFRO0FBQzNCLE1BQUEsUUFBUSxDQUFDLFVBQVQsR0FBc0IsSUFBdEI7QUFDQSxNQUFBLFFBQVEsQ0FBQyxLQUFULENBQWUsSUFBZixDQUFvQixJQUFwQixFQUEwQixRQUExQixFQUFvQyxLQUFwQyxFQUEyQyxNQUEzQztBQUNIOzs7MEJBRUssUSxFQUFVLEssRUFBTztBQUNuQixVQUFJLE9BQU8sR0FBRyxRQUFRLENBQUMsT0FBdkI7O0FBQ0EsVUFBSSxPQUFPLENBQUMsSUFBUixJQUFnQixPQUFPLENBQUMsSUFBUixDQUFhLFFBQWIsQ0FBc0IsS0FBSyxDQUFDLE1BQTVCLENBQXBCLEVBQXlEO0FBQ3JELFFBQUEsS0FBSyxDQUFDLGNBQU47QUFDQSxRQUFBLEtBQUssQ0FBQyxlQUFOOztBQUNBLFlBQUksS0FBSyxDQUFDLE1BQU4sQ0FBYSxZQUFiLENBQTBCLE9BQTFCLE1BQXVDLFFBQXZDLElBQW1ELEtBQUssQ0FBQyxNQUFOLENBQWEsT0FBYixLQUF5QixJQUFoRixFQUFzRjtBQUNsRjtBQUNIOztBQUNELFlBQUksRUFBRSxHQUFHLEtBQUssQ0FBQyxNQUFmOztBQUNBLGVBQU8sRUFBRSxDQUFDLFFBQUgsQ0FBWSxXQUFaLE9BQThCLElBQXJDLEVBQTJDO0FBQ3ZDLFVBQUEsRUFBRSxHQUFHLEVBQUUsQ0FBQyxVQUFSOztBQUNBLGNBQUksQ0FBQyxFQUFELElBQU8sRUFBRSxLQUFLLE9BQU8sQ0FBQyxJQUExQixFQUFnQztBQUM1QixrQkFBTSxJQUFJLEtBQUosQ0FBVSw4Q0FBVixDQUFOO0FBQ0g7QUFDSjs7QUFDRCxRQUFBLE9BQU8sQ0FBQyxpQkFBUixDQUEwQixFQUFFLENBQUMsWUFBSCxDQUFnQixZQUFoQixDQUExQixFQUF5RCxLQUF6RDtBQUNBLFFBQUEsT0FBTyxDQUFDLFFBQVIsR0FkcUQsQ0FnQnJEO0FBQ0gsT0FqQkQsTUFpQk8sSUFBSSxPQUFPLENBQUMsT0FBUixDQUFnQixPQUFoQixJQUEyQixDQUFDLE9BQU8sQ0FBQyxPQUFSLENBQWdCLGVBQWhELEVBQWlFO0FBQ3BFLFFBQUEsT0FBTyxDQUFDLE9BQVIsQ0FBZ0IsZUFBaEIsR0FBa0MsS0FBbEM7QUFDQSxRQUFBLFVBQVUsQ0FBQztBQUFBLGlCQUFNLE9BQU8sQ0FBQyxRQUFSLEVBQU47QUFBQSxTQUFELENBQVY7QUFDSDtBQUNKOzs7MEJBRUssUSxFQUFVLE0sRUFBUSxLLEVBQU87QUFDM0IsVUFBSSxRQUFRLENBQUMsT0FBVCxDQUFpQixRQUFqQixJQUE2QixDQUFDLEVBQUQsRUFBSyxFQUFMLEVBQVMsRUFBVCxFQUFhLEVBQWIsRUFBaUIsUUFBakIsQ0FBMEIsS0FBSyxDQUFDLE9BQWhDLENBQWpDLEVBQTJFO0FBQ3ZFO0FBQ0g7O0FBRUQsVUFBSSxRQUFRLENBQUMsVUFBYixFQUF5QjtBQUNyQixRQUFBLFFBQVEsQ0FBQyxVQUFULEdBQXNCLEtBQXRCO0FBQ0g7O0FBQ0QsTUFBQSxRQUFRLENBQUMsZUFBVCxDQUF5QixJQUF6QjtBQUNBLFVBQUksS0FBSyxDQUFDLE9BQU4sS0FBa0IsRUFBdEIsRUFBMEI7O0FBRTFCLFVBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxXQUFqQixJQUFnQyxNQUFNLENBQUMsV0FBUCxDQUFtQixLQUFuQixPQUErQixDQUFuRSxFQUFzRTtBQUNsRSxRQUFBLFFBQVEsQ0FBQyxPQUFULENBQWlCLFFBQWpCLEdBQTRCLEtBQTVCO0FBQ0EsUUFBQSxRQUFRLENBQUMsT0FBVCxDQUFpQixRQUFqQjtBQUNBO0FBQ0g7O0FBRUQsVUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFULENBQWlCLFdBQWxCLElBQWlDLFFBQVEsQ0FBQyxPQUFULENBQWlCLGdCQUF0RCxFQUF3RTtBQUNwRSxRQUFBLFFBQVEsQ0FBQyxPQUFULENBQWlCLGdCQUFqQixHQUFvQyxLQUFwQztBQUNBLFFBQUEsUUFBUSxDQUFDLFlBQVQsR0FBd0IsSUFBeEI7QUFDQSxRQUFBLFFBQVEsQ0FBQyxTQUFULEdBQXFCLE9BQXJCLEVBQThCLEtBQTlCLEVBQXFDLElBQXJDO0FBQ0E7QUFDSDs7QUFFRCxVQUFJLENBQUMsUUFBUSxDQUFDLE9BQVQsQ0FBaUIsUUFBdEIsRUFBZ0M7QUFDNUIsWUFBSSxRQUFRLENBQUMsT0FBVCxDQUFpQixnQkFBckIsRUFBdUM7QUFDbkMsVUFBQSxRQUFRLENBQUMsU0FBVCxHQUFxQixXQUFyQixDQUFpQyxLQUFqQyxFQUF3QyxJQUF4QyxFQUE4QyxFQUE5QztBQUNILFNBRkQsTUFFTztBQUNILGNBQUksT0FBTyxHQUFHLFFBQVEsQ0FBQyxVQUFULENBQW9CLFFBQXBCLEVBQThCLElBQTlCLEVBQW9DLEtBQXBDLENBQWQ7QUFFQSxjQUFJLEtBQUssQ0FBQyxPQUFELENBQUwsSUFBa0IsQ0FBQyxPQUF2QixFQUFnQztBQUVoQyxjQUFJLE9BQU8sR0FBRyxRQUFRLENBQUMsT0FBVCxDQUFpQixRQUFqQixHQUE0QixJQUE1QixDQUFpQyxVQUFBLE9BQU8sRUFBSTtBQUN0RCxtQkFBTyxPQUFPLENBQUMsVUFBUixDQUFtQixDQUFuQixNQUEwQixPQUFqQztBQUNILFdBRmEsQ0FBZDs7QUFJQSxjQUFJLE9BQU8sT0FBUCxLQUFtQixXQUF2QixFQUFvQztBQUNoQyxZQUFBLFFBQVEsQ0FBQyxTQUFULEdBQXFCLFdBQXJCLENBQWlDLEtBQWpDLEVBQXdDLElBQXhDLEVBQThDLE9BQTlDLEVBQXVELElBQXZEO0FBQ0g7QUFDSjtBQUNKOztBQUVELFVBQ0ssQ0FBQyxRQUFRLENBQUMsT0FBVCxDQUFpQixPQUFqQixDQUF5QixPQUF6QixJQUFvQyxRQUFRLENBQUMsT0FBVCxDQUFpQixnQkFBdEQsS0FDRyxRQUFRLENBQUMsWUFBVCxLQUEwQixLQUQ5QixJQUVDLFFBQVEsQ0FBQyxPQUFULENBQWlCLFFBQWpCLElBQTZCLENBQUMsQ0FBRCxFQUFJLEVBQUosRUFBUSxRQUFSLENBQWlCLEtBQUssQ0FBQyxPQUF2QixDQUhsQyxFQUlFO0FBQ0UsWUFBSSxLQUFLLENBQUMsT0FBTixLQUFrQixFQUF0QixFQUEwQjtBQUN0QixjQUFNLElBQUksR0FBRyxRQUFRLENBQUMsT0FBVCxDQUFpQixLQUFqQixDQUF1QixnQ0FBdkIsRUFBYjs7QUFDQSxjQUFJLElBQUksQ0FBQyxNQUFMLEdBQWMsQ0FBbEIsRUFBcUI7QUFDakIsWUFBQSxRQUFRLENBQUMsT0FBVCxDQUFpQixRQUFqQjtBQUNBO0FBQ0gsV0FIRCxNQUdPO0FBQ0gsWUFBQSxRQUFRLENBQUMsT0FBVCxDQUFpQixXQUFqQixDQUE2QixJQUE3QixFQUFtQyxJQUFuQztBQUNIO0FBQ0osU0FSRCxNQVFPO0FBQ0gsVUFBQSxRQUFRLENBQUMsT0FBVCxDQUFpQixXQUFqQixDQUE2QixJQUE3QixFQUFtQyxJQUFuQztBQUNIO0FBQ0o7QUFDSjs7O3FDQUVnQixLLEVBQU87QUFDcEIsVUFBSSxDQUFDLEtBQUssT0FBTCxDQUFhLFFBQWxCLEVBQTRCLE9BQU8sS0FBUDs7QUFFNUIsVUFBSSxLQUFLLE9BQUwsQ0FBYSxPQUFiLENBQXFCLFdBQXJCLENBQWlDLE1BQWpDLEtBQTRDLENBQWhELEVBQW1EO0FBQy9DLFlBQUksZUFBZSxHQUFHLEtBQXRCO0FBQ0EsUUFBQSxhQUFhLENBQUMsSUFBZCxHQUFxQixPQUFyQixDQUE2QixVQUFBLENBQUMsRUFBSTtBQUM5QixjQUFJLEtBQUssQ0FBQyxPQUFOLEtBQWtCLENBQUMsQ0FBQyxHQUF4QixFQUE2QixlQUFlLEdBQUcsSUFBbEI7QUFDaEMsU0FGRDtBQUlBLGVBQU8sQ0FBQyxlQUFSO0FBQ0g7O0FBRUQsYUFBTyxLQUFQO0FBQ0gsSyxDQUNEOzs7OytCQUNXLFEsRUFBVSxFLEVBQUksSyxFQUFPO0FBQzVCO0FBQ0EsVUFBSSxLQUFKOztBQUNBLFVBQUksT0FBTyxHQUFHLFFBQVEsQ0FBQyxPQUF2QjtBQUNBLFVBQUksSUFBSSxHQUFHLE9BQU8sQ0FBQyxLQUFSLENBQWMsY0FBZCxDQUNQLEtBRE8sRUFFUCxPQUFPLENBQUMsZ0JBRkQsRUFHUCxJQUhPLEVBSVAsT0FBTyxDQUFDLFdBSkQsRUFLUCxPQUFPLENBQUMsZ0JBTEQsQ0FBWDs7QUFRQSxVQUFJLElBQUosRUFBVTtBQUNOLGVBQU8sSUFBSSxDQUFDLGtCQUFMLENBQXdCLFVBQXhCLENBQW1DLENBQW5DLENBQVA7QUFDSCxPQUZELE1BRU87QUFDSCxlQUFPLEtBQVA7QUFDSDtBQUNKOzs7b0NBRWUsRSxFQUFJO0FBQ2hCLFdBQUssT0FBTCxDQUFhLE9BQWIsQ0FBcUIsT0FBckIsR0FBK0IsRUFBL0I7QUFDQSxVQUFJLElBQUksR0FBRyxLQUFLLE9BQUwsQ0FBYSxLQUFiLENBQW1CLGNBQW5CLENBQ1AsS0FETyxFQUVQLEtBQUssT0FBTCxDQUFhLGdCQUZOLEVBR1AsSUFITyxFQUlQLEtBQUssT0FBTCxDQUFhLFdBSk4sRUFLUCxLQUFLLE9BQUwsQ0FBYSxnQkFMTixDQUFYOztBQVFBLFVBQUksSUFBSixFQUFVO0FBQ04sYUFBSyxPQUFMLENBQWEsT0FBYixDQUFxQixZQUFyQixHQUFvQyxJQUFJLENBQUMsbUJBQXpDO0FBQ0EsYUFBSyxPQUFMLENBQWEsT0FBYixDQUFxQixXQUFyQixHQUFtQyxJQUFJLENBQUMsV0FBeEM7QUFDQSxhQUFLLE9BQUwsQ0FBYSxPQUFiLENBQXFCLGNBQXJCLEdBQXNDLElBQUksQ0FBQyxxQkFBM0M7QUFDSDtBQUNKOzs7Z0NBRVc7QUFBQTs7QUFDUixhQUFPO0FBQ0gsUUFBQSxXQUFXLEVBQUUscUJBQUMsQ0FBRCxFQUFJLEVBQUosRUFBUSxPQUFSLEVBQXNDO0FBQUEsY0FBckIsUUFBcUIsdUVBQVYsS0FBVTs7QUFDL0MsY0FBTSxJQUFJLEdBQUcsS0FBSSxDQUFDLE9BQUwsQ0FBYSxLQUFiLENBQW1CLGdDQUFuQixFQUFiOztBQUNBLGNBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFMLENBQVcsSUFBWCxDQUFkO0FBQ0EsY0FBTSxRQUFRLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFOLEdBQWUsQ0FBaEIsQ0FBdEI7O0FBQ0EsY0FBSSxRQUFRLENBQUMsS0FBVCxDQUFlLEdBQWYsRUFBb0IsTUFBcEIsR0FBNkIsQ0FBN0IsR0FBaUMsQ0FBckMsRUFBd0M7QUFDcEM7QUFDSDs7QUFDRCxjQUFJLFFBQVEsQ0FBQyxJQUFULEdBQWdCLENBQWhCLE1BQXVCLEdBQTNCLEVBQWdDO0FBQzVCO0FBQ0g7O0FBQ0QsY0FBSSxPQUFPLEdBQUcsS0FBSSxDQUFDLE9BQW5CO0FBQ0EsVUFBQSxPQUFPLENBQUMsT0FBUixDQUFnQixPQUFoQixHQUEwQixPQUExQjtBQUVBLGNBQUksY0FBYyxHQUFHLE9BQU8sQ0FBQyxVQUFSLENBQW1CLElBQW5CLENBQXdCLFVBQUEsSUFBSSxFQUFJO0FBQ2pELG1CQUFPLElBQUksQ0FBQyxPQUFMLEtBQWlCLE9BQXhCO0FBQ0gsV0FGb0IsQ0FBckI7QUFJQSxVQUFBLE9BQU8sQ0FBQyxPQUFSLENBQWdCLFVBQWhCLEdBQTZCLGNBQTdCO0FBQ0EsY0FBSSxPQUFPLENBQUMsVUFBUixJQUFzQixRQUExQixFQUFvQyxPQUFPLENBQUMsV0FBUixDQUFvQixFQUFwQixFQUF3QixJQUF4QjtBQUN2QyxTQXBCRTtBQXFCSDtBQUNBLFFBQUEsS0FBSyxFQUFFLGVBQUMsQ0FBRCxFQUFJLEVBQUosRUFBVztBQUNkO0FBQ0EsY0FBSSxLQUFJLENBQUMsT0FBTCxDQUFhLFFBQWIsSUFBeUIsS0FBSSxDQUFDLE9BQUwsQ0FBYSxPQUFiLENBQXFCLGFBQWxELEVBQWlFO0FBQzdELFlBQUEsQ0FBQyxDQUFDLGNBQUY7QUFDQSxZQUFBLENBQUMsQ0FBQyxlQUFGO0FBQ0EsWUFBQSxVQUFVLENBQUMsWUFBTTtBQUNiLGNBQUEsS0FBSSxDQUFDLE9BQUwsQ0FBYSxpQkFBYixDQUErQixLQUFJLENBQUMsT0FBTCxDQUFhLFlBQTVDLEVBQTBELENBQTFEOztBQUNBLGNBQUEsS0FBSSxDQUFDLE9BQUwsQ0FBYSxRQUFiO0FBQ0gsYUFIUyxFQUdQLENBSE8sQ0FBVjtBQUlIO0FBQ0osU0FoQ0U7QUFpQ0gsUUFBQSxLQUFLLEVBQUUsZUFBQyxDQUFELEVBQUksRUFBSixFQUFXO0FBQ2QsY0FBSSxLQUFJLENBQUMsT0FBTCxDQUFhLFFBQWpCLEVBQTJCO0FBQ3ZCLGdCQUFJLEtBQUksQ0FBQyxPQUFMLENBQWEsZUFBakIsRUFBa0M7QUFDOUIsY0FBQSxLQUFJLENBQUMsU0FBTCxHQUFpQixLQUFqQixDQUF1QixDQUF2QixFQUEwQixFQUExQjtBQUNIO0FBQ0o7QUFDSixTQXZDRTtBQXdDSDtBQUNBLFFBQUEsTUFBTSxFQUFFLGdCQUFDLENBQUQsRUFBSSxFQUFKLEVBQVc7QUFDZixjQUFJLEtBQUksQ0FBQyxPQUFMLENBQWEsUUFBakIsRUFBMkI7QUFDdkIsWUFBQSxDQUFDLENBQUMsY0FBRjtBQUNBLFlBQUEsQ0FBQyxDQUFDLGVBQUY7QUFDQSxZQUFBLEtBQUksQ0FBQyxPQUFMLENBQWEsUUFBYixHQUF3QixLQUF4Qjs7QUFDQSxZQUFBLEtBQUksQ0FBQyxPQUFMLENBQWEsUUFBYjtBQUNIO0FBQ0osU0FoREU7QUFpREgsUUFBQSxHQUFHLEVBQUUsYUFBQyxDQUFELEVBQUksRUFBSixFQUFXO0FBQ1o7QUFDQSxVQUFBLEtBQUksQ0FBQyxTQUFMLEdBQWlCLEtBQWpCLENBQXVCLENBQXZCLEVBQTBCLEVBQTFCO0FBQ0gsU0FwREU7QUFxREg7QUFDQSxRQUFBLEtBQUssRUFBRSxlQUFDLENBQUQsRUFBSSxFQUFKLEVBQVEsTUFBUixFQUFtQjtBQUN0QixjQUFJLEtBQUksQ0FBQyxPQUFMLENBQWEsUUFBakIsRUFBMkI7QUFDdkIsZ0JBQUksS0FBSSxDQUFDLE9BQUwsQ0FBYSxpQkFBakIsRUFBb0M7QUFDaEMsY0FBQSxLQUFJLENBQUMsU0FBTCxHQUFpQixLQUFqQixDQUF1QixDQUF2QixFQUEwQixFQUExQjtBQUNILGFBRkQsTUFFTyxJQUFJLENBQUMsS0FBSSxDQUFDLE9BQUwsQ0FBYSxXQUFsQixFQUErQjtBQUNsQyxjQUFBLENBQUMsQ0FBQyxlQUFGO0FBQ0EsY0FBQSxVQUFVLENBQUMsWUFBTTtBQUNiLGdCQUFBLEtBQUksQ0FBQyxPQUFMLENBQWEsUUFBYjs7QUFDQSxnQkFBQSxLQUFJLENBQUMsT0FBTCxDQUFhLFFBQWIsR0FBd0IsS0FBeEI7QUFDSCxlQUhTLEVBR1AsQ0FITyxDQUFWO0FBSUg7QUFDSjs7QUFDRCxjQUFNLElBQUksR0FBRyxLQUFJLENBQUMsT0FBTCxDQUFhLEtBQWIsQ0FBbUIsZ0NBQW5CLEdBQXNELElBQXRELEVBQWI7O0FBQ0EsY0FBSSxJQUFJLENBQUMsV0FBTCxDQUFpQixLQUFJLENBQUMsT0FBTCxDQUFhLE9BQWIsQ0FBcUIsT0FBdEMsTUFBbUQsSUFBSSxDQUFDLE1BQUwsR0FBYyxDQUFyRSxFQUF3RTtBQUNwRSxZQUFBLENBQUMsQ0FBQyxlQUFGO0FBQ0EsWUFBQSxVQUFVLENBQUMsWUFBTTtBQUNiLGNBQUEsS0FBSSxDQUFDLE9BQUwsQ0FBYSxRQUFiOztBQUNBLGNBQUEsS0FBSSxDQUFDLE9BQUwsQ0FBYSxRQUFiLEdBQXdCLEtBQXhCO0FBQ0gsYUFIUyxFQUdQLENBSE8sQ0FBVjtBQUlIO0FBQ0osU0ExRUU7QUEyRUg7QUFDQSxRQUFBLEVBQUUsRUFBRSxZQUFDLENBQUQsRUFBSSxFQUFKLEVBQVc7QUFDWDtBQUNBLGNBQUksS0FBSSxDQUFDLE9BQUwsQ0FBYSxRQUFiLElBQXlCLEtBQUksQ0FBQyxPQUFMLENBQWEsT0FBYixDQUFxQixhQUFsRCxFQUFpRTtBQUM3RCxZQUFBLENBQUMsQ0FBQyxjQUFGO0FBQ0EsWUFBQSxDQUFDLENBQUMsZUFBRjtBQUNBLGdCQUFJLEtBQUssR0FBRyxLQUFJLENBQUMsT0FBTCxDQUFhLE9BQWIsQ0FBcUIsYUFBckIsQ0FBbUMsTUFBL0M7QUFBQSxnQkFDSSxRQUFRLEdBQUcsS0FBSSxDQUFDLE9BQUwsQ0FBYSxZQUQ1Qjs7QUFHQSxnQkFBSSxLQUFLLEdBQUcsUUFBUixJQUFvQixRQUFRLEdBQUcsQ0FBbkMsRUFBc0M7QUFDbEMsY0FBQSxLQUFJLENBQUMsT0FBTCxDQUFhLFlBQWI7O0FBQ0EsY0FBQSxLQUFJLENBQUMsV0FBTDtBQUNILGFBSEQsTUFHTyxJQUFJLFFBQVEsS0FBSyxDQUFqQixFQUFvQjtBQUN2QixjQUFBLEtBQUksQ0FBQyxPQUFMLENBQWEsWUFBYixHQUE0QixLQUFLLEdBQUcsQ0FBcEM7O0FBQ0EsY0FBQSxLQUFJLENBQUMsV0FBTDs7QUFDQSxjQUFBLEtBQUksQ0FBQyxPQUFMLENBQWEsSUFBYixDQUFrQixhQUFsQixDQUFnQyxJQUFoQyxFQUFzQyxTQUF0QyxHQUFrRCxLQUFJLENBQUMsT0FBTCxDQUFhLElBQWIsQ0FBa0IsYUFBbEIsQ0FDOUMsSUFEOEMsRUFFaEQsWUFGRjtBQUdIO0FBQ0o7QUFDSixTQS9GRTtBQWdHSDtBQUNBLFFBQUEsSUFBSSxFQUFFLGNBQUMsQ0FBRCxFQUFJLEVBQUosRUFBVztBQUNiO0FBQ0EsY0FBSSxLQUFJLENBQUMsT0FBTCxDQUFhLFFBQWIsSUFBeUIsS0FBSSxDQUFDLE9BQUwsQ0FBYSxPQUFiLENBQXFCLGFBQWxELEVBQWlFO0FBQzdELFlBQUEsQ0FBQyxDQUFDLGNBQUY7QUFDQSxZQUFBLENBQUMsQ0FBQyxlQUFGO0FBQ0EsZ0JBQUksS0FBSyxHQUFHLEtBQUksQ0FBQyxPQUFMLENBQWEsT0FBYixDQUFxQixhQUFyQixDQUFtQyxNQUFuQyxHQUE0QyxDQUF4RDtBQUFBLGdCQUNJLFFBQVEsR0FBRyxLQUFJLENBQUMsT0FBTCxDQUFhLFlBRDVCOztBQUdBLGdCQUFJLEtBQUssR0FBRyxRQUFaLEVBQXNCO0FBQ2xCLGNBQUEsS0FBSSxDQUFDLE9BQUwsQ0FBYSxZQUFiOztBQUNBLGNBQUEsS0FBSSxDQUFDLFdBQUw7QUFDSCxhQUhELE1BR08sSUFBSSxLQUFLLEtBQUssUUFBZCxFQUF3QjtBQUMzQixjQUFBLEtBQUksQ0FBQyxPQUFMLENBQWEsWUFBYixHQUE0QixDQUE1Qjs7QUFDQSxjQUFBLEtBQUksQ0FBQyxXQUFMOztBQUNBLGNBQUEsS0FBSSxDQUFDLE9BQUwsQ0FBYSxJQUFiLENBQWtCLGFBQWxCLENBQWdDLElBQWhDLEVBQXNDLFNBQXRDLEdBQWtELENBQWxEO0FBQ0g7QUFDSjtBQUNKLFNBbEhFO0FBbUhILGtCQUFRLGlCQUFDLENBQUQsRUFBSSxFQUFKLEVBQVc7QUFDZixjQUFJLEtBQUksQ0FBQyxPQUFMLENBQWEsUUFBYixJQUF5QixLQUFJLENBQUMsT0FBTCxDQUFhLE9BQWIsQ0FBcUIsV0FBckIsQ0FBaUMsTUFBakMsR0FBMEMsQ0FBdkUsRUFBMEU7QUFDdEUsWUFBQSxLQUFJLENBQUMsT0FBTCxDQUFhLFFBQWI7QUFDSCxXQUZELE1BRU8sSUFBSSxLQUFJLENBQUMsT0FBTCxDQUFhLFFBQWpCLEVBQTJCO0FBQzlCLFlBQUEsS0FBSSxDQUFDLE9BQUwsQ0FBYSxXQUFiLENBQXlCLEVBQXpCO0FBQ0gsV0FGTSxNQUVBLElBQUksQ0FBQyxLQUFJLENBQUMsT0FBTCxDQUFhLFFBQWxCLEVBQTRCO0FBQy9CLGdCQUFNLElBQUksR0FBRyxLQUFJLENBQUMsT0FBTCxDQUFhLEtBQWIsQ0FBbUIsZ0NBQW5CLEVBQWI7O0FBQ0EsZ0JBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFMLENBQVcsR0FBWCxDQUFkOztBQUNBLGdCQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTixHQUFlLENBQWhCLENBQUwsQ0FBd0IsS0FBeEIsQ0FBOEIsR0FBOUIsRUFBbUMsTUFBbkMsR0FBNEMsQ0FBNUMsR0FBZ0QsQ0FBcEQsRUFBdUQ7QUFDbkQ7QUFDSDs7QUFDRCxnQkFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU4sR0FBZSxDQUFoQixDQUFMLENBQXdCLElBQXhCLEdBQStCLENBQS9CLE1BQXNDLEdBQTFDLEVBQStDO0FBQzNDO0FBQ0g7O0FBQ0QsWUFBQSxLQUFJLENBQUMsT0FBTCxDQUFhLFVBQWIsR0FBMEIsSUFBMUI7O0FBQ0EsWUFBQSxLQUFJLENBQUMsU0FBTCxHQUFpQixXQUFqQixDQUE2QixDQUE3QixFQUFnQyxFQUFoQyxFQUFvQyxLQUFJLENBQUMsT0FBTCxDQUFhLE9BQWIsQ0FBcUIsT0FBckIsSUFBZ0MsR0FBcEUsRUFBeUUsSUFBekU7QUFDSDtBQUNKO0FBcElFLE9BQVA7QUFzSUg7OztnQ0FFVyxLLEVBQU87QUFDZixVQUFJLEdBQUcsR0FBRyxLQUFLLE9BQUwsQ0FBYSxJQUFiLENBQWtCLGdCQUFsQixDQUFtQyxJQUFuQyxDQUFWO0FBQUEsVUFDSSxNQUFNLEdBQUcsR0FBRyxDQUFDLE1BQUosS0FBZSxDQUQ1QjtBQUdBLFVBQUksS0FBSixFQUFXLEtBQUssT0FBTCxDQUFhLFlBQWIsR0FBNEIsUUFBUSxDQUFDLEtBQUQsQ0FBcEM7O0FBRVgsV0FBSyxJQUFJLENBQUMsR0FBRyxDQUFiLEVBQWdCLENBQUMsR0FBRyxNQUFwQixFQUE0QixDQUFDLEVBQTdCLEVBQWlDO0FBQzdCLFlBQUksRUFBRSxHQUFHLEdBQUcsQ0FBQyxDQUFELENBQVo7O0FBQ0EsWUFBSSxDQUFDLEtBQUssS0FBSyxPQUFMLENBQWEsWUFBdkIsRUFBcUM7QUFDakMsVUFBQSxFQUFFLENBQUMsU0FBSCxDQUFhLEdBQWIsQ0FBaUIsS0FBSyxPQUFMLENBQWEsT0FBYixDQUFxQixVQUFyQixDQUFnQyxXQUFqRDtBQUVBLGNBQUksWUFBWSxHQUFHLEVBQUUsQ0FBQyxxQkFBSCxFQUFuQjtBQUNBLGNBQUksY0FBYyxHQUFHLEtBQUssT0FBTCxDQUFhLElBQWIsQ0FBa0IsYUFBbEIsQ0FBZ0MsSUFBaEMsRUFBc0MscUJBQXRDLEVBQXJCOztBQUVBLGNBQUksWUFBWSxDQUFDLE1BQWIsR0FBc0IsY0FBYyxDQUFDLE1BQXpDLEVBQWlEO0FBQzdDLGdCQUFJLGNBQWMsR0FBRyxZQUFZLENBQUMsTUFBYixHQUFzQixjQUFjLENBQUMsTUFBMUQ7QUFDQSxpQkFBSyxPQUFMLENBQWEsSUFBYixDQUFrQixhQUFsQixDQUFnQyxJQUFoQyxFQUFzQyxTQUF0QyxJQUFtRCxjQUFuRDtBQUNILFdBSEQsTUFHTyxJQUFJLFlBQVksQ0FBQyxHQUFiLEdBQW1CLGNBQWMsQ0FBQyxHQUF0QyxFQUEyQztBQUM5QyxnQkFBSSxlQUFjLEdBQUcsY0FBYyxDQUFDLEdBQWYsR0FBcUIsWUFBWSxDQUFDLEdBQXZEOztBQUNBLGlCQUFLLE9BQUwsQ0FBYSxJQUFiLENBQWtCLGFBQWxCLENBQWdDLElBQWhDLEVBQXNDLFNBQXRDLElBQW1ELGVBQW5EO0FBQ0g7QUFDSixTQWJELE1BYU87QUFDSCxVQUFBLEVBQUUsQ0FBQyxTQUFILENBQWEsTUFBYixDQUFvQixLQUFLLE9BQUwsQ0FBYSxPQUFiLENBQXFCLFVBQXJCLENBQWdDLFdBQXBEO0FBQ0g7QUFDSjtBQUNKOzs7a0NBRWEsSSxFQUFNLGEsRUFBZTtBQUMvQixVQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMscUJBQUwsR0FBNkIsTUFBMUM7O0FBRUEsVUFBSSxhQUFKLEVBQW1CO0FBQ2YsWUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFlBQUwsSUFBcUIsTUFBTSxDQUFDLGdCQUFQLENBQXdCLElBQXhCLENBQWpDO0FBQ0EsZUFBTyxNQUFNLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxTQUFQLENBQW5CLEdBQXVDLFVBQVUsQ0FBQyxLQUFLLENBQUMsWUFBUCxDQUF4RDtBQUNIOztBQUVELGFBQU8sTUFBUDtBQUNIOzs7MkJBeFphO0FBQ1YsYUFBTyxDQUNIO0FBQ0ksUUFBQSxHQUFHLEVBQUUsQ0FEVDtBQUVJLFFBQUEsS0FBSyxFQUFFO0FBRlgsT0FERyxFQUtIO0FBQ0ksUUFBQSxHQUFHLEVBQUUsQ0FEVDtBQUVJLFFBQUEsS0FBSyxFQUFFO0FBRlgsT0FMRyxFQVNIO0FBQ0ksUUFBQSxHQUFHLEVBQUUsRUFEVDtBQUVJLFFBQUEsS0FBSyxFQUFFO0FBRlgsT0FURyxFQWFIO0FBQ0ksUUFBQSxHQUFHLEVBQUUsRUFEVDtBQUVJLFFBQUEsS0FBSyxFQUFFO0FBRlgsT0FiRyxFQWlCSDtBQUNJLFFBQUEsR0FBRyxFQUFFLEVBRFQ7QUFFSSxRQUFBLEtBQUssRUFBRTtBQUZYLE9BakJHLEVBcUJIO0FBQ0ksUUFBQSxHQUFHLEVBQUUsRUFEVDtBQUVJLFFBQUEsS0FBSyxFQUFFO0FBRlgsT0FyQkcsRUF5Qkg7QUFDSSxRQUFBLEdBQUcsRUFBRSxFQURUO0FBRUksUUFBQSxLQUFLLEVBQUU7QUFGWCxPQXpCRyxFQTZCSDtBQUNJLFFBQUEsR0FBRyxFQUFFLEdBRFQ7QUFFSSxRQUFBLEtBQUssRUFBRTtBQUZYLE9BN0JHLENBQVA7QUFrQ0g7OzsyQkFFYSxJLEVBQU07QUFDaEIsVUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLE1BQWpCLEVBQXlCO0FBQ3JCLFFBQUEsSUFBSSxDQUFDLE1BQUw7QUFDQTtBQUNIOztBQUNELFVBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQWxCLEVBQTBCO0FBQ3RCLFFBQUEsSUFBSSxDQUFDLFVBQUwsQ0FBZ0IsV0FBaEIsQ0FBNEIsSUFBNUI7QUFDSDtBQUNKOzs7Ozs7ZUE4V1UsYTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDamFULGlCO0FBQ0YsNkJBQVksT0FBWixFQUFxQjtBQUFBOztBQUNqQixTQUFLLE9BQUwsR0FBZSxPQUFmO0FBQ0EsU0FBSyxPQUFMLENBQWEsVUFBYixHQUEwQixJQUExQjtBQUNBLFNBQUssSUFBTCxHQUFZLEtBQUssT0FBTCxDQUFhLElBQXpCO0FBQ0gsRyxDQUVEOzs7Ozt5QkFDSyxJLEVBQU07QUFBQTs7QUFDUCxXQUFLLGNBQUwsR0FBc0IsS0FBSyxPQUFMLENBQWEsTUFBYixDQUFvQixLQUFwQixDQUEwQixJQUExQixDQUErQixJQUEvQixFQUFxQyxJQUFyQyxDQUF0QjtBQUNBLFdBQUssd0JBQUwsR0FBZ0MsS0FBSyxRQUFMLENBQzVCLFlBQU07QUFDRixZQUFJLEtBQUksQ0FBQyxPQUFMLENBQWEsUUFBakIsRUFBMkI7QUFDdkIsVUFBQSxLQUFJLENBQUMsT0FBTCxDQUFhLFdBQWIsQ0FBeUIsS0FBSSxDQUFDLE9BQUwsQ0FBYSxPQUFiLENBQXFCLE9BQTlDLEVBQXVELEtBQXZEO0FBQ0g7QUFDSixPQUwyQixFQU01QixHQU40QixFQU81QixLQVA0QixDQUFoQztBQVNBLFdBQUssaUJBQUwsR0FBeUIsS0FBSyxRQUFMLENBQ3JCLFlBQU07QUFDRixZQUFJLEtBQUksQ0FBQyxPQUFMLENBQWEsUUFBakIsRUFBMkI7QUFDdkIsVUFBQSxLQUFJLENBQUMsT0FBTCxDQUFhLEtBQWIsQ0FBbUIsbUJBQW5CLENBQXVDLElBQXZDO0FBQ0g7QUFDSixPQUxvQixFQU1yQixHQU5xQixFQU9yQixLQVBxQixDQUF6QixDQVhPLENBcUJQOztBQUNBLFdBQUssT0FBTCxDQUFhLEtBQWIsQ0FBbUIsV0FBbkIsR0FBaUMsZ0JBQWpDLENBQWtELGVBQWxELEVBQW1FLEtBQUssY0FBeEUsRUFBd0YsS0FBeEY7QUFDQSxXQUFLLE9BQUwsQ0FBYSxLQUFiLENBQW1CLFdBQW5CLEdBQWlDLGdCQUFqQyxDQUFrRCxXQUFsRCxFQUErRCxLQUFLLGNBQXBFLEVBQW9GLEtBQXBGO0FBQ0EsTUFBQSxNQUFNLENBQUMsZ0JBQVAsQ0FBd0IsUUFBeEIsRUFBa0MsS0FBSyxpQkFBdkM7O0FBRUEsVUFBSSxLQUFLLGFBQVQsRUFBd0I7QUFDcEIsYUFBSyxhQUFMLENBQW1CLGdCQUFuQixDQUFvQyxRQUFwQyxFQUE4QyxLQUFLLHdCQUFuRCxFQUE2RSxLQUE3RTtBQUNILE9BRkQsTUFFTztBQUNILFFBQUEsTUFBTSxDQUFDLGdCQUFQLENBQXdCLFFBQXhCLEVBQWtDLEtBQUssd0JBQXZDO0FBQ0g7QUFDSixLLENBRUQ7Ozs7MkJBQ08sSSxFQUFNO0FBQ1QsV0FBSyxPQUFMLENBQWEsS0FBYixDQUFtQixXQUFuQixHQUFpQyxtQkFBakMsQ0FBcUQsV0FBckQsRUFBa0UsS0FBSyxjQUF2RSxFQUF1RixLQUF2RjtBQUNBLFdBQUssT0FBTCxDQUFhLEtBQWIsQ0FBbUIsV0FBbkIsR0FBaUMsbUJBQWpDLENBQXFELGVBQXJELEVBQXNFLEtBQUssY0FBM0UsRUFBMkYsS0FBM0Y7QUFDQSxNQUFBLE1BQU0sQ0FBQyxtQkFBUCxDQUEyQixRQUEzQixFQUFxQyxLQUFLLGlCQUExQzs7QUFFQSxVQUFJLEtBQUssYUFBVCxFQUF3QjtBQUNwQixhQUFLLGFBQUwsQ0FBbUIsbUJBQW5CLENBQXVDLFFBQXZDLEVBQWlELEtBQUssd0JBQXRELEVBQWdGLEtBQWhGO0FBQ0gsT0FGRCxNQUVPO0FBQ0gsUUFBQSxNQUFNLENBQUMsbUJBQVAsQ0FBMkIsUUFBM0IsRUFBcUMsS0FBSyx3QkFBMUM7QUFDSDtBQUNKOzs7NkJBRVEsSSxFQUFNLEksRUFBTSxTLEVBQVc7QUFBQTtBQUFBOztBQUM1QixVQUFJLE9BQUo7QUFDQSxhQUFPLFlBQU07QUFDVCxZQUFJLE9BQU8sR0FBRyxNQUFkO0FBQUEsWUFDSSxJQUFJLEdBQUcsVUFEWDs7QUFFQSxZQUFJLEtBQUssR0FBRyxTQUFSLEtBQVEsR0FBTTtBQUNkLFVBQUEsT0FBTyxHQUFHLElBQVY7QUFDQSxjQUFJLENBQUMsU0FBTCxFQUFnQixJQUFJLENBQUMsS0FBTCxDQUFXLE9BQVgsRUFBb0IsSUFBcEI7QUFDbkIsU0FIRDs7QUFJQSxZQUFJLE9BQU8sR0FBRyxTQUFTLElBQUksQ0FBQyxPQUE1QjtBQUNBLFFBQUEsWUFBWSxDQUFDLE9BQUQsQ0FBWjtBQUNBLFFBQUEsT0FBTyxHQUFHLFVBQVUsQ0FBQyxLQUFELEVBQVEsSUFBUixDQUFwQjtBQUNBLFlBQUksT0FBSixFQUFhLElBQUksQ0FBQyxLQUFMLENBQVcsT0FBWCxFQUFvQixJQUFwQjtBQUNoQixPQVhEO0FBWUg7Ozs7OztlQUdVLGlCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN2RWY7SUFDTSxZO0FBQ0Ysd0JBQVksT0FBWixFQUFxQjtBQUFBOztBQUNqQixTQUFLLE9BQUwsR0FBZSxPQUFmO0FBQ0EsU0FBSyxPQUFMLENBQWEsS0FBYixHQUFxQixJQUFyQjtBQUNIOzs7O2tDQUVhO0FBQ1YsVUFBSSxNQUFKOztBQUNBLFVBQUksS0FBSyxPQUFMLENBQWEsT0FBYixDQUFxQixVQUF6QixFQUFxQztBQUNqQyxRQUFBLE1BQU0sR0FBRyxLQUFLLE9BQUwsQ0FBYSxPQUFiLENBQXFCLFVBQXJCLENBQWdDLE1BQXpDO0FBQ0g7O0FBRUQsVUFBSSxDQUFDLE1BQUwsRUFBYTtBQUNULGVBQU8sUUFBUDtBQUNIOztBQUVELGFBQU8sTUFBTSxDQUFDLGFBQVAsQ0FBcUIsUUFBNUI7QUFDSDs7O3dDQUVtQixRLEVBQVU7QUFBQTs7QUFDMUIsVUFBSSxPQUFPLEdBQUcsS0FBSyxPQUFMLENBQWEsT0FBM0I7QUFBQSxVQUNJLFdBREo7QUFHQSxVQUFJLElBQUksR0FBRyxLQUFLLGNBQUwsQ0FDUCxLQURPLEVBRVAsS0FBSyxPQUFMLENBQWEsZ0JBRk4sRUFHUCxJQUhPLEVBSVAsS0FBSyxPQUFMLENBQWEsV0FKTixFQUtQLEtBQUssT0FBTCxDQUFhLGdCQUxOLENBQVg7O0FBUUEsVUFBSSxPQUFPLElBQVAsS0FBZ0IsV0FBcEIsRUFBaUM7QUFDN0IsWUFBSSxDQUFDLEtBQUssT0FBTCxDQUFhLFlBQWxCLEVBQWdDO0FBQzVCLGVBQUssT0FBTCxDQUFhLElBQWIsQ0FBa0IsS0FBbEIsQ0FBd0IsT0FBeEI7QUFDQTtBQUNIOztBQUVELFlBQUksQ0FBQyxLQUFLLGlCQUFMLENBQXVCLE9BQU8sQ0FBQyxPQUEvQixDQUFMLEVBQThDO0FBQzFDLFVBQUEsV0FBVyxHQUFHLEtBQUssbUNBQUwsQ0FDVixLQUFLLE9BQUwsQ0FBYSxPQUFiLENBQXFCLE9BRFgsRUFFVixJQUFJLENBQUMsZUFGSyxDQUFkO0FBSUgsU0FMRCxNQUtPO0FBQ0gsVUFBQSxXQUFXLEdBQUcsS0FBSywrQkFBTCxDQUFxQyxJQUFJLENBQUMsZUFBMUMsQ0FBZDtBQUNIOztBQUVELGFBQUssT0FBTCxDQUFhLElBQWIsQ0FBa0IsS0FBbEIsQ0FBd0IsT0FBeEIsa0JBQTBDLFdBQVcsQ0FBQyxHQUF0RCw2REFDaUMsV0FBVyxDQUFDLElBRDdDLDhEQUVrQyxXQUFXLENBQUMsS0FGOUMsK0RBR21DLFdBQVcsQ0FBQyxNQUgvQzs7QUFRQSxZQUFJLFdBQVcsQ0FBQyxJQUFaLEtBQXFCLE1BQXpCLEVBQWlDO0FBQzdCLGVBQUssT0FBTCxDQUFhLElBQWIsQ0FBa0IsS0FBbEIsQ0FBd0IsSUFBeEIsR0FBK0IsTUFBL0I7QUFDSDs7QUFFRCxZQUFJLFdBQVcsQ0FBQyxHQUFaLEtBQW9CLE1BQXhCLEVBQWdDO0FBQzVCLGVBQUssT0FBTCxDQUFhLElBQWIsQ0FBa0IsS0FBbEIsQ0FBd0IsR0FBeEIsR0FBOEIsTUFBOUI7QUFDSDs7QUFFRCxZQUFJLFFBQUosRUFBYyxLQUFLLGNBQUw7QUFFZCxRQUFBLE1BQU0sQ0FBQyxVQUFQLENBQWtCLFlBQU07QUFDcEIsY0FBSSxjQUFjLEdBQUc7QUFDakIsWUFBQSxLQUFLLEVBQUUsS0FBSSxDQUFDLE9BQUwsQ0FBYSxJQUFiLENBQWtCLFdBRFI7QUFFakIsWUFBQSxNQUFNLEVBQUUsS0FBSSxDQUFDLE9BQUwsQ0FBYSxJQUFiLENBQWtCO0FBRlQsV0FBckI7O0FBSUEsY0FBSSxlQUFlLEdBQUcsS0FBSSxDQUFDLGVBQUwsQ0FBcUIsV0FBckIsRUFBa0MsY0FBbEMsQ0FBdEI7O0FBRUEsY0FBSSwyQkFBMkIsR0FDM0IsTUFBTSxDQUFDLFVBQVAsR0FBb0IsY0FBYyxDQUFDLEtBQW5DLEtBQTZDLGVBQWUsQ0FBQyxJQUFoQixJQUF3QixlQUFlLENBQUMsS0FBckYsQ0FESjtBQUVBLGNBQUkseUJBQXlCLEdBQ3pCLE1BQU0sQ0FBQyxXQUFQLEdBQXFCLGNBQWMsQ0FBQyxNQUFwQyxLQUErQyxlQUFlLENBQUMsR0FBaEIsSUFBdUIsZUFBZSxDQUFDLE1BQXRGLENBREo7O0FBRUEsY0FBSSwyQkFBMkIsSUFBSSx5QkFBbkMsRUFBOEQ7QUFDMUQsWUFBQSxLQUFJLENBQUMsT0FBTCxDQUFhLElBQWIsQ0FBa0IsS0FBbEIsQ0FBd0IsT0FBeEIsR0FBa0MsZUFBbEM7O0FBQ0EsWUFBQSxLQUFJLENBQUMsbUJBQUwsQ0FBeUIsUUFBekI7QUFDSDtBQUNKLFNBZkQsRUFlRyxDQWZIO0FBZ0JILE9BakRELE1BaURPO0FBQ0gsYUFBSyxPQUFMLENBQWEsSUFBYixDQUFrQixLQUFsQixDQUF3QixPQUF4QixHQUFrQyxlQUFsQztBQUNIO0FBQ0o7OztrQ0FFYSxhLEVBQWUsSSxFQUFNLE0sRUFBUTtBQUN2QyxVQUFJLEtBQUo7QUFDQSxVQUFJLElBQUksR0FBRyxhQUFYOztBQUVBLFVBQUksSUFBSixFQUFVO0FBQ04sYUFBSyxJQUFJLENBQUMsR0FBRyxDQUFiLEVBQWdCLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBekIsRUFBaUMsQ0FBQyxFQUFsQyxFQUFzQztBQUNsQyxVQUFBLElBQUksR0FBRyxJQUFJLENBQUMsVUFBTCxDQUFnQixJQUFJLENBQUMsQ0FBRCxDQUFwQixDQUFQOztBQUNBLGNBQUksSUFBSSxLQUFLLFNBQWIsRUFBd0I7QUFDcEI7QUFDSDs7QUFDRCxpQkFBTyxJQUFJLENBQUMsTUFBTCxHQUFjLE1BQXJCLEVBQTZCO0FBQ3pCLFlBQUEsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFmO0FBQ0EsWUFBQSxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVo7QUFDSDs7QUFDRCxjQUFJLElBQUksQ0FBQyxVQUFMLENBQWdCLE1BQWhCLEtBQTJCLENBQTNCLElBQWdDLENBQUMsSUFBSSxDQUFDLE1BQTFDLEVBQWtEO0FBQzlDLFlBQUEsSUFBSSxHQUFHLElBQUksQ0FBQyxlQUFaO0FBQ0g7QUFDSjtBQUNKOztBQUNELFVBQUksR0FBRyxHQUFHLEtBQUssa0JBQUwsRUFBVjtBQUVBLE1BQUEsS0FBSyxHQUFHLEtBQUssV0FBTCxHQUFtQixXQUFuQixFQUFSO0FBQ0EsTUFBQSxLQUFLLENBQUMsUUFBTixDQUFlLElBQWYsRUFBcUIsTUFBckI7QUFDQSxNQUFBLEtBQUssQ0FBQyxNQUFOLENBQWEsSUFBYixFQUFtQixNQUFuQjtBQUNBLE1BQUEsS0FBSyxDQUFDLFFBQU4sQ0FBZSxJQUFmOztBQUVBLFVBQUk7QUFDQSxRQUFBLEdBQUcsQ0FBQyxlQUFKLEdBREEsQ0FFQTtBQUNILE9BSEQsQ0FHRSxPQUFPLEtBQVAsRUFBYyxDQUFFOztBQUVsQixNQUFBLEdBQUcsQ0FBQyxRQUFKLENBQWEsS0FBYjtBQUNBLE1BQUEsYUFBYSxDQUFDLEtBQWQ7QUFDSDs7O3VDQUVrQixJLEVBQU0sbUIsRUFBcUIsZ0IsRUFBa0IsYSxFQUFlLEksRUFBTTtBQUNqRixVQUFJLElBQUksR0FBRyxLQUFLLGNBQUwsQ0FDUCxJQURPLEVBRVAsZ0JBRk8sRUFHUCxtQkFITyxFQUlQLEtBQUssT0FBTCxDQUFhLFdBSk4sRUFLUCxLQUFLLE9BQUwsQ0FBYSxnQkFMTixDQUFYOztBQVFBLFVBQUksSUFBSSxLQUFLLFNBQWIsRUFBd0I7QUFDcEIsWUFBSSxPQUFPLEdBQUcsS0FBSyxPQUFMLENBQWEsT0FBM0I7QUFDQSxZQUFJLFlBQVksR0FBRyxJQUFJLFdBQUosQ0FBZ0Isa0JBQWhCLEVBQW9DO0FBQ25ELFVBQUEsTUFBTSxFQUFFO0FBQ0osWUFBQSxJQUFJLEVBQUUsSUFERjtBQUVKLFlBQUEsUUFBUSxFQUFFLE9BRk47QUFHSixZQUFBLE9BQU8sRUFBRSxJQUhMO0FBSUosWUFBQSxLQUFLLEVBQUU7QUFKSDtBQUQyQyxTQUFwQyxDQUFuQjs7QUFTQSxZQUFJLENBQUMsS0FBSyxpQkFBTCxDQUF1QixPQUFPLENBQUMsT0FBL0IsQ0FBTCxFQUE4QztBQUMxQyxjQUFJLE9BQU8sR0FBRyxLQUFLLE9BQUwsQ0FBYSxPQUFiLENBQXFCLE9BQW5DO0FBQ0EsY0FBSSxVQUFVLEdBQ1YsT0FBTyxLQUFLLE9BQUwsQ0FBYSxpQkFBcEIsSUFBeUMsUUFBekMsR0FBb0QsS0FBSyxPQUFMLENBQWEsaUJBQWpFLEdBQXFGLEdBRHpGO0FBRUEsVUFBQSxJQUFJLElBQUksVUFBUjtBQUNBLGNBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxlQUFwQjtBQUNBLGNBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxlQUFMLEdBQXVCLElBQUksQ0FBQyxXQUFMLENBQWlCLE1BQXhDLEdBQWlELFVBQVUsQ0FBQyxNQUF6RTtBQUNBLFVBQUEsT0FBTyxDQUFDLEtBQVIsR0FDSSxPQUFPLENBQUMsS0FBUixDQUFjLFNBQWQsQ0FBd0IsQ0FBeEIsRUFBMkIsUUFBM0IsSUFBdUMsSUFBdkMsR0FBOEMsT0FBTyxDQUFDLEtBQVIsQ0FBYyxTQUFkLENBQXdCLE1BQXhCLEVBQWdDLE9BQU8sQ0FBQyxLQUFSLENBQWMsTUFBOUMsQ0FEbEQ7QUFFQSxVQUFBLE9BQU8sQ0FBQyxjQUFSLEdBQXlCLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBekM7QUFDQSxVQUFBLE9BQU8sQ0FBQyxZQUFSLEdBQXVCLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBdkM7QUFDSCxTQVhELE1BV087QUFDSDtBQUNBLGNBQUksV0FBVSxHQUNWLE9BQU8sS0FBSyxPQUFMLENBQWEsaUJBQXBCLElBQXlDLFFBQXpDLEdBQW9ELEtBQUssT0FBTCxDQUFhLGlCQUFqRSxHQUFxRixNQUR6Rjs7QUFHQSxjQUFJLGFBQWEsQ0FBQyxPQUFkLEtBQTBCLEdBQTlCLEVBQW1DO0FBQy9CLFlBQUEsV0FBVSxHQUFHLE1BQU0sV0FBbkI7QUFDSDs7QUFDRCxVQUFBLElBQUksSUFBSSxXQUFSO0FBQ0EsZUFBSyxTQUFMLENBQ0ksSUFESixFQUVJLElBQUksQ0FBQyxlQUZULEVBR0ksSUFBSSxDQUFDLGVBQUwsR0FBdUIsSUFBSSxDQUFDLFdBQUwsQ0FBaUIsTUFBeEMsR0FBaUQsQ0FBQyxLQUFLLE9BQUwsQ0FBYSxnQkFIbkU7QUFLSDs7QUFFRCxRQUFBLE9BQU8sQ0FBQyxPQUFSLENBQWdCLGFBQWhCLENBQThCLFlBQTlCO0FBQ0g7QUFDSjs7OzhCQUVTLEksRUFBTSxRLEVBQVUsTSxFQUFRO0FBQzlCLFVBQUksS0FBSixFQUFXLEdBQVgsRUFBZ0IsS0FBaEI7QUFDQSxNQUFBLEdBQUcsR0FBRyxLQUFLLGtCQUFMLEVBQU47QUFGOEIsaUJBR08sR0FIUDtBQUFBLFVBR3RCLFlBSHNCLFFBR3RCLFlBSHNCO0FBQUEsVUFHUixVQUhRLFFBR1IsVUFIUTs7QUFJOUIsVUFBSSxVQUFVLENBQUMsVUFBWCxJQUF5QixVQUFVLENBQUMsVUFBWCxDQUFzQixZQUF0QixDQUE3QixFQUFrRTtBQUM5RCxZQUFNLEtBQUssR0FBRyxVQUFVLENBQUMsVUFBWCxDQUFzQixZQUF0QixDQUFkOztBQUNBLFlBQUksS0FBSyxDQUFDLFFBQU4sS0FBbUIsSUFBSSxDQUFDLFNBQTVCLEVBQXVDO0FBQ25DLFVBQUEsS0FBSyxHQUFHLEtBQVI7QUFDSCxTQUZELE1BRU8sSUFBSSxZQUFZLEdBQUcsQ0FBZixJQUFvQixLQUFLLENBQUMsUUFBTixLQUFtQixJQUFJLENBQUMsWUFBaEQsRUFBOEQ7QUFDakUsVUFBQSxLQUFLLEdBQUcsVUFBVSxDQUFDLFVBQVgsQ0FBc0IsWUFBWSxHQUFHLENBQXJDLENBQVI7QUFDSDtBQUNKLE9BUEQsTUFPTztBQUNILFFBQUEsS0FBSyxHQUFHLEdBQUcsQ0FBQyxVQUFaO0FBQ0g7O0FBQ0QsTUFBQSxLQUFLLEdBQUcsS0FBSyxXQUFMLEdBQW1CLFdBQW5CLEVBQVI7QUFDQSxNQUFBLEtBQUssQ0FBQyxRQUFOLENBQWUsS0FBZixFQUFzQixRQUF0QjtBQUNBLE1BQUEsS0FBSyxDQUFDLE1BQU4sQ0FBYSxLQUFiLEVBQW9CLE1BQXBCO0FBQ0EsTUFBQSxLQUFLLENBQUMsY0FBTjtBQUVBLFVBQUksRUFBRSxHQUFHLEtBQUssV0FBTCxHQUFtQixhQUFuQixDQUFpQyxLQUFqQyxDQUFUO0FBQ0EsTUFBQSxFQUFFLENBQUMsU0FBSCxHQUFlLElBQWY7QUFDQSxVQUFJLElBQUksR0FBRyxLQUFLLFdBQUwsR0FBbUIsc0JBQW5CLEVBQVg7QUFBQSxVQUNJLElBREo7QUFBQSxVQUVJLFFBRko7O0FBR0EsYUFBUSxJQUFJLEdBQUcsRUFBRSxDQUFDLFVBQWxCLEVBQStCO0FBQzNCLFFBQUEsUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFMLENBQWlCLElBQWpCLENBQVg7QUFDSDs7QUFDRCxNQUFBLEtBQUssQ0FBQyxVQUFOLENBQWlCLElBQWpCLEVBM0I4QixDQTZCOUI7O0FBQ0EsVUFBSSxRQUFKLEVBQWM7QUFDVixRQUFBLEtBQUssR0FBRyxLQUFLLENBQUMsVUFBTixFQUFSO0FBQ0EsUUFBQSxLQUFLLENBQUMsYUFBTixDQUFvQixRQUFwQjtBQUNBLFFBQUEsS0FBSyxDQUFDLFFBQU4sQ0FBZSxJQUFmO0FBQ0EsUUFBQSxHQUFHLENBQUMsZUFBSjtBQUNBLFFBQUEsR0FBRyxDQUFDLFFBQUosQ0FBYSxLQUFiO0FBQ0g7QUFDSjs7O3lDQUVvQjtBQUNqQixVQUFJLEtBQUssT0FBTCxDQUFhLFVBQWIsQ0FBd0IsTUFBNUIsRUFBb0M7QUFDaEMsZUFBTyxLQUFLLE9BQUwsQ0FBYSxVQUFiLENBQXdCLE1BQXhCLENBQStCLGFBQS9CLENBQTZDLFlBQTdDLEVBQVA7QUFDSDs7QUFFRCxhQUFPLE1BQU0sQ0FBQyxZQUFQLEVBQVA7QUFDSDs7OzRDQUV1QixPLEVBQVM7QUFDN0IsVUFBSSxPQUFPLENBQUMsVUFBUixLQUF1QixJQUEzQixFQUFpQztBQUM3QixlQUFPLENBQVA7QUFDSDs7QUFFRCxXQUFLLElBQUksQ0FBQyxHQUFHLENBQWIsRUFBZ0IsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxVQUFSLENBQW1CLFVBQW5CLENBQThCLE1BQWxELEVBQTBELENBQUMsRUFBM0QsRUFBK0Q7QUFDM0QsWUFBSSxJQUFJLEdBQUcsT0FBTyxDQUFDLFVBQVIsQ0FBbUIsVUFBbkIsQ0FBOEIsQ0FBOUIsQ0FBWDs7QUFFQSxZQUFJLElBQUksS0FBSyxPQUFiLEVBQXNCO0FBQ2xCLGlCQUFPLENBQVA7QUFDSDtBQUNKO0FBQ0osSyxDQUVEOzs7O21EQUMrQixHLEVBQUs7QUFDaEMsVUFBSSxHQUFHLEdBQUcsS0FBSyxrQkFBTCxFQUFWO0FBQ0EsVUFBSSxRQUFRLEdBQUcsR0FBRyxDQUFDLFVBQW5CO0FBQ0EsVUFBSSxJQUFJLEdBQUcsRUFBWDtBQUNBLFVBQUksTUFBSjs7QUFFQSxVQUFJLFFBQVEsSUFBSSxJQUFoQixFQUFzQjtBQUNsQixZQUFJLENBQUo7QUFDQSxZQUFJLEVBQUUsR0FBRyxRQUFRLENBQUMsZUFBbEI7O0FBQ0EsZUFBTyxRQUFRLEtBQUssSUFBYixJQUFxQixFQUFFLEtBQUssTUFBbkMsRUFBMkM7QUFDdkMsVUFBQSxDQUFDLEdBQUcsS0FBSyx1QkFBTCxDQUE2QixRQUE3QixDQUFKO0FBQ0EsVUFBQSxJQUFJLENBQUMsSUFBTCxDQUFVLENBQVY7QUFDQSxVQUFBLFFBQVEsR0FBRyxRQUFRLENBQUMsVUFBcEI7O0FBQ0EsY0FBSSxRQUFRLEtBQUssSUFBakIsRUFBdUI7QUFDbkIsWUFBQSxFQUFFLEdBQUcsUUFBUSxDQUFDLGVBQWQ7QUFDSDtBQUNKOztBQUNELFFBQUEsSUFBSSxDQUFDLE9BQUwsR0FYa0IsQ0FhbEI7O0FBQ0EsUUFBQSxNQUFNLEdBQUcsR0FBRyxDQUFDLFVBQUosQ0FBZSxDQUFmLEVBQWtCLFdBQTNCO0FBRUEsZUFBTztBQUNILFVBQUEsUUFBUSxFQUFFLFFBRFA7QUFFSCxVQUFBLElBQUksRUFBRSxJQUZIO0FBR0gsVUFBQSxNQUFNLEVBQUU7QUFITCxTQUFQO0FBS0g7QUFDSjs7O3VEQUVrQztBQUMvQixVQUFJLE9BQU8sR0FBRyxLQUFLLE9BQUwsQ0FBYSxPQUEzQjtBQUFBLFVBQ0ksSUFBSSxHQUFHLEVBRFg7O0FBR0EsVUFBSSxDQUFDLEtBQUssaUJBQUwsQ0FBdUIsT0FBTyxDQUFDLE9BQS9CLENBQUwsRUFBOEM7QUFDMUMsWUFBSSxhQUFhLEdBQUcsS0FBSyxPQUFMLENBQWEsT0FBYixDQUFxQixPQUF6Qzs7QUFDQSxZQUFJLGFBQUosRUFBbUI7QUFDZixjQUFJLFFBQVEsR0FBRyxhQUFhLENBQUMsY0FBN0I7O0FBQ0EsY0FBSSxhQUFhLENBQUMsS0FBZCxJQUF1QixRQUFRLElBQUksQ0FBdkMsRUFBMEM7QUFDdEMsWUFBQSxJQUFJLEdBQUcsYUFBYSxDQUFDLEtBQWQsQ0FBb0IsU0FBcEIsQ0FBOEIsQ0FBOUIsRUFBaUMsUUFBakMsQ0FBUDtBQUNIO0FBQ0o7QUFDSixPQVJELE1BUU87QUFDSCxZQUFJLFlBQVksR0FBRyxLQUFLLGtCQUFMLEdBQTBCLFVBQTdDOztBQUVBLFlBQUksWUFBWSxJQUFJLElBQXBCLEVBQTBCO0FBQ3RCLGNBQU0sWUFBWSxHQUFHLEtBQUssa0JBQUwsR0FBMEIsWUFBL0M7O0FBQ0EsY0FBSSxZQUFZLENBQUMsVUFBYixJQUEyQixZQUFZLENBQUMsVUFBYixDQUF3QixZQUF4QixDQUEvQixFQUFzRTtBQUNsRSxnQkFBTSxLQUFLLEdBQUcsWUFBWSxDQUFDLFVBQWIsQ0FBd0IsWUFBeEIsQ0FBZDs7QUFDQSxnQkFBSSxLQUFLLENBQUMsUUFBTixLQUFtQixJQUFJLENBQUMsU0FBNUIsRUFBdUM7QUFDbkMsY0FBQSxJQUFJLEdBQUcsS0FBSyxDQUFDLFdBQWI7QUFDSCxhQUZELE1BRU8sSUFBSSxZQUFZLEdBQUcsQ0FBZixJQUFvQixLQUFLLENBQUMsUUFBTixLQUFtQixJQUFJLENBQUMsWUFBaEQsRUFBOEQ7QUFDakUsY0FBQSxJQUFJLElBQUksWUFBWSxDQUFDLFVBQWIsQ0FBd0IsWUFBWSxHQUFHLENBQXZDLEVBQTBDLFdBQWxEO0FBQ0g7QUFDSixXQVBELE1BT087QUFDSCxnQkFBSSxrQkFBa0IsR0FBRyxZQUFZLENBQUMsV0FBdEM7QUFDQSxnQkFBSSxpQkFBaUIsR0FBRyxLQUFLLGtCQUFMLEdBQTBCLFVBQTFCLENBQXFDLENBQXJDLEVBQXdDLFdBQWhFOztBQUVBLGdCQUFJLGtCQUFrQixJQUFJLGlCQUFpQixJQUFJLENBQS9DLEVBQWtEO0FBQzlDLGNBQUEsSUFBSSxHQUFHLGtCQUFrQixDQUFDLFNBQW5CLENBQTZCLENBQTdCLEVBQWdDLGlCQUFoQyxDQUFQO0FBQ0g7QUFDSjtBQUNKO0FBQ0o7O0FBQ0QsYUFBTyxJQUFQO0FBQ0g7OztzQ0FFaUIsSSxFQUFNO0FBQ3BCLE1BQUEsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFMLENBQWEsU0FBYixFQUF3QixHQUF4QixDQUFQLENBRG9CLENBQ2lCOztBQUNyQyxVQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsS0FBTCxDQUFXLEdBQVgsQ0FBakI7QUFDQSxVQUFJLFdBQVcsR0FBRyxVQUFVLENBQUMsTUFBWCxHQUFvQixDQUF0QztBQUNBLGFBQU8sVUFBVSxDQUFDLFdBQUQsQ0FBVixDQUF3QixJQUF4QixFQUFQO0FBQ0g7OzttQ0FFYyxpQixFQUFtQixnQixFQUFrQixtQixFQUFxQixXLEVBQWEsYyxFQUFnQjtBQUFBOztBQUNsRyxVQUFJLEdBQUcsR0FBRyxLQUFLLE9BQUwsQ0FBYSxPQUF2QjtBQUNBLFVBQUksUUFBSixFQUFjLElBQWQsRUFBb0IsTUFBcEI7O0FBRUEsVUFBSSxDQUFDLEtBQUssaUJBQUwsQ0FBdUIsR0FBRyxDQUFDLE9BQTNCLENBQUwsRUFBMEM7QUFDdEMsUUFBQSxRQUFRLEdBQUcsS0FBSyxPQUFMLENBQWEsT0FBYixDQUFxQixPQUFoQztBQUNILE9BRkQsTUFFTztBQUNILFlBQUksYUFBYSxHQUFHLEtBQUssOEJBQUwsQ0FBb0MsR0FBcEMsQ0FBcEI7O0FBRUEsWUFBSSxhQUFKLEVBQW1CO0FBQ2YsVUFBQSxRQUFRLEdBQUcsYUFBYSxDQUFDLFFBQXpCO0FBQ0EsVUFBQSxJQUFJLEdBQUcsYUFBYSxDQUFDLElBQXJCO0FBQ0EsVUFBQSxNQUFNLEdBQUcsYUFBYSxDQUFDLE1BQXZCO0FBQ0g7QUFDSjs7QUFFRCxVQUFJLGNBQWMsR0FBRyxLQUFLLGdDQUFMLEVBQXJCO0FBQ0EsVUFBSSx3QkFBd0IsR0FBRyxLQUFLLGlCQUFMLENBQXVCLGNBQXZCLENBQS9COztBQUVBLFVBQUksY0FBSixFQUFvQjtBQUNoQixlQUFPO0FBQ0gsVUFBQSxlQUFlLEVBQUUsY0FBYyxDQUFDLE1BQWYsR0FBd0Isd0JBQXdCLENBQUMsTUFEL0Q7QUFFSCxVQUFBLFdBQVcsRUFBRSx3QkFGVjtBQUdILFVBQUEsc0JBQXNCLEVBQUUsUUFIckI7QUFJSCxVQUFBLG1CQUFtQixFQUFFLElBSmxCO0FBS0gsVUFBQSxxQkFBcUIsRUFBRTtBQUxwQixTQUFQO0FBT0g7O0FBRUQsVUFBSSxjQUFjLEtBQUssU0FBbkIsSUFBZ0MsY0FBYyxLQUFLLElBQXZELEVBQTZEO0FBQ3pELFlBQUksd0JBQXdCLEdBQUcsQ0FBQyxDQUFoQztBQUNBLFlBQUksV0FBSjtBQUVBLGFBQUssT0FBTCxDQUFhLFVBQWIsQ0FBd0IsT0FBeEIsQ0FBZ0MsVUFBQSxNQUFNLEVBQUk7QUFDdEMsY0FBSSxDQUFDLEdBQUcsTUFBTSxDQUFDLE9BQWY7QUFDQSxjQUFJLEdBQUcsR0FBRyxNQUFNLENBQUMsbUJBQVAsR0FDSixNQUFJLENBQUMseUJBQUwsQ0FBK0IsY0FBL0IsRUFBK0MsQ0FBL0MsQ0FESSxHQUVKLGNBQWMsQ0FBQyxXQUFmLENBQTJCLENBQTNCLENBRk47O0FBSUEsY0FBSSxHQUFHLEdBQUcsd0JBQVYsRUFBb0M7QUFDaEMsWUFBQSx3QkFBd0IsR0FBRyxHQUEzQjtBQUNBLFlBQUEsV0FBVyxHQUFHLENBQWQ7QUFDQSxZQUFBLG1CQUFtQixHQUFHLE1BQU0sQ0FBQyxtQkFBN0I7QUFDSDtBQUNKLFNBWEQ7O0FBYUEsWUFDSSx3QkFBd0IsSUFBSSxDQUE1QixLQUNDLHdCQUF3QixLQUFLLENBQTdCLElBQ0csQ0FBQyxtQkFESixJQUVHLFlBQVksSUFBWixDQUFpQixjQUFjLENBQUMsU0FBZixDQUF5Qix3QkFBd0IsR0FBRyxDQUFwRCxFQUF1RCx3QkFBdkQsQ0FBakIsQ0FISixDQURKLEVBS0U7QUFDRSxjQUFJLHFCQUFxQixHQUFHLGNBQWMsQ0FBQyxTQUFmLENBQ3hCLHdCQUF3QixHQUFHLENBREgsRUFFeEIsY0FBYyxDQUFDLE1BRlMsQ0FBNUI7QUFLQSxVQUFBLFdBQVcsR0FBRyxjQUFjLENBQUMsU0FBZixDQUF5Qix3QkFBekIsRUFBbUQsd0JBQXdCLEdBQUcsQ0FBOUUsQ0FBZDtBQUNBLGNBQUksZ0JBQWdCLEdBQUcscUJBQXFCLENBQUMsU0FBdEIsQ0FBZ0MsQ0FBaEMsRUFBbUMsQ0FBbkMsQ0FBdkI7QUFDQSxjQUFJLFlBQVksR0FDWixxQkFBcUIsQ0FBQyxNQUF0QixHQUErQixDQUEvQixLQUFxQyxnQkFBZ0IsS0FBSyxHQUFyQixJQUE0QixnQkFBZ0IsS0FBSyxNQUF0RixDQURKOztBQUVBLGNBQUksZ0JBQUosRUFBc0I7QUFDbEIsWUFBQSxxQkFBcUIsR0FBRyxxQkFBcUIsQ0FBQyxJQUF0QixFQUF4QjtBQUNIOztBQUVELGNBQUksS0FBSyxHQUFHLFdBQVcsR0FBRyxTQUFILEdBQWUsV0FBdEM7QUFFQSxlQUFLLE9BQUwsQ0FBYSxnQkFBYixHQUFnQyxLQUFLLENBQUMsSUFBTixDQUFXLHFCQUFYLENBQWhDOztBQUVBLGNBQUksQ0FBQyxZQUFELEtBQWtCLGlCQUFpQixJQUFJLENBQUMsS0FBSyxDQUFDLElBQU4sQ0FBVyxxQkFBWCxDQUF4QyxDQUFKLEVBQWdGO0FBQzVFLG1CQUFPO0FBQ0gsY0FBQSxlQUFlLEVBQUUsd0JBRGQ7QUFFSCxjQUFBLFdBQVcsRUFBRSxxQkFGVjtBQUdILGNBQUEsc0JBQXNCLEVBQUUsUUFIckI7QUFJSCxjQUFBLG1CQUFtQixFQUFFLElBSmxCO0FBS0gsY0FBQSxxQkFBcUIsRUFBRSxNQUxwQjtBQU1ILGNBQUEsa0JBQWtCLEVBQUU7QUFOakIsYUFBUDtBQVFIO0FBQ0o7QUFDSjtBQUNKOzs7OENBRXlCLEcsRUFBSyxLLEVBQU07QUFDakMsVUFBSSxXQUFXLEdBQUcsR0FBRyxDQUNoQixLQURhLENBQ1AsRUFETyxFQUViLE9BRmEsR0FHYixJQUhhLENBR1IsRUFIUSxDQUFsQjtBQUlBLFVBQUksS0FBSyxHQUFHLENBQUMsQ0FBYjs7QUFFQSxXQUFLLElBQUksSUFBSSxHQUFHLENBQVgsRUFBYyxHQUFHLEdBQUcsR0FBRyxDQUFDLE1BQTdCLEVBQXFDLElBQUksR0FBRyxHQUE1QyxFQUFpRCxJQUFJLEVBQXJELEVBQXlEO0FBQ3JELFlBQUksU0FBUyxHQUFHLElBQUksS0FBSyxHQUFHLENBQUMsTUFBSixHQUFhLENBQXRDO0FBQ0EsWUFBSSxZQUFZLEdBQUcsS0FBSyxJQUFMLENBQVUsV0FBVyxDQUFDLElBQUksR0FBRyxDQUFSLENBQXJCLENBQW5CO0FBQ0EsWUFBSSxLQUFLLEdBQUcsS0FBSSxLQUFLLFdBQVcsQ0FBQyxJQUFELENBQWhDOztBQUVBLFlBQUksS0FBSyxLQUFLLFNBQVMsSUFBSSxZQUFsQixDQUFULEVBQTBDO0FBQ3RDLFVBQUEsS0FBSyxHQUFHLEdBQUcsQ0FBQyxNQUFKLEdBQWEsQ0FBYixHQUFpQixJQUF6QjtBQUNBO0FBQ0g7QUFDSjs7QUFFRCxhQUFPLEtBQVA7QUFDSDs7O3NDQUVpQixPLEVBQVM7QUFDdkIsVUFBSSxDQUFDLE9BQUwsRUFBYztBQUNWLGVBQU8sS0FBUDtBQUNIOztBQUNELGFBQU8sT0FBTyxDQUFDLFFBQVIsS0FBcUIsT0FBckIsSUFBZ0MsT0FBTyxDQUFDLFFBQVIsS0FBcUIsVUFBNUQ7QUFDSDs7O29DQUVlLFcsRUFBYSxjLEVBQWdCO0FBQ3pDLFVBQUksV0FBVyxHQUFHLE1BQU0sQ0FBQyxVQUF6QjtBQUNBLFVBQUksWUFBWSxHQUFHLE1BQU0sQ0FBQyxXQUExQjtBQUNBLFVBQUksR0FBRyxHQUFHLFFBQVEsQ0FBQyxlQUFuQjtBQUNBLFVBQUksVUFBVSxHQUFHLENBQUMsTUFBTSxDQUFDLFdBQVAsSUFBc0IsR0FBRyxDQUFDLFVBQTNCLEtBQTBDLEdBQUcsQ0FBQyxVQUFKLElBQWtCLENBQTVELENBQWpCO0FBQ0EsVUFBSSxTQUFTLEdBQUcsQ0FBQyxNQUFNLENBQUMsV0FBUCxJQUFzQixHQUFHLENBQUMsU0FBM0IsS0FBeUMsR0FBRyxDQUFDLFNBQUosSUFBaUIsQ0FBMUQsQ0FBaEI7QUFFQSxVQUFJLE9BQU8sR0FDUCxPQUFPLFdBQVcsQ0FBQyxHQUFuQixLQUEyQixRQUEzQixHQUNNLFdBQVcsQ0FBQyxHQURsQixHQUVNLFNBQVMsR0FBRyxZQUFaLEdBQTJCLFdBQVcsQ0FBQyxNQUF2QyxHQUFnRCxjQUFjLENBQUMsTUFIekU7QUFJQSxVQUFJLFNBQVMsR0FDVCxPQUFPLFdBQVcsQ0FBQyxLQUFuQixLQUE2QixRQUE3QixHQUF3QyxXQUFXLENBQUMsS0FBcEQsR0FBNEQsV0FBVyxDQUFDLElBQVosR0FBbUIsY0FBYyxDQUFDLEtBRGxHO0FBRUEsVUFBSSxVQUFVLEdBQ1YsT0FBTyxXQUFXLENBQUMsTUFBbkIsS0FBOEIsUUFBOUIsR0FBeUMsV0FBVyxDQUFDLE1BQXJELEdBQThELFdBQVcsQ0FBQyxHQUFaLEdBQWtCLGNBQWMsQ0FBQyxNQURuRztBQUVBLFVBQUksUUFBUSxHQUNSLE9BQU8sV0FBVyxDQUFDLElBQW5CLEtBQTRCLFFBQTVCLEdBQ00sV0FBVyxDQUFDLElBRGxCLEdBRU0sVUFBVSxHQUFHLFdBQWIsR0FBMkIsV0FBVyxDQUFDLEtBQXZDLEdBQStDLGNBQWMsQ0FBQyxLQUh4RTtBQUtBLGFBQU87QUFDSCxRQUFBLEdBQUcsRUFBRSxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUwsQ0FBVyxTQUFYLENBRFo7QUFFSCxRQUFBLEtBQUssRUFBRSxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUwsQ0FBVSxVQUFVLEdBQUcsV0FBdkIsQ0FGaEI7QUFHSCxRQUFBLE1BQU0sRUFBRSxVQUFVLEdBQUcsSUFBSSxDQUFDLElBQUwsQ0FBVSxTQUFTLEdBQUcsWUFBdEIsQ0FIbEI7QUFJSCxRQUFBLElBQUksRUFBRSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUwsQ0FBVyxVQUFYO0FBSmQsT0FBUDtBQU1IOzs7d0NBRW1CO0FBQ2hCO0FBQ0E7QUFDQTtBQUNBLFVBQUksVUFBVSxHQUFHO0FBQ2IsUUFBQSxLQUFLLEVBQUUsSUFETTtBQUViLFFBQUEsTUFBTSxFQUFFO0FBRkssT0FBakI7QUFLQSxXQUFLLE9BQUwsQ0FBYSxJQUFiLENBQWtCLEtBQWxCLENBQXdCLE9BQXhCO0FBTUEsTUFBQSxVQUFVLENBQUMsS0FBWCxHQUFtQixLQUFLLE9BQUwsQ0FBYSxJQUFiLENBQWtCLFdBQXJDO0FBQ0EsTUFBQSxVQUFVLENBQUMsTUFBWCxHQUFvQixLQUFLLE9BQUwsQ0FBYSxJQUFiLENBQWtCLFlBQXRDO0FBRUEsV0FBSyxPQUFMLENBQWEsSUFBYixDQUFrQixLQUFsQixDQUF3QixPQUF4QjtBQUVBLGFBQU8sVUFBUDtBQUNILEssQ0FFRDs7Ozt3REFDb0MsTyxFQUFTLFEsRUFBVSxPLEVBQVM7QUFDNUQsVUFBSSxVQUFVLEdBQUcsQ0FDYixXQURhLEVBRWIsV0FGYSxFQUdiLE9BSGEsRUFJYixRQUphLEVBS2IsV0FMYSxFQU1iLFdBTmEsRUFPYixnQkFQYSxFQVFiLGtCQVJhLEVBU2IsbUJBVGEsRUFVYixpQkFWYSxFQVdiLFlBWGEsRUFZYixjQVphLEVBYWIsZUFiYSxFQWNiLGFBZGEsRUFlYixXQWZhLEVBZ0JiLGFBaEJhLEVBaUJiLFlBakJhLEVBa0JiLGFBbEJhLEVBbUJiLFVBbkJhLEVBb0JiLGdCQXBCYSxFQXFCYixZQXJCYSxFQXNCYixZQXRCYSxFQXVCYixXQXZCYSxFQXdCYixlQXhCYSxFQXlCYixZQXpCYSxFQTBCYixnQkExQmEsRUEyQmIsZUEzQmEsRUE0QmIsYUE1QmEsQ0FBakI7QUErQkEsVUFBSSxTQUFTLEdBQUcsTUFBTSxDQUFDLGVBQVAsS0FBMkIsSUFBM0M7QUFFQSxVQUFJLEdBQUcsR0FBRyxLQUFLLFdBQUwsR0FBbUIsYUFBbkIsQ0FBaUMsS0FBakMsQ0FBVjtBQUNBLE1BQUEsR0FBRyxDQUFDLEVBQUosR0FBUywwQ0FBVDtBQUNBLFdBQUssV0FBTCxHQUFtQixJQUFuQixDQUF3QixXQUF4QixDQUFvQyxHQUFwQztBQUVBLFVBQUksS0FBSyxHQUFHLEdBQUcsQ0FBQyxLQUFoQjtBQUNBLFVBQUksUUFBUSxHQUFHLE1BQU0sQ0FBQyxnQkFBUCxHQUEwQixnQkFBZ0IsQ0FBQyxPQUFELENBQTFDLEdBQXNELE9BQU8sQ0FBQyxZQUE3RTtBQUVBLE1BQUEsS0FBSyxDQUFDLFVBQU4sR0FBbUIsVUFBbkI7O0FBQ0EsVUFBSSxPQUFPLENBQUMsUUFBUixLQUFxQixPQUF6QixFQUFrQztBQUM5QixRQUFBLEtBQUssQ0FBQyxRQUFOLEdBQWlCLFlBQWpCO0FBQ0gsT0E1QzJELENBOEM1RDs7O0FBQ0EsTUFBQSxLQUFLLENBQUMsUUFBTixHQUFpQixVQUFqQjtBQUNBLE1BQUEsS0FBSyxDQUFDLFVBQU4sR0FBbUIsUUFBbkIsQ0FoRDRELENBa0Q1RDs7QUFDQSxNQUFBLFVBQVUsQ0FBQyxPQUFYLENBQW1CLFVBQUEsSUFBSSxFQUFJO0FBQ3ZCLFFBQUEsS0FBSyxDQUFDLElBQUQsQ0FBTCxHQUFjLFFBQVEsQ0FBQyxJQUFELENBQXRCO0FBQ0gsT0FGRDs7QUFJQSxVQUFJLFNBQUosRUFBZTtBQUNYLFFBQUEsS0FBSyxDQUFDLEtBQU4sYUFBaUIsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFWLENBQVIsR0FBMkIsQ0FBNUM7QUFDQSxZQUFJLE9BQU8sQ0FBQyxZQUFSLEdBQXVCLFFBQVEsQ0FBQyxRQUFRLENBQUMsTUFBVixDQUFuQyxFQUFzRCxLQUFLLENBQUMsU0FBTixHQUFrQixRQUFsQjtBQUN6RCxPQUhELE1BR087QUFDSCxRQUFBLEtBQUssQ0FBQyxRQUFOLEdBQWlCLFFBQWpCO0FBQ0g7O0FBRUQsTUFBQSxHQUFHLENBQUMsV0FBSixHQUFrQixPQUFPLENBQUMsS0FBUixDQUFjLFNBQWQsQ0FBd0IsQ0FBeEIsRUFBMkIsUUFBM0IsQ0FBbEI7O0FBRUEsVUFBSSxPQUFPLENBQUMsUUFBUixLQUFxQixPQUF6QixFQUFrQztBQUM5QixRQUFBLEdBQUcsQ0FBQyxXQUFKLEdBQWtCLEdBQUcsQ0FBQyxXQUFKLENBQWdCLE9BQWhCLENBQXdCLEtBQXhCLEVBQStCLEdBQS9CLENBQWxCO0FBQ0g7O0FBRUQsVUFBSSxJQUFJLEdBQUcsS0FBSyxXQUFMLEdBQW1CLGFBQW5CLENBQWlDLE1BQWpDLENBQVg7QUFDQSxNQUFBLElBQUksQ0FBQyxXQUFMLEdBQW1CLE9BQU8sQ0FBQyxLQUFSLENBQWMsU0FBZCxDQUF3QixRQUF4QixLQUFxQyxHQUF4RDtBQUNBLE1BQUEsR0FBRyxDQUFDLFdBQUosQ0FBZ0IsSUFBaEI7QUFFQSxVQUFJLElBQUksR0FBRyxPQUFPLENBQUMscUJBQVIsRUFBWDtBQUNBLFVBQUksR0FBRyxHQUFHLFFBQVEsQ0FBQyxlQUFuQjtBQUNBLFVBQUksVUFBVSxHQUFHLENBQUMsTUFBTSxDQUFDLFdBQVAsSUFBc0IsR0FBRyxDQUFDLFVBQTNCLEtBQTBDLEdBQUcsQ0FBQyxVQUFKLElBQWtCLENBQTVELENBQWpCO0FBQ0EsVUFBSSxTQUFTLEdBQUcsQ0FBQyxNQUFNLENBQUMsV0FBUCxJQUFzQixHQUFHLENBQUMsU0FBM0IsS0FBeUMsR0FBRyxDQUFDLFNBQUosSUFBaUIsQ0FBMUQsQ0FBaEI7QUFFQSxVQUFJLFdBQVcsR0FBRztBQUNkLFFBQUEsR0FBRyxFQUNDLElBQUksQ0FBQyxHQUFMLEdBQ0EsU0FEQSxHQUVBLElBQUksQ0FBQyxTQUZMLEdBR0EsUUFBUSxDQUFDLFFBQVEsQ0FBQyxjQUFWLENBSFIsR0FJQSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVYsQ0FKUixHQUtBLE9BQU8sQ0FBQyxTQVBFO0FBUWQsUUFBQSxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUwsR0FBWSxVQUFaLEdBQXlCLElBQUksQ0FBQyxVQUE5QixHQUEyQyxRQUFRLENBQUMsUUFBUSxDQUFDLGVBQVY7QUFSM0MsT0FBbEI7QUFXQSxVQUFJLFdBQVcsR0FBRyxNQUFNLENBQUMsVUFBekI7QUFDQSxVQUFJLFlBQVksR0FBRyxNQUFNLENBQUMsV0FBMUI7QUFFQSxVQUFJLGNBQWMsR0FBRyxLQUFLLGlCQUFMLEVBQXJCO0FBQ0EsVUFBSSxlQUFlLEdBQUcsS0FBSyxlQUFMLENBQXFCLFdBQXJCLEVBQWtDLGNBQWxDLENBQXRCOztBQUVBLFVBQUksZUFBZSxDQUFDLEtBQXBCLEVBQTJCO0FBQ3ZCLFFBQUEsV0FBVyxDQUFDLEtBQVosR0FBb0IsV0FBVyxHQUFHLFdBQVcsQ0FBQyxJQUE5QztBQUNBLFFBQUEsV0FBVyxDQUFDLElBQVosR0FBbUIsTUFBbkI7QUFDSDs7QUFFRCxVQUFJLFlBQVksR0FBRyxLQUFLLE9BQUwsQ0FBYSxhQUFiLEdBQ2IsS0FBSyxPQUFMLENBQWEsYUFBYixDQUEyQixZQURkLEdBRWIsS0FBSyxXQUFMLEdBQW1CLElBQW5CLENBQXdCLFlBRjlCOztBQUlBLFVBQUksZUFBZSxDQUFDLE1BQXBCLEVBQTRCO0FBQ3hCLFlBQUksVUFBVSxHQUFHLEtBQUssT0FBTCxDQUFhLGFBQWIsR0FDWCxLQUFLLE9BQUwsQ0FBYSxhQUFiLENBQTJCLHFCQUEzQixFQURXLEdBRVgsS0FBSyxXQUFMLEdBQW1CLElBQW5CLENBQXdCLHFCQUF4QixFQUZOO0FBR0EsWUFBSSxvQkFBb0IsR0FBRyxZQUFZLElBQUksWUFBWSxHQUFHLFVBQVUsQ0FBQyxHQUE5QixDQUF2QztBQUVBLFFBQUEsV0FBVyxDQUFDLE1BQVosR0FBcUIsb0JBQW9CLElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxHQUFwQixHQUEwQixJQUFJLENBQUMsU0FBbkMsQ0FBekM7QUFDQSxRQUFBLFdBQVcsQ0FBQyxHQUFaLEdBQWtCLE1BQWxCO0FBQ0g7O0FBRUQsTUFBQSxlQUFlLEdBQUcsS0FBSyxlQUFMLENBQXFCLFdBQXJCLEVBQWtDLGNBQWxDLENBQWxCOztBQUNBLFVBQUksZUFBZSxDQUFDLElBQXBCLEVBQTBCO0FBQ3RCLFFBQUEsV0FBVyxDQUFDLElBQVosR0FDSSxXQUFXLEdBQUcsY0FBYyxDQUFDLEtBQTdCLEdBQXFDLFVBQVUsR0FBRyxXQUFiLEdBQTJCLGNBQWMsQ0FBQyxLQUEvRSxHQUF1RixVQUQzRjtBQUVBLGVBQU8sV0FBVyxDQUFDLEtBQW5CO0FBQ0g7O0FBQ0QsVUFBSSxlQUFlLENBQUMsR0FBcEIsRUFBeUI7QUFDckIsUUFBQSxXQUFXLENBQUMsR0FBWixHQUNJLFlBQVksR0FBRyxjQUFjLENBQUMsTUFBOUIsR0FBdUMsU0FBUyxHQUFHLFlBQVosR0FBMkIsY0FBYyxDQUFDLE1BQWpGLEdBQTBGLFNBRDlGO0FBRUEsZUFBTyxXQUFXLENBQUMsTUFBbkI7QUFDSDs7QUFFRCxXQUFLLFdBQUwsR0FBbUIsSUFBbkIsQ0FBd0IsV0FBeEIsQ0FBb0MsR0FBcEM7QUFDQSxhQUFPLFdBQVA7QUFDSDs7O29EQUUrQixvQixFQUFzQjtBQUNsRCxVQUFJLGNBQWMsR0FBRyxHQUFyQjtBQUNBLFVBQUksUUFBSjtBQUFBLFVBQ0ksUUFBUSxpQkFBVSxJQUFJLElBQUosR0FBVyxPQUFYLEVBQVYsY0FBa0MsSUFBSSxDQUFDLE1BQUwsR0FDckMsUUFEcUMsR0FFckMsTUFGcUMsQ0FFOUIsQ0FGOEIsQ0FBbEMsQ0FEWjtBQUlBLFVBQUksS0FBSjtBQUNBLFVBQUksR0FBRyxHQUFHLEtBQUssa0JBQUwsRUFBVjtBQUNBLFVBQUksU0FBUyxHQUFHLEdBQUcsQ0FBQyxVQUFKLENBQWUsQ0FBZixDQUFoQjtBQUVBLE1BQUEsS0FBSyxHQUFHLEtBQUssV0FBTCxHQUFtQixXQUFuQixFQUFSO0FBQ0EsTUFBQSxLQUFLLENBQUMsUUFBTixDQUFlLEdBQUcsQ0FBQyxVQUFuQixFQUErQixvQkFBL0I7QUFDQSxNQUFBLEtBQUssQ0FBQyxNQUFOLENBQWEsR0FBRyxDQUFDLFVBQWpCLEVBQTZCLG9CQUE3QjtBQUVBLE1BQUEsS0FBSyxDQUFDLFFBQU4sQ0FBZSxLQUFmLEVBZGtELENBZ0JsRDs7QUFDQSxNQUFBLFFBQVEsR0FBRyxLQUFLLFdBQUwsR0FBbUIsYUFBbkIsQ0FBaUMsTUFBakMsQ0FBWDtBQUNBLE1BQUEsUUFBUSxDQUFDLEVBQVQsR0FBYyxRQUFkO0FBRUEsTUFBQSxRQUFRLENBQUMsV0FBVCxDQUFxQixLQUFLLFdBQUwsR0FBbUIsY0FBbkIsQ0FBa0MsY0FBbEMsQ0FBckI7QUFDQSxNQUFBLEtBQUssQ0FBQyxVQUFOLENBQWlCLFFBQWpCO0FBQ0EsTUFBQSxHQUFHLENBQUMsZUFBSjtBQUNBLE1BQUEsR0FBRyxDQUFDLFFBQUosQ0FBYSxTQUFiO0FBRUEsVUFBSSxJQUFJLEdBQUcsUUFBUSxDQUFDLHFCQUFULEVBQVg7QUFDQSxVQUFJLEdBQUcsR0FBRyxRQUFRLENBQUMsZUFBbkI7QUFDQSxVQUFJLFVBQVUsR0FBRyxDQUFDLE1BQU0sQ0FBQyxXQUFQLElBQXNCLEdBQUcsQ0FBQyxVQUEzQixLQUEwQyxHQUFHLENBQUMsVUFBSixJQUFrQixDQUE1RCxDQUFqQjtBQUNBLFVBQUksU0FBUyxHQUFHLENBQUMsTUFBTSxDQUFDLFdBQVAsSUFBc0IsR0FBRyxDQUFDLFNBQTNCLEtBQXlDLEdBQUcsQ0FBQyxTQUFKLElBQWlCLENBQTFELENBQWhCO0FBQ0EsVUFBSSxXQUFXLEdBQUc7QUFDZCxRQUFBLElBQUksRUFBRSxJQUFJLENBQUMsSUFBTCxHQUFZLFVBREo7QUFFZCxRQUFBLEdBQUcsRUFBRSxJQUFJLENBQUMsR0FBTCxHQUFXLFFBQVEsQ0FBQyxZQUFwQixHQUFtQztBQUYxQixPQUFsQjtBQUlBLFVBQUksV0FBVyxHQUFHLE1BQU0sQ0FBQyxVQUF6QjtBQUNBLFVBQUksWUFBWSxHQUFHLE1BQU0sQ0FBQyxXQUExQjtBQUVBLFVBQUksY0FBYyxHQUFHLEtBQUssaUJBQUwsRUFBckI7QUFDQSxVQUFJLGVBQWUsR0FBRyxLQUFLLGVBQUwsQ0FBcUIsV0FBckIsRUFBa0MsY0FBbEMsQ0FBdEI7O0FBRUEsVUFBSSxlQUFlLENBQUMsS0FBcEIsRUFBMkI7QUFDdkIsUUFBQSxXQUFXLENBQUMsSUFBWixHQUFtQixNQUFuQjtBQUNBLFFBQUEsV0FBVyxDQUFDLEtBQVosR0FBb0IsV0FBVyxHQUFHLElBQUksQ0FBQyxJQUFuQixHQUEwQixVQUE5QztBQUNIOztBQUVELFVBQUksWUFBWSxHQUFHLEtBQUssT0FBTCxDQUFhLGFBQWIsR0FDYixLQUFLLE9BQUwsQ0FBYSxhQUFiLENBQTJCLFlBRGQsR0FFYixLQUFLLFdBQUwsR0FBbUIsSUFBbkIsQ0FBd0IsWUFGOUI7O0FBSUEsVUFBSSxlQUFlLENBQUMsTUFBcEIsRUFBNEI7QUFDeEIsWUFBSSxVQUFVLEdBQUcsS0FBSyxPQUFMLENBQWEsYUFBYixHQUNYLEtBQUssT0FBTCxDQUFhLGFBQWIsQ0FBMkIscUJBQTNCLEVBRFcsR0FFWCxLQUFLLFdBQUwsR0FBbUIsSUFBbkIsQ0FBd0IscUJBQXhCLEVBRk47QUFHQSxZQUFJLG9CQUFvQixHQUFHLFlBQVksSUFBSSxZQUFZLEdBQUcsVUFBVSxDQUFDLEdBQTlCLENBQXZDO0FBRUEsUUFBQSxXQUFXLENBQUMsR0FBWixHQUFrQixNQUFsQjtBQUNBLFFBQUEsV0FBVyxDQUFDLE1BQVosR0FBcUIsb0JBQW9CLElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxHQUF4QixDQUF6QztBQUNIOztBQUVELE1BQUEsZUFBZSxHQUFHLEtBQUssZUFBTCxDQUFxQixXQUFyQixFQUFrQyxjQUFsQyxDQUFsQjs7QUFDQSxVQUFJLGVBQWUsQ0FBQyxJQUFwQixFQUEwQjtBQUN0QixRQUFBLFdBQVcsQ0FBQyxJQUFaLEdBQ0ksV0FBVyxHQUFHLGNBQWMsQ0FBQyxLQUE3QixHQUFxQyxVQUFVLEdBQUcsV0FBYixHQUEyQixjQUFjLENBQUMsS0FBL0UsR0FBdUYsVUFEM0Y7QUFFQSxlQUFPLFdBQVcsQ0FBQyxLQUFuQjtBQUNIOztBQUNELFVBQUksZUFBZSxDQUFDLEdBQXBCLEVBQXlCO0FBQ3JCLFFBQUEsV0FBVyxDQUFDLEdBQVosR0FDSSxZQUFZLEdBQUcsY0FBYyxDQUFDLE1BQTlCLEdBQXVDLFNBQVMsR0FBRyxZQUFaLEdBQTJCLGNBQWMsQ0FBQyxNQUFqRixHQUEwRixTQUQ5RjtBQUVBLGVBQU8sV0FBVyxDQUFDLE1BQW5CO0FBQ0g7O0FBRUQsTUFBQSxRQUFRLENBQUMsVUFBVCxDQUFvQixXQUFwQixDQUFnQyxRQUFoQztBQUNBLGFBQU8sV0FBUDtBQUNILEssQ0FFRDs7OzttQ0FDZSxJLEVBQU07QUFDakIsVUFBSSxnQkFBZ0IsR0FBRyxFQUF2QjtBQUFBLFVBQ0ksVUFESjtBQUVBLFVBQUkscUJBQXFCLEdBQUcsR0FBNUI7QUFDQSxVQUFJLENBQUMsR0FBRyxLQUFLLElBQWI7QUFFQSxVQUFJLE9BQU8sQ0FBUCxLQUFhLFdBQWpCLEVBQThCOztBQUU5QixhQUFPLFVBQVUsS0FBSyxTQUFmLElBQTRCLFVBQVUsQ0FBQyxNQUFYLEtBQXNCLENBQXpELEVBQTREO0FBQ3hELFFBQUEsVUFBVSxHQUFHLENBQUMsQ0FBQyxxQkFBRixFQUFiOztBQUVBLFlBQUksVUFBVSxDQUFDLE1BQVgsS0FBc0IsQ0FBMUIsRUFBNkI7QUFDekIsVUFBQSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFVBQUYsQ0FBYSxDQUFiLENBQUo7O0FBQ0EsY0FBSSxDQUFDLEtBQUssU0FBTixJQUFtQixDQUFDLENBQUMsQ0FBQyxxQkFBMUIsRUFBaUQ7QUFDN0M7QUFDSDtBQUNKO0FBQ0o7O0FBRUQsVUFBSSxPQUFPLEdBQUcsVUFBVSxDQUFDLEdBQXpCO0FBQ0EsVUFBSSxVQUFVLEdBQUcsT0FBTyxHQUFHLFVBQVUsQ0FBQyxNQUF0Qzs7QUFFQSxVQUFJLE9BQU8sR0FBRyxDQUFkLEVBQWlCO0FBQ2IsUUFBQSxNQUFNLENBQUMsUUFBUCxDQUFnQixDQUFoQixFQUFtQixNQUFNLENBQUMsV0FBUCxHQUFxQixVQUFVLENBQUMsR0FBaEMsR0FBc0MsZ0JBQXpEO0FBQ0gsT0FGRCxNQUVPLElBQUksVUFBVSxHQUFHLE1BQU0sQ0FBQyxXQUF4QixFQUFxQztBQUN4QyxZQUFJLElBQUksR0FBRyxNQUFNLENBQUMsV0FBUCxHQUFxQixVQUFVLENBQUMsR0FBaEMsR0FBc0MsZ0JBQWpEOztBQUVBLFlBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQyxXQUFkLEdBQTRCLHFCQUFoQyxFQUF1RDtBQUNuRCxVQUFBLElBQUksR0FBRyxNQUFNLENBQUMsV0FBUCxHQUFxQixxQkFBNUI7QUFDSDs7QUFFRCxZQUFJLE9BQU8sR0FBRyxNQUFNLENBQUMsV0FBUCxJQUFzQixNQUFNLENBQUMsV0FBUCxHQUFxQixVQUEzQyxDQUFkOztBQUVBLFlBQUksT0FBTyxHQUFHLElBQWQsRUFBb0I7QUFDaEIsVUFBQSxPQUFPLEdBQUcsSUFBVjtBQUNIOztBQUVELFFBQUEsTUFBTSxDQUFDLFFBQVAsQ0FBZ0IsQ0FBaEIsRUFBbUIsT0FBbkI7QUFDSDtBQUNKOzs7Ozs7ZUFHVSxZOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3c0JmO0lBQ00sYTtBQUNGLHlCQUFZLE9BQVosRUFBcUI7QUFBQTs7QUFDakIsU0FBSyxPQUFMLEdBQWUsT0FBZjtBQUNBLFNBQUssT0FBTCxDQUFhLE1BQWIsR0FBc0IsSUFBdEI7QUFDSDs7OztpQ0FFWSxPLEVBQVMsSyxFQUFPO0FBQUE7O0FBQ3pCLGFBQU8sS0FBSyxDQUFDLE1BQU4sQ0FBYSxVQUFBLE1BQU0sRUFBSTtBQUMxQixlQUFPLEtBQUksQ0FBQyxJQUFMLENBQVUsT0FBVixFQUFtQixNQUFuQixDQUFQO0FBQ0gsT0FGTSxDQUFQO0FBR0g7Ozt5QkFFSSxPLEVBQVMsTSxFQUFRO0FBQ2xCLGFBQU8sS0FBSyxLQUFMLENBQVcsT0FBWCxFQUFvQixNQUFwQixNQUFnQyxJQUF2QztBQUNIOzs7MEJBRUssTyxFQUFTLE0sRUFBUSxJLEVBQU07QUFDekIsTUFBQSxJQUFJLEdBQUcsSUFBSSxJQUFJLEVBQWYsQ0FEeUIsQ0FFekI7O0FBQ0EsVUFBSSxVQUFVLEdBQUcsQ0FBakI7QUFBQSxVQUNJO0FBQ0EsTUFBQSxNQUFNLEdBQUcsRUFGYjtBQUFBLFVBR0k7QUFDQSxNQUFBLEdBQUcsR0FBRyxNQUFNLENBQUMsTUFKakI7QUFBQSxVQUtJO0FBQ0EsTUFBQSxVQUFVLEdBQUcsQ0FOakI7QUFBQSxVQU9JO0FBQ0EsTUFBQSxTQUFTLEdBQUcsQ0FSaEI7QUFBQSxVQVNJLEdBQUcsR0FBRyxJQUFJLENBQUMsR0FBTCxJQUFZLEVBVHRCO0FBQUEsVUFVSSxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUwsSUFBYSxFQVZ4QjtBQUFBLFVBV0ksYUFBYSxHQUFJLElBQUksQ0FBQyxhQUFMLElBQXNCLE1BQXZCLElBQWtDLE1BQU0sQ0FBQyxXQUFQLEVBWHREO0FBQUEsVUFZSTtBQUNBLE1BQUEsRUFiSjtBQUFBLFVBY0k7QUFDQSxNQUFBLFdBZko7QUFpQkEsTUFBQSxPQUFPLEdBQUksSUFBSSxDQUFDLGFBQUwsSUFBc0IsT0FBdkIsSUFBbUMsT0FBTyxDQUFDLFdBQVIsRUFBN0M7QUFFQSxVQUFJLFlBQVksR0FBRyxLQUFLLFFBQUwsQ0FBYyxhQUFkLEVBQTZCLE9BQTdCLEVBQXNDLENBQXRDLEVBQXlDLENBQXpDLEVBQTRDLEVBQTVDLENBQW5COztBQUNBLFVBQUksQ0FBQyxZQUFMLEVBQW1CO0FBQ2YsZUFBTyxJQUFQO0FBQ0g7O0FBRUQsYUFBTztBQUNILFFBQUEsUUFBUSxFQUFFLEtBQUssTUFBTCxDQUFZLE1BQVosRUFBb0IsWUFBWSxDQUFDLEtBQWpDLEVBQXdDLEdBQXhDLEVBQTZDLElBQTdDLENBRFA7QUFFSCxRQUFBLEtBQUssRUFBRSxZQUFZLENBQUM7QUFGakIsT0FBUDtBQUlIOzs7NkJBRVEsTSxFQUFRLE8sRUFBUyxXLEVBQWEsWSxFQUFjLFksRUFBYztBQUMvRDtBQUNBLFVBQUksT0FBTyxDQUFDLE1BQVIsS0FBbUIsWUFBdkIsRUFBcUM7QUFDakM7QUFDQSxlQUFPO0FBQ0gsVUFBQSxLQUFLLEVBQUUsS0FBSyxjQUFMLENBQW9CLFlBQXBCLENBREo7QUFFSCxVQUFBLEtBQUssRUFBRSxZQUFZLENBQUMsS0FBYjtBQUZKLFNBQVA7QUFJSCxPQVI4RCxDQVUvRDs7O0FBQ0EsVUFBSSxNQUFNLENBQUMsTUFBUCxLQUFrQixXQUFsQixJQUFpQyxPQUFPLENBQUMsTUFBUixHQUFpQixZQUFqQixHQUFnQyxNQUFNLENBQUMsTUFBUCxHQUFnQixXQUFyRixFQUFrRztBQUM5RixlQUFPLFNBQVA7QUFDSDs7QUFFRCxVQUFJLENBQUMsR0FBRyxPQUFPLENBQUMsWUFBRCxDQUFmO0FBQ0EsVUFBSSxLQUFLLEdBQUcsTUFBTSxDQUFDLE9BQVAsQ0FBZSxDQUFmLEVBQWtCLFdBQWxCLENBQVo7QUFDQSxVQUFJLElBQUosRUFBVSxJQUFWOztBQUVBLGFBQU8sS0FBSyxHQUFHLENBQUMsQ0FBaEIsRUFBbUI7QUFDZixRQUFBLFlBQVksQ0FBQyxJQUFiLENBQWtCLEtBQWxCO0FBQ0EsUUFBQSxJQUFJLEdBQUcsS0FBSyxRQUFMLENBQWMsTUFBZCxFQUFzQixPQUF0QixFQUErQixLQUFLLEdBQUcsQ0FBdkMsRUFBMEMsWUFBWSxHQUFHLENBQXpELEVBQTRELFlBQTVELENBQVA7QUFDQSxRQUFBLFlBQVksQ0FBQyxHQUFiLEdBSGUsQ0FLZjs7QUFDQSxZQUFJLENBQUMsSUFBTCxFQUFXO0FBQ1AsaUJBQU8sSUFBUDtBQUNIOztBQUVELFlBQUksQ0FBQyxJQUFELElBQVMsSUFBSSxDQUFDLEtBQUwsR0FBYSxJQUFJLENBQUMsS0FBL0IsRUFBc0M7QUFDbEMsVUFBQSxJQUFJLEdBQUcsSUFBUDtBQUNIOztBQUVELFFBQUEsS0FBSyxHQUFHLE1BQU0sQ0FBQyxPQUFQLENBQWUsQ0FBZixFQUFrQixLQUFLLEdBQUcsQ0FBMUIsQ0FBUjtBQUNIOztBQUVELGFBQU8sSUFBUDtBQUNIOzs7bUNBRWMsWSxFQUFjO0FBQ3pCLFVBQUksS0FBSyxHQUFHLENBQVo7QUFDQSxVQUFJLElBQUksR0FBRyxDQUFYO0FBRUEsTUFBQSxZQUFZLENBQUMsT0FBYixDQUFxQixVQUFDLEtBQUQsRUFBUSxDQUFSLEVBQWM7QUFDL0IsWUFBSSxDQUFDLEdBQUcsQ0FBUixFQUFXO0FBQ1AsY0FBSSxZQUFZLENBQUMsQ0FBQyxHQUFHLENBQUwsQ0FBWixHQUFzQixDQUF0QixLQUE0QixLQUFoQyxFQUF1QztBQUNuQyxZQUFBLElBQUksSUFBSSxJQUFJLEdBQUcsQ0FBZjtBQUNILFdBRkQsTUFFTztBQUNILFlBQUEsSUFBSSxHQUFHLENBQVA7QUFDSDtBQUNKOztBQUVELFFBQUEsS0FBSyxJQUFJLElBQVQ7QUFDSCxPQVZEO0FBWUEsYUFBTyxLQUFQO0FBQ0g7OzsyQkFFTSxNLEVBQVEsTyxFQUFTLEcsRUFBSyxJLEVBQU07QUFDL0IsVUFBSSxRQUFRLEdBQUcsTUFBTSxDQUFDLFNBQVAsQ0FBaUIsQ0FBakIsRUFBb0IsT0FBTyxDQUFDLENBQUQsQ0FBM0IsQ0FBZjtBQUVBLE1BQUEsT0FBTyxDQUFDLE9BQVIsQ0FBZ0IsVUFBQyxLQUFELEVBQVEsQ0FBUixFQUFjO0FBQzFCLFFBQUEsUUFBUSxJQUNKLEdBQUcsR0FDSCxNQUFNLENBQUMsS0FBRCxDQUROLEdBRUEsSUFGQSxHQUdBLE1BQU0sQ0FBQyxTQUFQLENBQWlCLEtBQUssR0FBRyxDQUF6QixFQUE0QixPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUwsQ0FBUCxHQUFpQixPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUwsQ0FBeEIsR0FBa0MsTUFBTSxDQUFDLE1BQXJFLENBSko7QUFLSCxPQU5EO0FBUUEsYUFBTyxRQUFQO0FBQ0g7OzsyQkFFTSxPLEVBQVMsRyxFQUFLLEksRUFBTTtBQUFBOztBQUN2QixNQUFBLElBQUksR0FBRyxJQUFJLElBQUksRUFBZjtBQUNBLGFBQ0ksR0FBRyxDQUNDO0FBREQsT0FFRSxNQUZMLENBRVksVUFBQyxJQUFELEVBQU8sT0FBUCxFQUFnQixHQUFoQixFQUFxQixHQUFyQixFQUE2QjtBQUNqQyxZQUFJLEdBQUcsR0FBRyxPQUFWOztBQUVBLFlBQUksSUFBSSxDQUFDLE9BQVQsRUFBa0I7QUFDZCxVQUFBLEdBQUcsR0FBRyxJQUFJLENBQUMsT0FBTCxDQUFhLE9BQWIsQ0FBTjs7QUFFQSxjQUFJLENBQUMsR0FBTCxFQUFVO0FBQ047QUFDQSxZQUFBLEdBQUcsR0FBRyxFQUFOO0FBQ0g7QUFDSjs7QUFFRCxZQUFJLFFBQVEsR0FBRyxNQUFJLENBQUMsS0FBTCxDQUFXLE9BQVgsRUFBb0IsR0FBcEIsRUFBeUIsSUFBekIsQ0FBZjs7QUFFQSxZQUFJLFFBQVEsSUFBSSxJQUFoQixFQUFzQjtBQUNsQixVQUFBLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTixDQUFKLEdBQW9CO0FBQ2hCLFlBQUEsTUFBTSxFQUFFLFFBQVEsQ0FBQyxRQUREO0FBRWhCLFlBQUEsS0FBSyxFQUFFLFFBQVEsQ0FBQyxLQUZBO0FBR2hCLFlBQUEsS0FBSyxFQUFFLEdBSFM7QUFJaEIsWUFBQSxRQUFRLEVBQUU7QUFKTSxXQUFwQjtBQU1IOztBQUVELGVBQU8sSUFBUDtBQUNILE9BMUJMLEVBMEJPLEVBMUJQLEVBNEJLLElBNUJMLENBNEJVLFVBQUMsQ0FBRCxFQUFJLENBQUosRUFBVTtBQUNaLFlBQUksT0FBTyxHQUFHLENBQUMsQ0FBQyxLQUFGLEdBQVUsQ0FBQyxDQUFDLEtBQTFCO0FBQ0EsWUFBSSxPQUFKLEVBQWEsT0FBTyxPQUFQO0FBQ2IsZUFBTyxDQUFDLENBQUMsS0FBRixHQUFVLENBQUMsQ0FBQyxLQUFuQjtBQUNILE9BaENMLENBREo7QUFtQ0g7Ozs7OztlQUdVLGE7Ozs7Ozs7Ozs7OztBQzdKZjs7OztBQUxBOzs7O2VBT2UsbUI7Ozs7Ozs7QUNQZixJQUFJLENBQUMsS0FBSyxDQUFDLFNBQU4sQ0FBZ0IsSUFBckIsRUFBMkI7QUFDdkIsRUFBQSxLQUFLLENBQUMsU0FBTixDQUFnQixJQUFoQixHQUF1QixVQUFTLFNBQVQsRUFBb0I7QUFDdkMsUUFBSSxTQUFTLElBQWIsRUFBbUI7QUFDZixZQUFNLElBQUksU0FBSixDQUFjLGtEQUFkLENBQU47QUFDSDs7QUFDRCxRQUFJLE9BQU8sU0FBUCxLQUFxQixVQUF6QixFQUFxQztBQUNqQyxZQUFNLElBQUksU0FBSixDQUFjLDhCQUFkLENBQU47QUFDSDs7QUFDRCxRQUFJLElBQUksR0FBRyxNQUFNLENBQUMsSUFBRCxDQUFqQjtBQUNBLFFBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFMLEtBQWdCLENBQTdCO0FBQ0EsUUFBSSxPQUFPLEdBQUcsU0FBUyxDQUFDLENBQUQsQ0FBdkI7QUFDQSxRQUFJLEtBQUo7O0FBRUEsU0FBSyxJQUFJLENBQUMsR0FBRyxDQUFiLEVBQWdCLENBQUMsR0FBRyxNQUFwQixFQUE0QixDQUFDLEVBQTdCLEVBQWlDO0FBQzdCLE1BQUEsS0FBSyxHQUFHLElBQUksQ0FBQyxDQUFELENBQVo7O0FBQ0EsVUFBSSxTQUFTLENBQUMsSUFBVixDQUFlLE9BQWYsRUFBd0IsS0FBeEIsRUFBK0IsQ0FBL0IsRUFBa0MsSUFBbEMsQ0FBSixFQUE2QztBQUN6QyxlQUFPLEtBQVA7QUFDSDtBQUNKOztBQUNELFdBQU8sU0FBUDtBQUNILEdBbkJEO0FBb0JIOztBQUVELElBQUksTUFBTSxJQUFJLE9BQU8sTUFBTSxDQUFDLFdBQWQsS0FBOEIsVUFBNUMsRUFBd0Q7QUFDcEQ7QUFEb0QsTUFFM0MsV0FGMkMsR0FFcEQsU0FBUyxXQUFULENBQXFCLEtBQXJCLEVBQTRCLE1BQTVCLEVBQW9DO0FBQ2hDLElBQUEsTUFBTSxHQUFHLE1BQU0sSUFBSTtBQUNmLE1BQUEsT0FBTyxFQUFFLEtBRE07QUFFZixNQUFBLFVBQVUsRUFBRSxLQUZHO0FBR2YsTUFBQSxNQUFNLEVBQUU7QUFITyxLQUFuQjtBQUtBLFFBQUksR0FBRyxHQUFHLFFBQVEsQ0FBQyxXQUFULENBQXFCLGFBQXJCLENBQVY7QUFDQSxJQUFBLEdBQUcsQ0FBQyxlQUFKLENBQW9CLEtBQXBCLEVBQTJCLE1BQU0sQ0FBQyxPQUFsQyxFQUEyQyxNQUFNLENBQUMsVUFBbEQsRUFBOEQsTUFBTSxDQUFDLE1BQXJFO0FBQ0EsV0FBTyxHQUFQO0FBQ0gsR0FYbUQ7O0FBYXBELE1BQUksT0FBTyxNQUFNLENBQUMsS0FBZCxLQUF3QixXQUE1QixFQUF5QztBQUNyQyxJQUFBLFdBQVcsQ0FBQyxTQUFaLEdBQXdCLE1BQU0sQ0FBQyxLQUFQLENBQWEsU0FBckM7QUFDSDs7QUFFRCxFQUFBLE1BQU0sQ0FBQyxXQUFQLEdBQXFCLFdBQXJCO0FBQ0giLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigpe2Z1bmN0aW9uIHIoZSxuLHQpe2Z1bmN0aW9uIG8oaSxmKXtpZighbltpXSl7aWYoIWVbaV0pe3ZhciBjPVwiZnVuY3Rpb25cIj09dHlwZW9mIHJlcXVpcmUmJnJlcXVpcmU7aWYoIWYmJmMpcmV0dXJuIGMoaSwhMCk7aWYodSlyZXR1cm4gdShpLCEwKTt2YXIgYT1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK2krXCInXCIpO3Rocm93IGEuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixhfXZhciBwPW5baV09e2V4cG9ydHM6e319O2VbaV1bMF0uY2FsbChwLmV4cG9ydHMsZnVuY3Rpb24ocil7dmFyIG49ZVtpXVsxXVtyXTtyZXR1cm4gbyhufHxyKX0scCxwLmV4cG9ydHMscixlLG4sdCl9cmV0dXJuIG5baV0uZXhwb3J0c31mb3IodmFyIHU9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZSxpPTA7aTx0Lmxlbmd0aDtpKyspbyh0W2ldKTtyZXR1cm4gb31yZXR1cm4gcn0pKCkiLCIvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmVcbmltcG9ydCBUcmlidXRlVXRpbHMgZnJvbSBcIi4vdXRpbHNcIjtcbmltcG9ydCBUcmlidXRlRXZlbnRzIGZyb20gXCIuL1RyaWJ1dGVFdmVudHNcIjtcbmltcG9ydCBUcmlidXRlTWVudUV2ZW50cyBmcm9tIFwiLi9UcmlidXRlTWVudUV2ZW50c1wiO1xuaW1wb3J0IFRyaWJ1dGVSYW5nZSBmcm9tIFwiLi9UcmlidXRlUmFuZ2VcIjtcbmltcG9ydCBUcmlidXRlU2VhcmNoIGZyb20gXCIuL1RyaWJ1dGVTZWFyY2hcIjtcblxuY2xhc3MgVHJpYnV0ZSB7XG4gICAgY29uc3RydWN0b3Ioe1xuICAgICAgICB2YWx1ZXMgPSBudWxsLFxuICAgICAgICBpZnJhbWUgPSBudWxsLFxuICAgICAgICBzZWxlY3RDbGFzcyA9IFwiaGlnaGxpZ2h0XCIsXG4gICAgICAgIHRyaWdnZXIgPSBcIkBcIixcbiAgICAgICAgYXV0b2NvbXBsZXRlTW9kZSA9IGZhbHNlLFxuICAgICAgICBzZWxlY3RUZW1wbGF0ZSA9IG51bGwsXG4gICAgICAgIG1lbnVJdGVtVGVtcGxhdGUgPSBudWxsLFxuICAgICAgICBsb29rdXAgPSBcImtleVwiLFxuICAgICAgICBmaWxsQXR0ciA9IFwidmFsdWVcIixcbiAgICAgICAgY29sbGVjdGlvbiA9IG51bGwsXG4gICAgICAgIG1lbnVDb250YWluZXIgPSBudWxsLFxuICAgICAgICBzY3JvbGxDb250YWluZXIgPSBudWxsLFxuICAgICAgICBub01hdGNoVGVtcGxhdGUgPSBudWxsLFxuICAgICAgICBoZWFkZXJUZW1wbGF0ZSA9IG51bGwsXG4gICAgICAgIHJlcXVpcmVMZWFkaW5nU3BhY2UgPSB0cnVlLFxuICAgICAgICBhbGxvd1NwYWNlcyA9IGZhbHNlLFxuICAgICAgICByZXBsYWNlVGV4dFN1ZmZpeCA9IG51bGwsXG4gICAgICAgIHBvc2l0aW9uTWVudSA9IHRydWUsXG4gICAgICAgIHNwYWNlU2VsZWN0c01hdGNoID0gZmFsc2UsXG4gICAgICAgIHNlbGVjdFdpdGhDb21tYSA9IGZhbHNlLFxuICAgICAgICBzZWFyY2hPcHRzID0ge30sXG4gICAgICAgIGVkaXRvciA9IHt9LFxuICAgICAgICBpc1ZhbGlkU2VsZWN0aW9uID0gbnVsbCxcbiAgICAgICAgbWVudUl0ZW1MaW1pdCA9IG51bGwsXG4gICAgfSkge1xuICAgICAgICB0aGlzLmF1dG9jb21wbGV0ZU1vZGUgPSBhdXRvY29tcGxldGVNb2RlO1xuICAgICAgICB0aGlzLm1lbnVTZWxlY3RlZCA9IDA7XG4gICAgICAgIHRoaXMuY3VycmVudCA9IHt9O1xuICAgICAgICB0aGlzLmlucHV0RXZlbnQgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5pc0FjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB0aGlzLm1lbnVDb250YWluZXIgPSBtZW51Q29udGFpbmVyO1xuICAgICAgICB0aGlzLnNjcm9sbENvbnRhaW5lciA9IHNjcm9sbENvbnRhaW5lcjtcbiAgICAgICAgdGhpcy5hbGxvd1NwYWNlcyA9IGFsbG93U3BhY2VzO1xuICAgICAgICB0aGlzLnJlcGxhY2VUZXh0U3VmZml4ID0gcmVwbGFjZVRleHRTdWZmaXg7XG4gICAgICAgIHRoaXMucG9zaXRpb25NZW51ID0gcG9zaXRpb25NZW51O1xuICAgICAgICB0aGlzLmhhc1RyYWlsaW5nU3BhY2UgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5zcGFjZVNlbGVjdHNNYXRjaCA9IHNwYWNlU2VsZWN0c01hdGNoO1xuICAgICAgICB0aGlzLnNlbGVjdFdpdGhDb21tYSA9IHNlbGVjdFdpdGhDb21tYTtcbiAgICAgICAgdGhpcy5pbnZhbGlkRXZlbnQgPSBkb2N1bWVudC5jcmVhdGVFdmVudChcIkV2ZW50XCIpO1xuICAgICAgICB0aGlzLmludmFsaWRFdmVudC5pbml0RXZlbnQoXCJpbnZhbGlkXCIsIHRydWUsIHRydWUpO1xuXG4gICAgICAgIGlmICh0aGlzLmF1dG9jb21wbGV0ZU1vZGUpIHtcbiAgICAgICAgICAgIHRyaWdnZXIgPSBcIlwiO1xuICAgICAgICAgICAgYWxsb3dTcGFjZXMgPSBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh2YWx1ZXMpIHtcbiAgICAgICAgICAgIHRoaXMuY29sbGVjdGlvbiA9IFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIC8vIHN5bWJvbCB0aGF0IHN0YXJ0cyB0aGUgbG9va3VwXG4gICAgICAgICAgICAgICAgICAgIHRyaWdnZXI6IHRyaWdnZXIsXG5cbiAgICAgICAgICAgICAgICAgICAgLy8gaXMgaXQgd3JhcHBlZCBpbiBhbiBpZnJhbWVcbiAgICAgICAgICAgICAgICAgICAgaWZyYW1lOiBpZnJhbWUsXG5cbiAgICAgICAgICAgICAgICAgICAgLy8gY2xhc3MgYXBwbGllZCB0byBzZWxlY3RlZCBpdGVtXG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdENsYXNzOiBzZWxlY3RDbGFzcyxcblxuICAgICAgICAgICAgICAgICAgICAvLyBmdW5jdGlvbiBjYWxsZWQgb24gc2VsZWN0IHRoYXQgcmV0dW5zIHRoZSBjb250ZW50IHRvIGluc2VydFxuICAgICAgICAgICAgICAgICAgICBzZWxlY3RUZW1wbGF0ZTogKHNlbGVjdFRlbXBsYXRlIHx8IFRyaWJ1dGUuZGVmYXVsdFNlbGVjdFRlbXBsYXRlKS5iaW5kKHRoaXMpLFxuXG4gICAgICAgICAgICAgICAgICAgIC8vIGZ1bmN0aW9uIGNhbGxlZCB0aGF0IHJldHVybnMgY29udGVudCBmb3IgYW4gaXRlbVxuICAgICAgICAgICAgICAgICAgICBtZW51SXRlbVRlbXBsYXRlOiAobWVudUl0ZW1UZW1wbGF0ZSB8fCBUcmlidXRlLmRlZmF1bHRNZW51SXRlbVRlbXBsYXRlKS5iaW5kKHRoaXMpLFxuXG4gICAgICAgICAgICAgICAgICAgIC8vIGZ1bmN0aW9uIGNhbGxlZCB3aGVuIG1lbnUgaXMgZW1wdHksIGRpc2FibGVzIGhpZGluZyBvZiBtZW51LlxuICAgICAgICAgICAgICAgICAgICBub01hdGNoVGVtcGxhdGU6ICh0ID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgdCA9PT0gXCJmdW5jdGlvblwiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHQuYmluZCh0aGlzKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBub01hdGNoVGVtcGxhdGUgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFwiXCI7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfS5iaW5kKHRoaXMpXG4gICAgICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgICAgICB9KShub01hdGNoVGVtcGxhdGUpLFxuXG4gICAgICAgICAgICAgICAgICAgIGhlYWRlclRlbXBsYXRlOiAodCA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIHQgPT09IFwiZnVuY3Rpb25cIikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0LmJpbmQodGhpcyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVhZGVyVGVtcGxhdGUgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFwiXCI7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfS5iaW5kKHRoaXMpXG4gICAgICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgICAgICB9KShoZWFkZXJUZW1wbGF0ZSksXG5cbiAgICAgICAgICAgICAgICAgICAgLy8gY29sdW1uIHRvIHNlYXJjaCBhZ2FpbnN0IGluIHRoZSBvYmplY3RcbiAgICAgICAgICAgICAgICAgICAgbG9va3VwOiBsb29rdXAsXG5cbiAgICAgICAgICAgICAgICAgICAgLy8gY29sdW1uIHRoYXQgY29udGFpbnMgdGhlIGNvbnRlbnQgdG8gaW5zZXJ0IGJ5IGRlZmF1bHRcbiAgICAgICAgICAgICAgICAgICAgZmlsbEF0dHI6IGZpbGxBdHRyLFxuXG4gICAgICAgICAgICAgICAgICAgIC8vIGFycmF5IG9mIG9iamVjdHMgb3IgYSBmdW5jdGlvbiByZXR1cm5pbmcgYW4gYXJyYXkgb2Ygb2JqZWN0c1xuICAgICAgICAgICAgICAgICAgICB2YWx1ZXM6IHZhbHVlcyxcblxuICAgICAgICAgICAgICAgICAgICByZXF1aXJlTGVhZGluZ1NwYWNlOiByZXF1aXJlTGVhZGluZ1NwYWNlLFxuXG4gICAgICAgICAgICAgICAgICAgIHNlYXJjaE9wdHM6IHNlYXJjaE9wdHMsXG5cbiAgICAgICAgICAgICAgICAgICAgZWRpdG9yOiBlZGl0b3IsXG5cbiAgICAgICAgICAgICAgICAgICAgc2VsZWN0V2l0aENvbW1hOiBzZWxlY3RXaXRoQ29tbWEsXG5cbiAgICAgICAgICAgICAgICAgICAgaXNWYWxpZFNlbGVjdGlvbjogKHQgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiB0ID09PSBcImZ1bmN0aW9uXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdC5iaW5kKHRoaXMpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzVmFsaWRTZWxlY3Rpb24gfHxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFwiXCI7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfS5iaW5kKHRoaXMpXG4gICAgICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgICAgICB9KShpc1ZhbGlkU2VsZWN0aW9uKSxcblxuICAgICAgICAgICAgICAgICAgICBtZW51SXRlbUxpbWl0OiBtZW51SXRlbUxpbWl0LFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBdO1xuICAgICAgICB9IGVsc2UgaWYgKGNvbGxlY3Rpb24pIHtcbiAgICAgICAgICAgIGlmICh0aGlzLmF1dG9jb21wbGV0ZU1vZGUpIGNvbnNvbGUud2FybihcIlRyaWJ1dGUgaW4gYXV0b2NvbXBsZXRlIG1vZGUgZG9lcyBub3Qgd29yayBmb3IgY29sbGVjdGlvbnNcIik7XG4gICAgICAgICAgICB0aGlzLmNvbGxlY3Rpb24gPSBjb2xsZWN0aW9uLm1hcChpdGVtID0+IHtcbiAgICAgICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgICAgICB0cmlnZ2VyOiBpdGVtLnRyaWdnZXIgfHwgdHJpZ2dlcixcbiAgICAgICAgICAgICAgICAgICAgaWZyYW1lOiBpdGVtLmlmcmFtZSB8fCBpZnJhbWUsXG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdENsYXNzOiBpdGVtLnNlbGVjdENsYXNzIHx8IHNlbGVjdENsYXNzLFxuICAgICAgICAgICAgICAgICAgICBzZWxlY3RUZW1wbGF0ZTogKGl0ZW0uc2VsZWN0VGVtcGxhdGUgfHwgVHJpYnV0ZS5kZWZhdWx0U2VsZWN0VGVtcGxhdGUpLmJpbmQodGhpcyksXG4gICAgICAgICAgICAgICAgICAgIG1lbnVJdGVtVGVtcGxhdGU6IChpdGVtLm1lbnVJdGVtVGVtcGxhdGUgfHwgVHJpYnV0ZS5kZWZhdWx0TWVudUl0ZW1UZW1wbGF0ZSkuYmluZCh0aGlzKSxcbiAgICAgICAgICAgICAgICAgICAgLy8gZnVuY3Rpb24gY2FsbGVkIHdoZW4gbWVudSBpcyBlbXB0eSwgZGlzYWJsZXMgaGlkaW5nIG9mIG1lbnUuXG4gICAgICAgICAgICAgICAgICAgIG5vTWF0Y2hUZW1wbGF0ZTogKHQgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiB0ID09PSBcImZ1bmN0aW9uXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdC5iaW5kKHRoaXMpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgICAgICAgICAgICAgfSkobm9NYXRjaFRlbXBsYXRlKSxcbiAgICAgICAgICAgICAgICAgICAgaGVhZGVyVGVtcGxhdGU6ICh0ID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgdCA9PT0gXCJmdW5jdGlvblwiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHQuYmluZCh0aGlzKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgICAgICAgICAgICAgIH0pKGhlYWRlclRlbXBsYXRlKSxcbiAgICAgICAgICAgICAgICAgICAgbG9va3VwOiBpdGVtLmxvb2t1cCB8fCBsb29rdXAsXG4gICAgICAgICAgICAgICAgICAgIGZpbGxBdHRyOiBpdGVtLmZpbGxBdHRyIHx8IGZpbGxBdHRyLFxuICAgICAgICAgICAgICAgICAgICB2YWx1ZXM6IGl0ZW0udmFsdWVzLFxuICAgICAgICAgICAgICAgICAgICByZXF1aXJlTGVhZGluZ1NwYWNlOiBpdGVtLnJlcXVpcmVMZWFkaW5nU3BhY2UsXG4gICAgICAgICAgICAgICAgICAgIHNlYXJjaE9wdHM6IGl0ZW0uc2VhcmNoT3B0cyB8fCBzZWFyY2hPcHRzLFxuICAgICAgICAgICAgICAgICAgICBlZGl0b3I6IGl0ZW0uZWRpdG9yIHx8IGVkaXRvcixcbiAgICAgICAgICAgICAgICAgICAgaXNWYWxpZFNlbGVjdGlvbjogKHQgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiB0ID09PSBcImZ1bmN0aW9uXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdC5iaW5kKHRoaXMpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgICAgICAgICAgICAgfSkoaXNWYWxpZFNlbGVjdGlvbiksXG4gICAgICAgICAgICAgICAgICAgIG1lbnVJdGVtTGltaXQ6IGl0ZW0ubWVudUl0ZW1MaW1pdCB8fCBtZW51SXRlbUxpbWl0LFxuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIltUcmlidXRlXSBObyBjb2xsZWN0aW9uIHNwZWNpZmllZC5cIik7XG4gICAgICAgIH1cblxuICAgICAgICBuZXcgVHJpYnV0ZVJhbmdlKHRoaXMpO1xuICAgICAgICBuZXcgVHJpYnV0ZUV2ZW50cyh0aGlzKTtcbiAgICAgICAgbmV3IFRyaWJ1dGVNZW51RXZlbnRzKHRoaXMpO1xuICAgICAgICBuZXcgVHJpYnV0ZVNlYXJjaCh0aGlzKTtcbiAgICB9XG5cbiAgICBzdGF0aWMgZGVmYXVsdFNlbGVjdFRlbXBsYXRlKGl0ZW0pIHtcbiAgICAgICAgaWYgKHR5cGVvZiBpdGVtID09PSBcInVuZGVmaW5lZFwiKSByZXR1cm4gbnVsbDtcbiAgICAgICAgaWYgKHRoaXMucmFuZ2UuaXNDb250ZW50RWRpdGFibGUodGhpcy5jdXJyZW50LmVsZW1lbnQpKSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgICc8c3BhbiBjbGFzcz1cInRyaWJ1dGUtbWVudGlvblwiPicgK1xuICAgICAgICAgICAgICAgICh0aGlzLmN1cnJlbnQuY29sbGVjdGlvbi50cmlnZ2VyICsgaXRlbS5vcmlnaW5hbFt0aGlzLmN1cnJlbnQuY29sbGVjdGlvbi5maWxsQXR0cl0pICtcbiAgICAgICAgICAgICAgICBcIjwvc3Bhbj5cIlxuICAgICAgICAgICAgKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiB0aGlzLmN1cnJlbnQuY29sbGVjdGlvbi50cmlnZ2VyICsgaXRlbS5vcmlnaW5hbFt0aGlzLmN1cnJlbnQuY29sbGVjdGlvbi5maWxsQXR0cl07XG4gICAgfVxuXG4gICAgc3RhdGljIGRlZmF1bHRNZW51SXRlbVRlbXBsYXRlKG1hdGNoSXRlbSkge1xuICAgICAgICByZXR1cm4gbWF0Y2hJdGVtLnN0cmluZztcbiAgICB9XG5cbiAgICBzdGF0aWMgaW5wdXRUeXBlcygpIHtcbiAgICAgICAgcmV0dXJuIFtcIlRFWFRBUkVBXCIsIFwiSU5QVVRcIl07XG4gICAgfVxuXG4gICAgdHJpZ2dlcnMoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmNvbGxlY3Rpb24ubWFwKGNvbmZpZyA9PiB7XG4gICAgICAgICAgICByZXR1cm4gY29uZmlnLnRyaWdnZXI7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGF0dGFjaChlbCwgZWRpdG9yKSB7XG4gICAgICAgIGlmICghZWwpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIltUcmlidXRlXSBNdXN0IHBhc3MgaW4gYSBET00gbm9kZSBvciBOb2RlTGlzdC5cIik7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBDaGVjayBpZiBpdCBpcyBhIGpRdWVyeSBjb2xsZWN0aW9uXG4gICAgICAgIGlmICh0eXBlb2YgalF1ZXJ5ICE9PSBcInVuZGVmaW5lZFwiICYmIGVsIGluc3RhbmNlb2Ygd2luZG93LmpRdWVyeSkge1xuICAgICAgICAgICAgZWwgPSBlbC5nZXQoKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIElzIGVsIGFuIEFycmF5L0FycmF5LWxpa2Ugb2JqZWN0P1xuICAgICAgICBpZiAoZWwuY29uc3RydWN0b3IgPT09IE5vZGVMaXN0IHx8IGVsLmNvbnN0cnVjdG9yID09PSBIVE1MQ29sbGVjdGlvbiB8fCBlbC5jb25zdHJ1Y3RvciA9PT0gQXJyYXkpIHtcbiAgICAgICAgICAgIGxldCBsZW5ndGggPSBlbC5sZW5ndGg7XG4gICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxlbmd0aDsgKytpKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5fYXR0YWNoKGVsW2ldKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuX2F0dGFjaChlbCwgZWRpdG9yKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIF9hdHRhY2goZWwsIGVkaXRvcikge1xuICAgICAgICBpZiAoZWwuaGFzQXR0cmlidXRlKFwiZGF0YS10cmlidXRlXCIpKSB7XG4gICAgICAgICAgICBjb25zb2xlLndhcm4oXCJUcmlidXRlIHdhcyBhbHJlYWR5IGJvdW5kIHRvIFwiICsgZWwubm9kZU5hbWUpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5lbnN1cmVFZGl0YWJsZShlbCk7XG4gICAgICAgIHRoaXMuZXZlbnRzLmJpbmQoZWwsIGVkaXRvcik7XG5cbiAgICAgICAgaWYgKHRoaXMuc2Nyb2xsQ29udGFpbmVyKSB7XG4gICAgICAgICAgICB0aGlzLnNjcm9sbENvbnRhaW5lci5hZGRFdmVudExpc3RlbmVyKFwic2Nyb2xsXCIsIHRoaXMuc2Nyb2xsRXZlbnQuYmluZCh0aGlzKSk7XG4gICAgICAgIH1cblxuICAgICAgICBlbC5zZXRBdHRyaWJ1dGUoXCJkYXRhLXRyaWJ1dGVcIiwgdHJ1ZSk7XG4gICAgfVxuXG4gICAgc2Nyb2xsRXZlbnQoZSkge1xuICAgICAgICB0aGlzLmV2ZW50cy5zY3JvbGwodGhpcywgZSk7XG4gICAgfVxuXG4gICAgZW5zdXJlRWRpdGFibGUoZWxlbWVudCkge1xuICAgICAgICBpZiAoVHJpYnV0ZS5pbnB1dFR5cGVzKCkuaW5kZXhPZihlbGVtZW50Lm5vZGVOYW1lKSA9PT0gLTEpIHtcbiAgICAgICAgICAgIGlmIChlbGVtZW50LmNvbnRlbnRFZGl0YWJsZSkge1xuICAgICAgICAgICAgICAgIGVsZW1lbnQuY29udGVudEVkaXRhYmxlID0gdHJ1ZTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiW1RyaWJ1dGVdIENhbm5vdCBiaW5kIHRvIFwiICsgZWxlbWVudC5ub2RlTmFtZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBjcmVhdGVNZW51KCkge1xuICAgICAgICBsZXQgd3JhcHBlciA9IHRoaXMucmFuZ2UuZ2V0RG9jdW1lbnQoKS5jcmVhdGVFbGVtZW50KFwiZGl2XCIpLFxuICAgICAgICAgICAgdWwgPSB0aGlzLnJhbmdlLmdldERvY3VtZW50KCkuY3JlYXRlRWxlbWVudChcInVsXCIpO1xuXG4gICAgICAgIHdyYXBwZXIuY2xhc3NOYW1lID0gXCJ0cmlidXRlLWNvbnRhaW5lclwiO1xuICAgICAgICB3cmFwcGVyLmFwcGVuZENoaWxkKHVsKTtcblxuICAgICAgICBpZiAodGhpcy5tZW51Q29udGFpbmVyKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5tZW51Q29udGFpbmVyLmFwcGVuZENoaWxkKHdyYXBwZXIpO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHRoaXMucmFuZ2UuZ2V0RG9jdW1lbnQoKS5ib2R5LmFwcGVuZENoaWxkKHdyYXBwZXIpO1xuICAgIH1cblxuICAgIHNob3dNZW51Rm9yKGVsZW1lbnQsIHNjcm9sbFRvKSB7XG4gICAgICAgIC8vIE9ubHkgcHJvY2VlZCBpZiBtZW51IGlzbid0IGFscmVhZHkgc2hvd24gZm9yIHRoZSBjdXJyZW50IGVsZW1lbnQgJiBtZW50aW9uVGV4dFxuICAgICAgICBpZiAoXG4gICAgICAgICAgICB0aGlzLmlzQWN0aXZlICYmXG4gICAgICAgICAgICB0aGlzLmN1cnJlbnQuZWxlbWVudCA9PT0gZWxlbWVudCAmJlxuICAgICAgICAgICAgdGhpcy5jdXJyZW50Lm1lbnRpb25UZXh0ID09PSB0aGlzLmN1cnJlbnRNZW50aW9uVGV4dFNuYXBzaG90XG4gICAgICAgICkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuY3VycmVudE1lbnRpb25UZXh0U25hcHNob3QgPSB0aGlzLmN1cnJlbnQubWVudGlvblRleHQ7XG5cbiAgICAgICAgLy8gY3JlYXRlIHRoZSBtZW51IGlmIGl0IGRvZXNuJ3QgZXhpc3QuXG4gICAgICAgIGlmICghdGhpcy5tZW51KSB7XG4gICAgICAgICAgICB0aGlzLm1lbnUgPSB0aGlzLmNyZWF0ZU1lbnUoKTtcbiAgICAgICAgICAgIGVsZW1lbnQudHJpYnV0ZU1lbnUgPSB0aGlzLm1lbnU7XG4gICAgICAgICAgICB0aGlzLm1lbnVFdmVudHMuYmluZCh0aGlzLm1lbnUpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5pc0FjdGl2ZSA9IHRydWU7XG4gICAgICAgIHRoaXMubWVudVNlbGVjdGVkID0gMDtcblxuICAgICAgICBpZiAoIXRoaXMuY3VycmVudC5tZW50aW9uVGV4dCkge1xuICAgICAgICAgICAgdGhpcy5jdXJyZW50Lm1lbnRpb25UZXh0ID0gXCJcIjtcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IHByb2Nlc3NWYWx1ZXMgPSAodmFsdWVzLCB0ZXh0KSA9PiB7XG4gICAgICAgICAgICAvLyBUcmlidXRlIG1heSBub3QgYmUgYWN0aXZlIGFueSBtb3JlIGJ5IHRoZSB0aW1lIHRoZSB2YWx1ZSBjYWxsYmFjayByZXR1cm5zXG4gICAgICAgICAgICBpZiAoIXRoaXMuaXNBY3RpdmUpIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGxldCBpdGVtcyA9IHRoaXMuc2VhcmNoLmZpbHRlcih0aGlzLmN1cnJlbnQubWVudGlvblRleHQsIHZhbHVlcywge1xuICAgICAgICAgICAgICAgIHByZTogdGhpcy5jdXJyZW50LmNvbGxlY3Rpb24uc2VhcmNoT3B0cy5wcmUgfHwgXCI8c3Bhbj5cIixcbiAgICAgICAgICAgICAgICBwb3N0OiB0aGlzLmN1cnJlbnQuY29sbGVjdGlvbi5zZWFyY2hPcHRzLnBvc3QgfHwgXCI8L3NwYW4+XCIsXG4gICAgICAgICAgICAgICAgZXh0cmFjdDogZWwgPT4ge1xuICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIHRoaXMuY3VycmVudC5jb2xsZWN0aW9uLmxvb2t1cCA9PT0gXCJzdHJpbmdcIikge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGVsW3RoaXMuY3VycmVudC5jb2xsZWN0aW9uLmxvb2t1cF07XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIHRoaXMuY3VycmVudC5jb2xsZWN0aW9uLmxvb2t1cCA9PT0gXCJmdW5jdGlvblwiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5jdXJyZW50LmNvbGxlY3Rpb24ubG9va3VwKGVsLCB0aGlzLmN1cnJlbnQubWVudGlvblRleHQpO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiSW52YWxpZCBsb29rdXAgYXR0cmlidXRlLCBsb29rdXAgbXVzdCBiZSBzdHJpbmcgb3IgZnVuY3Rpb24uXCIpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICB0aGlzLmN1cnJlbnQuZmlsdGVyZWRJdGVtcyA9IGl0ZW1zO1xuXG4gICAgICAgICAgICBsZXQgdWwgPSB0aGlzLm1lbnUucXVlcnlTZWxlY3RvcihcInVsXCIpO1xuXG4gICAgICAgICAgICB0aGlzLnJhbmdlLnBvc2l0aW9uTWVudUF0Q2FyZXQoc2Nyb2xsVG8pO1xuXG4gICAgICAgICAgICBpZiAodGhpcy5jdXJyZW50LmNvbGxlY3Rpb24uaGVhZGVyVGVtcGxhdGUgJiYgdGhpcy5jdXJyZW50LmNvbGxlY3Rpb24uaGVhZGVyVGVtcGxhdGUodGV4dCkpIHtcbiAgICAgICAgICAgICAgICBjb25zdCBoZWFkZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO1xuICAgICAgICAgICAgICAgIGhlYWRlci5zZXRBdHRyaWJ1dGUoXCJjbGFzc1wiLCBcImhlYWRlclwiKTtcbiAgICAgICAgICAgICAgICBoZWFkZXIuaW5uZXJIVE1MID0gdGhpcy5jdXJyZW50LmNvbGxlY3Rpb24uaGVhZGVyVGVtcGxhdGUodGV4dCk7XG4gICAgICAgICAgICAgICAgY29uc3Qgb2xkSGVhZGVyID0gdGhpcy5tZW51LnF1ZXJ5U2VsZWN0b3IoXCIuaGVhZGVyXCIpO1xuICAgICAgICAgICAgICAgIGlmIChvbGRIZWFkZXIgJiYgb2xkSGVhZGVyLnJlbW92ZSkge1xuICAgICAgICAgICAgICAgICAgICBvbGRIZWFkZXIucmVtb3ZlKCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChvbGRIZWFkZXIgJiYgIW9sZEhlYWRlci5yZW1vdmUpIHtcbiAgICAgICAgICAgICAgICAgICAgb2xkSGVhZGVyLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQob2xkSGVhZGVyKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdGhpcy5tZW51Lmluc2VydEJlZm9yZShoZWFkZXIsIHRoaXMubWVudS5jaGlsZE5vZGVzWzBdKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKCFpdGVtcy5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICBsZXQgbm9NYXRjaEV2ZW50ID0gbmV3IEN1c3RvbUV2ZW50KFwidHJpYnV0ZS1uby1tYXRjaFwiLCB7IGRldGFpbDogdGhpcy5tZW51IH0pO1xuICAgICAgICAgICAgICAgIHRoaXMuY3VycmVudC5lbGVtZW50LmRpc3BhdGNoRXZlbnQobm9NYXRjaEV2ZW50KTtcbiAgICAgICAgICAgICAgICBpZiAoIXRoaXMuY3VycmVudC5jb2xsZWN0aW9uLm5vTWF0Y2hUZW1wbGF0ZSkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmhpZGVNZW51KCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgdWwuaW5uZXJIVE1MID0gdGhpcy5jdXJyZW50LmNvbGxlY3Rpb24ubm9NYXRjaFRlbXBsYXRlKCk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodGhpcy5jdXJyZW50LmNvbGxlY3Rpb24ubWVudUl0ZW1MaW1pdCkge1xuICAgICAgICAgICAgICAgIGl0ZW1zID0gaXRlbXMuc2xpY2UoMCwgdGhpcy5jdXJyZW50LmNvbGxlY3Rpb24ubWVudUl0ZW1MaW1pdCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHVsLmlubmVySFRNTCA9IFwiXCI7XG4gICAgICAgICAgICBsZXQgZnJhZ21lbnQgPSB0aGlzLnJhbmdlLmdldERvY3VtZW50KCkuY3JlYXRlRG9jdW1lbnRGcmFnbWVudCgpO1xuXG4gICAgICAgICAgICBpdGVtcy5mb3JFYWNoKChpdGVtLCBpbmRleCkgPT4ge1xuICAgICAgICAgICAgICAgIGxldCBsaSA9IHRoaXMucmFuZ2UuZ2V0RG9jdW1lbnQoKS5jcmVhdGVFbGVtZW50KFwibGlcIik7XG4gICAgICAgICAgICAgICAgbGkuc2V0QXR0cmlidXRlKFwiZGF0YS1pbmRleFwiLCBpbmRleCk7XG4gICAgICAgICAgICAgICAgbGkuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlbW92ZVwiLCBlID0+IHtcbiAgICAgICAgICAgICAgICAgICAgbGV0IGxpID0gZS50YXJnZXQ7XG4gICAgICAgICAgICAgICAgICAgIGxldCBpbmRleCA9XG4gICAgICAgICAgICAgICAgICAgICAgICBsaS5nZXRBdHRyaWJ1dGUoXCJkYXRhLWluZGV4XCIpIHx8IChsaS5wYXJlbnROb2RlICYmIGxpLnBhcmVudE5vZGUuZ2V0QXR0cmlidXRlKFwiZGF0YS1pbmRleFwiKSk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChlLm1vdmVtZW50WSAhPT0gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ldmVudHMuc2V0QWN0aXZlTGkoaW5kZXgpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgaWYgKHRoaXMubWVudVNlbGVjdGVkID09PSBpbmRleCkge1xuICAgICAgICAgICAgICAgICAgICBsaS5jbGFzc05hbWUgPSB0aGlzLmN1cnJlbnQuY29sbGVjdGlvbi5zZWxlY3RDbGFzcztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgbGkuaW5uZXJIVE1MID0gdGhpcy5jdXJyZW50LmNvbGxlY3Rpb24ubWVudUl0ZW1UZW1wbGF0ZShpdGVtKTtcbiAgICAgICAgICAgICAgICBmcmFnbWVudC5hcHBlbmRDaGlsZChsaSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHVsLmFwcGVuZENoaWxkKGZyYWdtZW50KTtcbiAgICAgICAgfTtcblxuICAgICAgICBpZiAodHlwZW9mIHRoaXMuY3VycmVudC5jb2xsZWN0aW9uLnZhbHVlcyA9PT0gXCJmdW5jdGlvblwiKSB7XG4gICAgICAgICAgICB0aGlzLmN1cnJlbnQuY29sbGVjdGlvbi52YWx1ZXModGhpcy5jdXJyZW50Lm1lbnRpb25UZXh0LCBwcm9jZXNzVmFsdWVzKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHByb2Nlc3NWYWx1ZXModGhpcy5jdXJyZW50LmNvbGxlY3Rpb24udmFsdWVzLCB0aGlzLmN1cnJlbnQubWVudGlvblRleHQpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMubWVudS5xdWVyeVNlbGVjdG9yKFwidWxcIikuc2Nyb2xsVG9wID0gMDtcbiAgICB9XG5cbiAgICBzaG93TWVudUZvckNvbGxlY3Rpb24oZWxlbWVudCwgY29sbGVjdGlvbkluZGV4KSB7XG4gICAgICAgIGlmIChlbGVtZW50ICE9PSBkb2N1bWVudC5hY3RpdmVFbGVtZW50KSB7XG4gICAgICAgICAgICB0aGlzLnBsYWNlQ2FyZXRBdEVuZChlbGVtZW50KTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuY3VycmVudC5jb2xsZWN0aW9uID0gdGhpcy5jb2xsZWN0aW9uW2NvbGxlY3Rpb25JbmRleCB8fCAwXTtcbiAgICAgICAgdGhpcy5jdXJyZW50LmV4dGVybmFsVHJpZ2dlciA9IHRydWU7XG4gICAgICAgIHRoaXMuY3VycmVudC5lbGVtZW50ID0gZWxlbWVudDtcblxuICAgICAgICBpZiAoZWxlbWVudC5pc0NvbnRlbnRFZGl0YWJsZSkgdGhpcy5pbnNlcnRUZXh0QXRDdXJzb3IodGhpcy5jdXJyZW50LmNvbGxlY3Rpb24udHJpZ2dlcik7XG4gICAgICAgIGVsc2UgdGhpcy5pbnNlcnRBdENhcmV0KGVsZW1lbnQsIHRoaXMuY3VycmVudC5jb2xsZWN0aW9uLnRyaWdnZXIpO1xuXG4gICAgICAgIHRoaXMuc2hvd01lbnVGb3IoZWxlbWVudCk7XG4gICAgfVxuXG4gICAgLy8gVE9ETzogbWFrZSBzdXJlIHRoaXMgd29ya3MgZm9yIGlucHV0cy90ZXh0YXJlYXNcbiAgICBwbGFjZUNhcmV0QXRFbmQoZWwpIHtcbiAgICAgICAgZWwuZm9jdXMoKTtcbiAgICAgICAgaWYgKHR5cGVvZiB3aW5kb3cuZ2V0U2VsZWN0aW9uICE9IFwidW5kZWZpbmVkXCIgJiYgdHlwZW9mIGRvY3VtZW50LmNyZWF0ZVJhbmdlICE9IFwidW5kZWZpbmVkXCIpIHtcbiAgICAgICAgICAgIHZhciByYW5nZSA9IGRvY3VtZW50LmNyZWF0ZVJhbmdlKCk7XG4gICAgICAgICAgICByYW5nZS5zZWxlY3ROb2RlQ29udGVudHMoZWwpO1xuICAgICAgICAgICAgcmFuZ2UuY29sbGFwc2UoZmFsc2UpO1xuICAgICAgICAgICAgdmFyIHNlbCA9IHdpbmRvdy5nZXRTZWxlY3Rpb24oKTtcbiAgICAgICAgICAgIHNlbC5yZW1vdmVBbGxSYW5nZXMoKTtcbiAgICAgICAgICAgIHNlbC5hZGRSYW5nZShyYW5nZSk7XG4gICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIGRvY3VtZW50LmJvZHkuY3JlYXRlVGV4dFJhbmdlICE9IFwidW5kZWZpbmVkXCIpIHtcbiAgICAgICAgICAgIHZhciB0ZXh0UmFuZ2UgPSBkb2N1bWVudC5ib2R5LmNyZWF0ZVRleHRSYW5nZSgpO1xuICAgICAgICAgICAgdGV4dFJhbmdlLm1vdmVUb0VsZW1lbnRUZXh0KGVsKTtcbiAgICAgICAgICAgIHRleHRSYW5nZS5jb2xsYXBzZShmYWxzZSk7XG4gICAgICAgICAgICB0ZXh0UmFuZ2Uuc2VsZWN0KCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvLyBmb3IgY29udGVudGVkaXRhYmxlXG4gICAgaW5zZXJ0VGV4dEF0Q3Vyc29yKHRleHQpIHtcbiAgICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lXG4gICAgICAgIHZhciBzZWwsIHJhbmdlLCBodG1sO1xuICAgICAgICBzZWwgPSB3aW5kb3cuZ2V0U2VsZWN0aW9uKCk7XG4gICAgICAgIHJhbmdlID0gc2VsLmdldFJhbmdlQXQoMCk7XG4gICAgICAgIHJhbmdlLmRlbGV0ZUNvbnRlbnRzKCk7XG4gICAgICAgIHZhciB0ZXh0Tm9kZSA9IGRvY3VtZW50LmNyZWF0ZVRleHROb2RlKHRleHQpO1xuICAgICAgICByYW5nZS5pbnNlcnROb2RlKHRleHROb2RlKTtcbiAgICAgICAgcmFuZ2Uuc2VsZWN0Tm9kZUNvbnRlbnRzKHRleHROb2RlKTtcbiAgICAgICAgcmFuZ2UuY29sbGFwc2UoZmFsc2UpO1xuICAgICAgICBzZWwucmVtb3ZlQWxsUmFuZ2VzKCk7XG4gICAgICAgIHNlbC5hZGRSYW5nZShyYW5nZSk7XG4gICAgfVxuXG4gICAgLy8gZm9yIHJlZ3VsYXIgaW5wdXRzXG4gICAgaW5zZXJ0QXRDYXJldCh0ZXh0YXJlYSwgdGV4dCkge1xuICAgICAgICB2YXIgc2Nyb2xsUG9zID0gdGV4dGFyZWEuc2Nyb2xsVG9wO1xuICAgICAgICB2YXIgY2FyZXRQb3MgPSB0ZXh0YXJlYS5zZWxlY3Rpb25TdGFydDtcblxuICAgICAgICB2YXIgZnJvbnQgPSB0ZXh0YXJlYS52YWx1ZS5zdWJzdHJpbmcoMCwgY2FyZXRQb3MpO1xuICAgICAgICB2YXIgYmFjayA9IHRleHRhcmVhLnZhbHVlLnN1YnN0cmluZyh0ZXh0YXJlYS5zZWxlY3Rpb25FbmQsIHRleHRhcmVhLnZhbHVlLmxlbmd0aCk7XG4gICAgICAgIHRleHRhcmVhLnZhbHVlID0gZnJvbnQgKyB0ZXh0ICsgYmFjaztcbiAgICAgICAgY2FyZXRQb3MgPSBjYXJldFBvcyArIHRleHQubGVuZ3RoO1xuICAgICAgICB0ZXh0YXJlYS5zZWxlY3Rpb25TdGFydCA9IGNhcmV0UG9zO1xuICAgICAgICB0ZXh0YXJlYS5zZWxlY3Rpb25FbmQgPSBjYXJldFBvcztcbiAgICAgICAgdGV4dGFyZWEuZm9jdXMoKTtcbiAgICAgICAgdGV4dGFyZWEuc2Nyb2xsVG9wID0gc2Nyb2xsUG9zO1xuICAgIH1cblxuICAgIGhpZGVNZW51KCkge1xuICAgICAgICBpZiAodGhpcy5tZW51KSB7XG4gICAgICAgICAgICB0aGlzLm1lbnUuc3R5bGUuY3NzVGV4dCA9IFwiZGlzcGxheTogbm9uZTtcIjtcbiAgICAgICAgICAgIHRoaXMuaXNBY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgICAgIHRoaXMubWVudVNlbGVjdGVkID0gMDtcbiAgICAgICAgICAgIHRoaXMuY3VycmVudCA9IHt9O1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgc2VsZWN0SXRlbUF0SW5kZXgoaW5kZXgsIG9yaWdpbmFsRXZlbnQpIHtcbiAgICAgICAgaW5kZXggPSBwYXJzZUludChpbmRleCk7XG4gICAgICAgIGlmICh0eXBlb2YgaW5kZXggIT09IFwibnVtYmVyXCIgfHwgaXNOYU4oaW5kZXgpKSByZXR1cm47XG4gICAgICAgIGxldCBpdGVtID0gdGhpcy5jdXJyZW50LmZpbHRlcmVkSXRlbXNbaW5kZXhdO1xuICAgICAgICBpZiAoXG4gICAgICAgICAgICB0eXBlb2YgdGhpcy5jdXJyZW50LmNvbGxlY3Rpb24uaXNWYWxpZFNlbGVjdGlvbiA9PT0gXCJmdW5jdGlvblwiICYmXG4gICAgICAgICAgICB0aGlzLmN1cnJlbnQuY29sbGVjdGlvbi5pc1ZhbGlkU2VsZWN0aW9uKGl0ZW0sIHRoaXMuY3VycmVudC5jb2xsZWN0aW9uLmVkaXRvcikgPT09IGZhbHNlXG4gICAgICAgICkge1xuICAgICAgICAgICAgdGhpcy5jdXJyZW50LmNvbGxlY3Rpb24uZWRpdG9yLmVsLmRpc3BhdGNoRXZlbnQodGhpcy5pbnZhbGlkRXZlbnQpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGxldCBjb250ZW50ID0gdGhpcy5jdXJyZW50LmNvbGxlY3Rpb24uc2VsZWN0VGVtcGxhdGUoaXRlbSk7XG4gICAgICAgIGlmIChjb250ZW50ICE9PSBudWxsKSB0aGlzLnJlcGxhY2VUZXh0KGNvbnRlbnQsIG9yaWdpbmFsRXZlbnQsIGl0ZW0pO1xuICAgIH1cblxuICAgIHJlcGxhY2VUZXh0KGNvbnRlbnQsIG9yaWdpbmFsRXZlbnQsIGl0ZW0pIHtcbiAgICAgICAgdGhpcy5yYW5nZS5yZXBsYWNlVHJpZ2dlclRleHQoY29udGVudCwgdHJ1ZSwgdHJ1ZSwgb3JpZ2luYWxFdmVudCwgaXRlbSk7XG4gICAgfVxuXG4gICAgX2FwcGVuZChjb2xsZWN0aW9uLCBuZXdWYWx1ZXMsIHJlcGxhY2UpIHtcbiAgICAgICAgaWYgKHR5cGVvZiBjb2xsZWN0aW9uLnZhbHVlcyA9PT0gXCJmdW5jdGlvblwiKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJVbmFibGUgdG8gYXBwZW5kIHRvIHZhbHVlcywgYXMgaXQgaXMgYSBmdW5jdGlvbi5cIik7XG4gICAgICAgIH0gZWxzZSBpZiAoIXJlcGxhY2UpIHtcbiAgICAgICAgICAgIGNvbGxlY3Rpb24udmFsdWVzID0gY29sbGVjdGlvbi52YWx1ZXMuY29uY2F0KG5ld1ZhbHVlcyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjb2xsZWN0aW9uLnZhbHVlcyA9IG5ld1ZhbHVlcztcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGFwcGVuZChjb2xsZWN0aW9uSW5kZXgsIG5ld1ZhbHVlcywgcmVwbGFjZSkge1xuICAgICAgICBsZXQgaW5kZXggPSBwYXJzZUludChjb2xsZWN0aW9uSW5kZXgpO1xuICAgICAgICBpZiAodHlwZW9mIGluZGV4ICE9PSBcIm51bWJlclwiKSB0aHJvdyBuZXcgRXJyb3IoXCJwbGVhc2UgcHJvdmlkZSBhbiBpbmRleCBmb3IgdGhlIGNvbGxlY3Rpb24gdG8gdXBkYXRlLlwiKTtcblxuICAgICAgICBsZXQgY29sbGVjdGlvbiA9IHRoaXMuY29sbGVjdGlvbltpbmRleF07XG5cbiAgICAgICAgdGhpcy5fYXBwZW5kKGNvbGxlY3Rpb24sIG5ld1ZhbHVlcywgcmVwbGFjZSk7XG4gICAgfVxuXG4gICAgYXBwZW5kQ3VycmVudChuZXdWYWx1ZXMsIHJlcGxhY2UpIHtcbiAgICAgICAgaWYgKHRoaXMuaXNBY3RpdmUpIHtcbiAgICAgICAgICAgIHRoaXMuX2FwcGVuZCh0aGlzLmN1cnJlbnQuY29sbGVjdGlvbiwgbmV3VmFsdWVzLCByZXBsYWNlKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIk5vIGFjdGl2ZSBzdGF0ZS4gUGxlYXNlIHVzZSBhcHBlbmQgaW5zdGVhZCBhbmQgcGFzcyBhbiBpbmRleC5cIik7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBkZXRhY2goZWwpIHtcbiAgICAgICAgaWYgKCFlbCkge1xuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiW1RyaWJ1dGVdIE11c3QgcGFzcyBpbiBhIERPTSBub2RlIG9yIE5vZGVMaXN0LlwiKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIENoZWNrIGlmIGl0IGlzIGEgalF1ZXJ5IGNvbGxlY3Rpb25cbiAgICAgICAgaWYgKHR5cGVvZiBqUXVlcnkgIT09IFwidW5kZWZpbmVkXCIgJiYgZWwgaW5zdGFuY2VvZiB3aW5kb3cualF1ZXJ5KSB7XG4gICAgICAgICAgICBlbCA9IGVsLmdldCgpO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gSXMgZWwgYW4gQXJyYXkvQXJyYXktbGlrZSBvYmplY3Q/XG4gICAgICAgIGlmIChlbC5jb25zdHJ1Y3RvciA9PT0gTm9kZUxpc3QgfHwgZWwuY29uc3RydWN0b3IgPT09IEhUTUxDb2xsZWN0aW9uIHx8IGVsLmNvbnN0cnVjdG9yID09PSBBcnJheSkge1xuICAgICAgICAgICAgbGV0IGxlbmd0aCA9IGVsLmxlbmd0aDtcbiAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuZ3RoOyArK2kpIHtcbiAgICAgICAgICAgICAgICB0aGlzLl9kZXRhY2goZWxbaV0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5fZGV0YWNoKGVsKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIF9kZXRhY2goZWwpIHtcbiAgICAgICAgdGhpcy5ldmVudHMudW5iaW5kKGVsKTtcbiAgICAgICAgaWYgKGVsLnRyaWJ1dGVNZW51KSB7XG4gICAgICAgICAgICB0aGlzLm1lbnVFdmVudHMudW5iaW5kKGVsLnRyaWJ1dGVNZW51KTtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5zY3JvbGxDb250YWluZXIpIHtcbiAgICAgICAgICAgIHRoaXMuc2Nyb2xsQ29udGFpbmVyLnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJzY3JvbGxcIiwgdGhpcy5zY3JvbGxFdmVudCk7XG4gICAgICAgIH1cblxuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgIGVsLnJlbW92ZUF0dHJpYnV0ZShcImRhdGEtdHJpYnV0ZVwiKTtcbiAgICAgICAgICAgIHRoaXMuaXNBY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgICAgIGlmIChlbC50cmlidXRlTWVudSkge1xuICAgICAgICAgICAgICAgIGVsLnRyaWJ1dGVNZW51LnJlbW92ZSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IFRyaWJ1dGU7XG4iLCJjbGFzcyBUcmlidXRlRXZlbnRzIHtcbiAgICBjb25zdHJ1Y3Rvcih0cmlidXRlKSB7XG4gICAgICAgIHRoaXMudHJpYnV0ZSA9IHRyaWJ1dGU7XG4gICAgICAgIHRoaXMudHJpYnV0ZS5ldmVudHMgPSB0aGlzO1xuICAgIH1cblxuICAgIHN0YXRpYyBrZXlzKCkge1xuICAgICAgICByZXR1cm4gW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGtleTogOSxcbiAgICAgICAgICAgICAgICB2YWx1ZTogXCJUQUJcIixcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAga2V5OiA4LFxuICAgICAgICAgICAgICAgIHZhbHVlOiBcIkRFTEVURVwiLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBrZXk6IDEzLFxuICAgICAgICAgICAgICAgIHZhbHVlOiBcIkVOVEVSXCIsXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGtleTogMjcsXG4gICAgICAgICAgICAgICAgdmFsdWU6IFwiRVNDQVBFXCIsXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGtleTogMzIsXG4gICAgICAgICAgICAgICAgdmFsdWU6IFwiU1BBQ0VcIixcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAga2V5OiAzOCxcbiAgICAgICAgICAgICAgICB2YWx1ZTogXCJVUFwiLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBrZXk6IDQwLFxuICAgICAgICAgICAgICAgIHZhbHVlOiBcIkRPV05cIixcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAga2V5OiAxODgsXG4gICAgICAgICAgICAgICAgdmFsdWU6IFwiQ09NTUFcIixcbiAgICAgICAgICAgIH0sXG4gICAgICAgIF07XG4gICAgfVxuXG4gICAgc3RhdGljIHJlbW92ZShlbGVtKSB7XG4gICAgICAgIGlmIChlbGVtICYmIGVsZW0ucmVtb3ZlKSB7XG4gICAgICAgICAgICBlbGVtLnJlbW92ZSgpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGlmIChlbGVtICYmICFlbGVtLnJlbW92ZSkge1xuICAgICAgICAgICAgZWxlbS5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKGVsZW0pO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgYmluZChlbGVtZW50LCBlZGl0b3IpIHtcbiAgICAgICAgZWxlbWVudC5ib3VuZEtleWRvd24gPSB0aGlzLmtleWRvd24uYmluZChlbGVtZW50LCB0aGlzLCBlZGl0b3IpO1xuICAgICAgICBlbGVtZW50LmJvdW5kS2V5dXAgPSB0aGlzLmtleXVwLmJpbmQoZWxlbWVudCwgdGhpcywgZWRpdG9yKTtcbiAgICAgICAgZWxlbWVudC5ib3VuZElucHV0ID0gdGhpcy5pbnB1dC5iaW5kKGVsZW1lbnQsIHRoaXMsIGVkaXRvcik7XG5cbiAgICAgICAgZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKFwia2V5ZG93blwiLCBlbGVtZW50LmJvdW5kS2V5ZG93biwgZmFsc2UpO1xuICAgICAgICBlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJrZXl1cFwiLCBlbGVtZW50LmJvdW5kS2V5dXAsIGZhbHNlKTtcbiAgICAgICAgZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKFwiaW5wdXRcIiwgZWxlbWVudC5ib3VuZElucHV0LCBmYWxzZSk7XG4gICAgfVxuXG4gICAgdW5iaW5kKGVsZW1lbnQpIHtcbiAgICAgICAgZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwia2V5ZG93blwiLCBlbGVtZW50LmJvdW5kS2V5ZG93biwgZmFsc2UpO1xuICAgICAgICBlbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJrZXl1cFwiLCBlbGVtZW50LmJvdW5kS2V5dXAsIGZhbHNlKTtcbiAgICAgICAgZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwiaW5wdXRcIiwgZWxlbWVudC5ib3VuZElucHV0LCBmYWxzZSk7XG5cbiAgICAgICAgZGVsZXRlIGVsZW1lbnQuYm91bmRLZXlkb3duO1xuICAgICAgICBkZWxldGUgZWxlbWVudC5ib3VuZEtleXVwO1xuICAgICAgICBkZWxldGUgZWxlbWVudC5ib3VuZElucHV0O1xuICAgIH1cblxuICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZVxuICAgIHNjcm9sbChpbnN0YW5jZSwgZSkge1xuICAgICAgICBpbnN0YW5jZS5pc0FjdGl2ZSA9IGZhbHNlO1xuICAgICAgICBpbnN0YW5jZS5oaWRlTWVudSgpO1xuICAgIH1cblxuICAgIGtleWRvd24oaW5zdGFuY2UsIGVkaXRvciwgZXZlbnQpIHtcbiAgICAgICAgaWYgKGluc3RhbmNlLnRyaWJ1dGUuaXNBY3RpdmUgJiYgWzE2LCAxNywgMTgsIDIwXS5pbmNsdWRlcyhldmVudC5rZXlDb2RlKSkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGlmIChpbnN0YW5jZS5zaG91bGREZWFjdGl2YXRlKGV2ZW50KSkge1xuICAgICAgICAgICAgaW5zdGFuY2UudHJpYnV0ZS5pc0FjdGl2ZSA9IGZhbHNlO1xuICAgICAgICAgICAgaW5zdGFuY2UudHJpYnV0ZS5oaWRlTWVudSgpO1xuICAgICAgICB9XG5cbiAgICAgICAgbGV0IGVsZW1lbnQgPSB0aGlzO1xuICAgICAgICBpbnN0YW5jZS5jb21tYW5kRXZlbnQgPSBmYWxzZTtcblxuICAgICAgICBUcmlidXRlRXZlbnRzLmtleXMoKS5mb3JFYWNoKG8gPT4ge1xuICAgICAgICAgICAgaWYgKG8ua2V5ID09PSBldmVudC5rZXlDb2RlKSB7XG4gICAgICAgICAgICAgICAgaW5zdGFuY2UuY29tbWFuZEV2ZW50ID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICBpbnN0YW5jZS5jYWxsYmFja3MoKVtvLnZhbHVlLnRvTG93ZXJDYXNlKCldKGV2ZW50LCBlbGVtZW50LCBlZGl0b3IpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBpbnB1dChpbnN0YW5jZSwgZXZlbnQsIGVkaXRvcikge1xuICAgICAgICBpbnN0YW5jZS5pbnB1dEV2ZW50ID0gdHJ1ZTtcbiAgICAgICAgaW5zdGFuY2Uua2V5dXAuY2FsbCh0aGlzLCBpbnN0YW5jZSwgZXZlbnQsIGVkaXRvcik7XG4gICAgfVxuXG4gICAgY2xpY2soaW5zdGFuY2UsIGV2ZW50KSB7XG4gICAgICAgIGxldCB0cmlidXRlID0gaW5zdGFuY2UudHJpYnV0ZTtcbiAgICAgICAgaWYgKHRyaWJ1dGUubWVudSAmJiB0cmlidXRlLm1lbnUuY29udGFpbnMoZXZlbnQudGFyZ2V0KSkge1xuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgaWYgKGV2ZW50LnRhcmdldC5nZXRBdHRyaWJ1dGUoXCJjbGFzc1wiKSA9PT0gXCJoZWFkZXJcIiB8fCBldmVudC50YXJnZXQudGFnTmFtZSA9PT0gXCJVTFwiKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbGV0IGxpID0gZXZlbnQudGFyZ2V0O1xuICAgICAgICAgICAgd2hpbGUgKGxpLm5vZGVOYW1lLnRvTG93ZXJDYXNlKCkgIT09IFwibGlcIikge1xuICAgICAgICAgICAgICAgIGxpID0gbGkucGFyZW50Tm9kZTtcbiAgICAgICAgICAgICAgICBpZiAoIWxpIHx8IGxpID09PSB0cmlidXRlLm1lbnUpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiY2Fubm90IGZpbmQgdGhlIDxsaT4gY29udGFpbmVyIGZvciB0aGUgY2xpY2tcIik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdHJpYnV0ZS5zZWxlY3RJdGVtQXRJbmRleChsaS5nZXRBdHRyaWJ1dGUoXCJkYXRhLWluZGV4XCIpLCBldmVudCk7XG4gICAgICAgICAgICB0cmlidXRlLmhpZGVNZW51KCk7XG5cbiAgICAgICAgICAgIC8vIFRPRE86IHNob3VsZCBmaXJlIHdpdGggZXh0ZXJuYWxUcmlnZ2VyIGFuZCB0YXJnZXQgaXMgb3V0c2lkZSBvZiBtZW51XG4gICAgICAgIH0gZWxzZSBpZiAodHJpYnV0ZS5jdXJyZW50LmVsZW1lbnQgJiYgIXRyaWJ1dGUuY3VycmVudC5leHRlcm5hbFRyaWdnZXIpIHtcbiAgICAgICAgICAgIHRyaWJ1dGUuY3VycmVudC5leHRlcm5hbFRyaWdnZXIgPSBmYWxzZTtcbiAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4gdHJpYnV0ZS5oaWRlTWVudSgpKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGtleXVwKGluc3RhbmNlLCBlZGl0b3IsIGV2ZW50KSB7XG4gICAgICAgIGlmIChpbnN0YW5jZS50cmlidXRlLmlzQWN0aXZlICYmIFsxNiwgMTcsIDE4LCAyMF0uaW5jbHVkZXMoZXZlbnQua2V5Q29kZSkpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChpbnN0YW5jZS5pbnB1dEV2ZW50KSB7XG4gICAgICAgICAgICBpbnN0YW5jZS5pbnB1dEV2ZW50ID0gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgaW5zdGFuY2UudXBkYXRlU2VsZWN0aW9uKHRoaXMpO1xuICAgICAgICBpZiAoZXZlbnQua2V5Q29kZSA9PT0gMjcpIHJldHVybjtcblxuICAgICAgICBpZiAoZWRpdG9yICYmIGVkaXRvci5jaGFyQ291bnRlciAmJiBlZGl0b3IuY2hhckNvdW50ZXIuY291bnQoKSA9PT0gMCkge1xuICAgICAgICAgICAgaW5zdGFuY2UudHJpYnV0ZS5pc0FjdGl2ZSA9IGZhbHNlO1xuICAgICAgICAgICAgaW5zdGFuY2UudHJpYnV0ZS5oaWRlTWVudSgpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCFpbnN0YW5jZS50cmlidXRlLmFsbG93U3BhY2VzICYmIGluc3RhbmNlLnRyaWJ1dGUuaGFzVHJhaWxpbmdTcGFjZSkge1xuICAgICAgICAgICAgaW5zdGFuY2UudHJpYnV0ZS5oYXNUcmFpbGluZ1NwYWNlID0gZmFsc2U7XG4gICAgICAgICAgICBpbnN0YW5jZS5jb21tYW5kRXZlbnQgPSB0cnVlO1xuICAgICAgICAgICAgaW5zdGFuY2UuY2FsbGJhY2tzKClbXCJzcGFjZVwiXShldmVudCwgdGhpcyk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoIWluc3RhbmNlLnRyaWJ1dGUuaXNBY3RpdmUpIHtcbiAgICAgICAgICAgIGlmIChpbnN0YW5jZS50cmlidXRlLmF1dG9jb21wbGV0ZU1vZGUpIHtcbiAgICAgICAgICAgICAgICBpbnN0YW5jZS5jYWxsYmFja3MoKS50cmlnZ2VyQ2hhcihldmVudCwgdGhpcywgXCJcIik7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGxldCBrZXlDb2RlID0gaW5zdGFuY2UuZ2V0S2V5Q29kZShpbnN0YW5jZSwgdGhpcywgZXZlbnQpO1xuXG4gICAgICAgICAgICAgICAgaWYgKGlzTmFOKGtleUNvZGUpIHx8ICFrZXlDb2RlKSByZXR1cm47XG5cbiAgICAgICAgICAgICAgICBsZXQgdHJpZ2dlciA9IGluc3RhbmNlLnRyaWJ1dGUudHJpZ2dlcnMoKS5maW5kKHRyaWdnZXIgPT4ge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJpZ2dlci5jaGFyQ29kZUF0KDApID09PSBrZXlDb2RlO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiB0cmlnZ2VyICE9PSBcInVuZGVmaW5lZFwiKSB7XG4gICAgICAgICAgICAgICAgICAgIGluc3RhbmNlLmNhbGxiYWNrcygpLnRyaWdnZXJDaGFyKGV2ZW50LCB0aGlzLCB0cmlnZ2VyLCB0cnVlKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoXG4gICAgICAgICAgICAoKGluc3RhbmNlLnRyaWJ1dGUuY3VycmVudC50cmlnZ2VyIHx8IGluc3RhbmNlLnRyaWJ1dGUuYXV0b2NvbXBsZXRlTW9kZSkgJiZcbiAgICAgICAgICAgICAgICBpbnN0YW5jZS5jb21tYW5kRXZlbnQgPT09IGZhbHNlKSB8fFxuICAgICAgICAgICAgKGluc3RhbmNlLnRyaWJ1dGUuaXNBY3RpdmUgJiYgWzgsIDQ2XS5pbmNsdWRlcyhldmVudC5rZXlDb2RlKSlcbiAgICAgICAgKSB7XG4gICAgICAgICAgICBpZiAoZXZlbnQua2V5Q29kZSA9PT0gODEpIHtcbiAgICAgICAgICAgICAgICBjb25zdCB0ZXh0ID0gaW5zdGFuY2UudHJpYnV0ZS5yYW5nZS5nZXRUZXh0UHJlY2VkaW5nQ3VycmVudFNlbGVjdGlvbigpO1xuICAgICAgICAgICAgICAgIGlmICh0ZXh0Lmxlbmd0aCA+IDEpIHtcbiAgICAgICAgICAgICAgICAgICAgaW5zdGFuY2UudHJpYnV0ZS5oaWRlTWVudSgpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgaW5zdGFuY2UudHJpYnV0ZS5zaG93TWVudUZvcih0aGlzLCB0cnVlKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGluc3RhbmNlLnRyaWJ1dGUuc2hvd01lbnVGb3IodGhpcywgdHJ1ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBzaG91bGREZWFjdGl2YXRlKGV2ZW50KSB7XG4gICAgICAgIGlmICghdGhpcy50cmlidXRlLmlzQWN0aXZlKSByZXR1cm4gZmFsc2U7XG5cbiAgICAgICAgaWYgKHRoaXMudHJpYnV0ZS5jdXJyZW50Lm1lbnRpb25UZXh0Lmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgbGV0IGV2ZW50S2V5UHJlc3NlZCA9IGZhbHNlO1xuICAgICAgICAgICAgVHJpYnV0ZUV2ZW50cy5rZXlzKCkuZm9yRWFjaChvID0+IHtcbiAgICAgICAgICAgICAgICBpZiAoZXZlbnQua2V5Q29kZSA9PT0gby5rZXkpIGV2ZW50S2V5UHJlc3NlZCA9IHRydWU7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgcmV0dXJuICFldmVudEtleVByZXNzZWQ7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZVxuICAgIGdldEtleUNvZGUoaW5zdGFuY2UsIGVsLCBldmVudCkge1xuICAgICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmVcbiAgICAgICAgbGV0IGNoYXI7XG4gICAgICAgIGxldCB0cmlidXRlID0gaW5zdGFuY2UudHJpYnV0ZTtcbiAgICAgICAgbGV0IGluZm8gPSB0cmlidXRlLnJhbmdlLmdldFRyaWdnZXJJbmZvKFxuICAgICAgICAgICAgZmFsc2UsXG4gICAgICAgICAgICB0cmlidXRlLmhhc1RyYWlsaW5nU3BhY2UsXG4gICAgICAgICAgICB0cnVlLFxuICAgICAgICAgICAgdHJpYnV0ZS5hbGxvd1NwYWNlcyxcbiAgICAgICAgICAgIHRyaWJ1dGUuYXV0b2NvbXBsZXRlTW9kZSxcbiAgICAgICAgKTtcblxuICAgICAgICBpZiAoaW5mbykge1xuICAgICAgICAgICAgcmV0dXJuIGluZm8ubWVudGlvblRyaWdnZXJDaGFyLmNoYXJDb2RlQXQoMCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICB1cGRhdGVTZWxlY3Rpb24oZWwpIHtcbiAgICAgICAgdGhpcy50cmlidXRlLmN1cnJlbnQuZWxlbWVudCA9IGVsO1xuICAgICAgICBsZXQgaW5mbyA9IHRoaXMudHJpYnV0ZS5yYW5nZS5nZXRUcmlnZ2VySW5mbyhcbiAgICAgICAgICAgIGZhbHNlLFxuICAgICAgICAgICAgdGhpcy50cmlidXRlLmhhc1RyYWlsaW5nU3BhY2UsXG4gICAgICAgICAgICB0cnVlLFxuICAgICAgICAgICAgdGhpcy50cmlidXRlLmFsbG93U3BhY2VzLFxuICAgICAgICAgICAgdGhpcy50cmlidXRlLmF1dG9jb21wbGV0ZU1vZGUsXG4gICAgICAgICk7XG5cbiAgICAgICAgaWYgKGluZm8pIHtcbiAgICAgICAgICAgIHRoaXMudHJpYnV0ZS5jdXJyZW50LnNlbGVjdGVkUGF0aCA9IGluZm8ubWVudGlvblNlbGVjdGVkUGF0aDtcbiAgICAgICAgICAgIHRoaXMudHJpYnV0ZS5jdXJyZW50Lm1lbnRpb25UZXh0ID0gaW5mby5tZW50aW9uVGV4dDtcbiAgICAgICAgICAgIHRoaXMudHJpYnV0ZS5jdXJyZW50LnNlbGVjdGVkT2Zmc2V0ID0gaW5mby5tZW50aW9uU2VsZWN0ZWRPZmZzZXQ7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBjYWxsYmFja3MoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICB0cmlnZ2VyQ2hhcjogKGUsIGVsLCB0cmlnZ2VyLCBzaG93TWVudSA9IGZhbHNlKSA9PiB7XG4gICAgICAgICAgICAgICAgY29uc3QgdGV4dCA9IHRoaXMudHJpYnV0ZS5yYW5nZS5nZXRUZXh0UHJlY2VkaW5nQ3VycmVudFNlbGVjdGlvbigpO1xuICAgICAgICAgICAgICAgIGNvbnN0IHdvcmRzID0gdGV4dC5zcGxpdCgvXFxzLyk7XG4gICAgICAgICAgICAgICAgY29uc3QgbGFzdFdvcmQgPSB3b3Jkc1t3b3Jkcy5sZW5ndGggLSAxXTtcbiAgICAgICAgICAgICAgICBpZiAobGFzdFdvcmQuc3BsaXQoXCJAXCIpLmxlbmd0aCAtIDEgPiAxKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYgKGxhc3RXb3JkLnRyaW0oKVswXSAhPT0gXCJAXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBsZXQgdHJpYnV0ZSA9IHRoaXMudHJpYnV0ZTtcbiAgICAgICAgICAgICAgICB0cmlidXRlLmN1cnJlbnQudHJpZ2dlciA9IHRyaWdnZXI7XG5cbiAgICAgICAgICAgICAgICBsZXQgY29sbGVjdGlvbkl0ZW0gPSB0cmlidXRlLmNvbGxlY3Rpb24uZmluZChpdGVtID0+IHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGl0ZW0udHJpZ2dlciA9PT0gdHJpZ2dlcjtcbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgIHRyaWJ1dGUuY3VycmVudC5jb2xsZWN0aW9uID0gY29sbGVjdGlvbkl0ZW07XG4gICAgICAgICAgICAgICAgaWYgKHRyaWJ1dGUuaW5wdXRFdmVudCB8fCBzaG93TWVudSkgdHJpYnV0ZS5zaG93TWVudUZvcihlbCwgdHJ1ZSk7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lXG4gICAgICAgICAgICBlbnRlcjogKGUsIGVsKSA9PiB7XG4gICAgICAgICAgICAgICAgLy8gY2hvb3NlIHNlbGVjdGlvblxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnRyaWJ1dGUuaXNBY3RpdmUgJiYgdGhpcy50cmlidXRlLmN1cnJlbnQuZmlsdGVyZWRJdGVtcykge1xuICAgICAgICAgICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50cmlidXRlLnNlbGVjdEl0ZW1BdEluZGV4KHRoaXMudHJpYnV0ZS5tZW51U2VsZWN0ZWQsIGUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50cmlidXRlLmhpZGVNZW51KCk7XG4gICAgICAgICAgICAgICAgICAgIH0sIDApO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBjb21tYTogKGUsIGVsKSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKHRoaXMudHJpYnV0ZS5pc0FjdGl2ZSkge1xuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy50cmlidXRlLnNlbGVjdFdpdGhDb21tYSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jYWxsYmFja3MoKS5lbnRlcihlLCBlbCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lXG4gICAgICAgICAgICBlc2NhcGU6IChlLCBlbCkgPT4ge1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLnRyaWJ1dGUuaXNBY3RpdmUpIHtcbiAgICAgICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnRyaWJ1dGUuaXNBY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50cmlidXRlLmhpZGVNZW51KCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHRhYjogKGUsIGVsKSA9PiB7XG4gICAgICAgICAgICAgICAgLy8gY2hvb3NlIGZpcnN0IG1hdGNoXG4gICAgICAgICAgICAgICAgdGhpcy5jYWxsYmFja3MoKS5lbnRlcihlLCBlbCk7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lXG4gICAgICAgICAgICBzcGFjZTogKGUsIGVsLCBlZGl0b3IpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAodGhpcy50cmlidXRlLmlzQWN0aXZlKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnRyaWJ1dGUuc3BhY2VTZWxlY3RzTWF0Y2gpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2FsbGJhY2tzKCkuZW50ZXIoZSwgZWwpO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKCF0aGlzLnRyaWJ1dGUuYWxsb3dTcGFjZXMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRyaWJ1dGUuaGlkZU1lbnUoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRyaWJ1dGUuaXNBY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sIDApO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGNvbnN0IHRleHQgPSB0aGlzLnRyaWJ1dGUucmFuZ2UuZ2V0VGV4dFByZWNlZGluZ0N1cnJlbnRTZWxlY3Rpb24oKS50cmltKCk7XG4gICAgICAgICAgICAgICAgaWYgKHRleHQubGFzdEluZGV4T2YodGhpcy50cmlidXRlLmN1cnJlbnQudHJpZ2dlcikgPT09IHRleHQubGVuZ3RoIC0gMSkge1xuICAgICAgICAgICAgICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudHJpYnV0ZS5oaWRlTWVudSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50cmlidXRlLmlzQWN0aXZlID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgIH0sIDApO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmVcbiAgICAgICAgICAgIHVwOiAoZSwgZWwpID0+IHtcbiAgICAgICAgICAgICAgICAvLyBuYXZpZ2F0ZSB1cCB1bFxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnRyaWJ1dGUuaXNBY3RpdmUgJiYgdGhpcy50cmlidXRlLmN1cnJlbnQuZmlsdGVyZWRJdGVtcykge1xuICAgICAgICAgICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAgICAgICAgICAgICAgIGxldCBjb3VudCA9IHRoaXMudHJpYnV0ZS5jdXJyZW50LmZpbHRlcmVkSXRlbXMubGVuZ3RoLFxuICAgICAgICAgICAgICAgICAgICAgICAgc2VsZWN0ZWQgPSB0aGlzLnRyaWJ1dGUubWVudVNlbGVjdGVkO1xuXG4gICAgICAgICAgICAgICAgICAgIGlmIChjb3VudCA+IHNlbGVjdGVkICYmIHNlbGVjdGVkID4gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50cmlidXRlLm1lbnVTZWxlY3RlZC0tO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRBY3RpdmVMaSgpO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHNlbGVjdGVkID09PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRyaWJ1dGUubWVudVNlbGVjdGVkID0gY291bnQgLSAxO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRBY3RpdmVMaSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50cmlidXRlLm1lbnUucXVlcnlTZWxlY3RvcihcInVsXCIpLnNjcm9sbFRvcCA9IHRoaXMudHJpYnV0ZS5tZW51LnF1ZXJ5U2VsZWN0b3IoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ1bFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgKS5vZmZzZXRIZWlnaHQ7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lXG4gICAgICAgICAgICBkb3duOiAoZSwgZWwpID0+IHtcbiAgICAgICAgICAgICAgICAvLyBuYXZpZ2F0ZSBkb3duIHVsXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMudHJpYnV0ZS5pc0FjdGl2ZSAmJiB0aGlzLnRyaWJ1dGUuY3VycmVudC5maWx0ZXJlZEl0ZW1zKSB7XG4gICAgICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgICAgICAgICAgICAgbGV0IGNvdW50ID0gdGhpcy50cmlidXRlLmN1cnJlbnQuZmlsdGVyZWRJdGVtcy5sZW5ndGggLSAxLFxuICAgICAgICAgICAgICAgICAgICAgICAgc2VsZWN0ZWQgPSB0aGlzLnRyaWJ1dGUubWVudVNlbGVjdGVkO1xuXG4gICAgICAgICAgICAgICAgICAgIGlmIChjb3VudCA+IHNlbGVjdGVkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRyaWJ1dGUubWVudVNlbGVjdGVkKys7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldEFjdGl2ZUxpKCk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoY291bnQgPT09IHNlbGVjdGVkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRyaWJ1dGUubWVudVNlbGVjdGVkID0gMDtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0QWN0aXZlTGkoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudHJpYnV0ZS5tZW51LnF1ZXJ5U2VsZWN0b3IoXCJ1bFwiKS5zY3JvbGxUb3AgPSAwO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGRlbGV0ZTogKGUsIGVsKSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKHRoaXMudHJpYnV0ZS5pc0FjdGl2ZSAmJiB0aGlzLnRyaWJ1dGUuY3VycmVudC5tZW50aW9uVGV4dC5sZW5ndGggPCAxKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMudHJpYnV0ZS5oaWRlTWVudSgpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy50cmlidXRlLmlzQWN0aXZlKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMudHJpYnV0ZS5zaG93TWVudUZvcihlbCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICghdGhpcy50cmlidXRlLmlzQWN0aXZlKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHRleHQgPSB0aGlzLnRyaWJ1dGUucmFuZ2UuZ2V0VGV4dFByZWNlZGluZ0N1cnJlbnRTZWxlY3Rpb24oKTtcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgd29yZHMgPSB0ZXh0LnNwbGl0KFwiIFwiKTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHdvcmRzW3dvcmRzLmxlbmd0aCAtIDFdLnNwbGl0KFwiQFwiKS5sZW5ndGggLSAxID4gMSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGlmICh3b3Jkc1t3b3Jkcy5sZW5ndGggLSAxXS50cmltKClbMF0gIT09IFwiQFwiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgdGhpcy50cmlidXRlLmlucHV0RXZlbnQgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmNhbGxiYWNrcygpLnRyaWdnZXJDaGFyKGUsIGVsLCB0aGlzLnRyaWJ1dGUuY3VycmVudC50cmlnZ2VyIHx8IFwiQFwiLCB0cnVlKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICB9O1xuICAgIH1cblxuICAgIHNldEFjdGl2ZUxpKGluZGV4KSB7XG4gICAgICAgIGxldCBsaXMgPSB0aGlzLnRyaWJ1dGUubWVudS5xdWVyeVNlbGVjdG9yQWxsKFwibGlcIiksXG4gICAgICAgICAgICBsZW5ndGggPSBsaXMubGVuZ3RoID4+PiAwO1xuXG4gICAgICAgIGlmIChpbmRleCkgdGhpcy50cmlidXRlLm1lbnVTZWxlY3RlZCA9IHBhcnNlSW50KGluZGV4KTtcblxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBsZXQgbGkgPSBsaXNbaV07XG4gICAgICAgICAgICBpZiAoaSA9PT0gdGhpcy50cmlidXRlLm1lbnVTZWxlY3RlZCkge1xuICAgICAgICAgICAgICAgIGxpLmNsYXNzTGlzdC5hZGQodGhpcy50cmlidXRlLmN1cnJlbnQuY29sbGVjdGlvbi5zZWxlY3RDbGFzcyk7XG5cbiAgICAgICAgICAgICAgICBsZXQgbGlDbGllbnRSZWN0ID0gbGkuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG4gICAgICAgICAgICAgICAgbGV0IG1lbnVDbGllbnRSZWN0ID0gdGhpcy50cmlidXRlLm1lbnUucXVlcnlTZWxlY3RvcihcInVsXCIpLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xuXG4gICAgICAgICAgICAgICAgaWYgKGxpQ2xpZW50UmVjdC5ib3R0b20gPiBtZW51Q2xpZW50UmVjdC5ib3R0b20pIHtcbiAgICAgICAgICAgICAgICAgICAgbGV0IHNjcm9sbERpc3RhbmNlID0gbGlDbGllbnRSZWN0LmJvdHRvbSAtIG1lbnVDbGllbnRSZWN0LmJvdHRvbTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50cmlidXRlLm1lbnUucXVlcnlTZWxlY3RvcihcInVsXCIpLnNjcm9sbFRvcCArPSBzY3JvbGxEaXN0YW5jZTtcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGxpQ2xpZW50UmVjdC50b3AgPCBtZW51Q2xpZW50UmVjdC50b3ApIHtcbiAgICAgICAgICAgICAgICAgICAgbGV0IHNjcm9sbERpc3RhbmNlID0gbWVudUNsaWVudFJlY3QudG9wIC0gbGlDbGllbnRSZWN0LnRvcDtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50cmlidXRlLm1lbnUucXVlcnlTZWxlY3RvcihcInVsXCIpLnNjcm9sbFRvcCAtPSBzY3JvbGxEaXN0YW5jZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGxpLmNsYXNzTGlzdC5yZW1vdmUodGhpcy50cmlidXRlLmN1cnJlbnQuY29sbGVjdGlvbi5zZWxlY3RDbGFzcyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBnZXRGdWxsSGVpZ2h0KGVsZW0sIGluY2x1ZGVNYXJnaW4pIHtcbiAgICAgICAgbGV0IGhlaWdodCA9IGVsZW0uZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkuaGVpZ2h0O1xuXG4gICAgICAgIGlmIChpbmNsdWRlTWFyZ2luKSB7XG4gICAgICAgICAgICBsZXQgc3R5bGUgPSBlbGVtLmN1cnJlbnRTdHlsZSB8fCB3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShlbGVtKTtcbiAgICAgICAgICAgIHJldHVybiBoZWlnaHQgKyBwYXJzZUZsb2F0KHN0eWxlLm1hcmdpblRvcCkgKyBwYXJzZUZsb2F0KHN0eWxlLm1hcmdpbkJvdHRvbSk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gaGVpZ2h0O1xuICAgIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgVHJpYnV0ZUV2ZW50cztcbiIsImNsYXNzIFRyaWJ1dGVNZW51RXZlbnRzIHtcbiAgICBjb25zdHJ1Y3Rvcih0cmlidXRlKSB7XG4gICAgICAgIHRoaXMudHJpYnV0ZSA9IHRyaWJ1dGU7XG4gICAgICAgIHRoaXMudHJpYnV0ZS5tZW51RXZlbnRzID0gdGhpcztcbiAgICAgICAgdGhpcy5tZW51ID0gdGhpcy50cmlidXRlLm1lbnU7XG4gICAgfVxuXG4gICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lXG4gICAgYmluZChtZW51KSB7XG4gICAgICAgIHRoaXMubWVudUNsaWNrRXZlbnQgPSB0aGlzLnRyaWJ1dGUuZXZlbnRzLmNsaWNrLmJpbmQobnVsbCwgdGhpcyk7XG4gICAgICAgIHRoaXMubWVudUNvbnRhaW5lclNjcm9sbEV2ZW50ID0gdGhpcy5kZWJvdW5jZShcbiAgICAgICAgICAgICgpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAodGhpcy50cmlidXRlLmlzQWN0aXZlKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMudHJpYnV0ZS5zaG93TWVudUZvcih0aGlzLnRyaWJ1dGUuY3VycmVudC5lbGVtZW50LCBmYWxzZSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIDMwMCxcbiAgICAgICAgICAgIGZhbHNlLFxuICAgICAgICApO1xuICAgICAgICB0aGlzLndpbmRvd1Jlc2l6ZUV2ZW50ID0gdGhpcy5kZWJvdW5jZShcbiAgICAgICAgICAgICgpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAodGhpcy50cmlidXRlLmlzQWN0aXZlKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMudHJpYnV0ZS5yYW5nZS5wb3NpdGlvbk1lbnVBdENhcmV0KHRydWUpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAzMDAsXG4gICAgICAgICAgICBmYWxzZSxcbiAgICAgICAgKTtcblxuICAgICAgICAvLyBmaXhlcyBJRTExIGlzc3VlcyB3aXRoIG1vdXNlZG93blxuICAgICAgICB0aGlzLnRyaWJ1dGUucmFuZ2UuZ2V0RG9jdW1lbnQoKS5hZGRFdmVudExpc3RlbmVyKFwiTVNQb2ludGVyRG93blwiLCB0aGlzLm1lbnVDbGlja0V2ZW50LCBmYWxzZSk7XG4gICAgICAgIHRoaXMudHJpYnV0ZS5yYW5nZS5nZXREb2N1bWVudCgpLmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZWRvd25cIiwgdGhpcy5tZW51Q2xpY2tFdmVudCwgZmFsc2UpO1xuICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcInJlc2l6ZVwiLCB0aGlzLndpbmRvd1Jlc2l6ZUV2ZW50KTtcblxuICAgICAgICBpZiAodGhpcy5tZW51Q29udGFpbmVyKSB7XG4gICAgICAgICAgICB0aGlzLm1lbnVDb250YWluZXIuYWRkRXZlbnRMaXN0ZW5lcihcInNjcm9sbFwiLCB0aGlzLm1lbnVDb250YWluZXJTY3JvbGxFdmVudCwgZmFsc2UpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJzY3JvbGxcIiwgdGhpcy5tZW51Q29udGFpbmVyU2Nyb2xsRXZlbnQpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lXG4gICAgdW5iaW5kKG1lbnUpIHtcbiAgICAgICAgdGhpcy50cmlidXRlLnJhbmdlLmdldERvY3VtZW50KCkucmVtb3ZlRXZlbnRMaXN0ZW5lcihcIm1vdXNlZG93blwiLCB0aGlzLm1lbnVDbGlja0V2ZW50LCBmYWxzZSk7XG4gICAgICAgIHRoaXMudHJpYnV0ZS5yYW5nZS5nZXREb2N1bWVudCgpLnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJNU1BvaW50ZXJEb3duXCIsIHRoaXMubWVudUNsaWNrRXZlbnQsIGZhbHNlKTtcbiAgICAgICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJyZXNpemVcIiwgdGhpcy53aW5kb3dSZXNpemVFdmVudCk7XG5cbiAgICAgICAgaWYgKHRoaXMubWVudUNvbnRhaW5lcikge1xuICAgICAgICAgICAgdGhpcy5tZW51Q29udGFpbmVyLnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJzY3JvbGxcIiwgdGhpcy5tZW51Q29udGFpbmVyU2Nyb2xsRXZlbnQsIGZhbHNlKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKFwic2Nyb2xsXCIsIHRoaXMubWVudUNvbnRhaW5lclNjcm9sbEV2ZW50KTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGRlYm91bmNlKGZ1bmMsIHdhaXQsIGltbWVkaWF0ZSkge1xuICAgICAgICB2YXIgdGltZW91dDtcbiAgICAgICAgcmV0dXJuICgpID0+IHtcbiAgICAgICAgICAgIHZhciBjb250ZXh0ID0gdGhpcyxcbiAgICAgICAgICAgICAgICBhcmdzID0gYXJndW1lbnRzO1xuICAgICAgICAgICAgdmFyIGxhdGVyID0gKCkgPT4ge1xuICAgICAgICAgICAgICAgIHRpbWVvdXQgPSBudWxsO1xuICAgICAgICAgICAgICAgIGlmICghaW1tZWRpYXRlKSBmdW5jLmFwcGx5KGNvbnRleHQsIGFyZ3MpO1xuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIHZhciBjYWxsTm93ID0gaW1tZWRpYXRlICYmICF0aW1lb3V0O1xuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KHRpbWVvdXQpO1xuICAgICAgICAgICAgdGltZW91dCA9IHNldFRpbWVvdXQobGF0ZXIsIHdhaXQpO1xuICAgICAgICAgICAgaWYgKGNhbGxOb3cpIGZ1bmMuYXBwbHkoY29udGV4dCwgYXJncyk7XG4gICAgICAgIH07XG4gICAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBUcmlidXRlTWVudUV2ZW50cztcbiIsIi8vIFRoYW5rcyB0byBodHRwczovL2dpdGh1Yi5jb20vamVmZi1jb2xsaW5zL21lbnQuaW9cbmNsYXNzIFRyaWJ1dGVSYW5nZSB7XG4gICAgY29uc3RydWN0b3IodHJpYnV0ZSkge1xuICAgICAgICB0aGlzLnRyaWJ1dGUgPSB0cmlidXRlO1xuICAgICAgICB0aGlzLnRyaWJ1dGUucmFuZ2UgPSB0aGlzO1xuICAgIH1cblxuICAgIGdldERvY3VtZW50KCkge1xuICAgICAgICBsZXQgaWZyYW1lO1xuICAgICAgICBpZiAodGhpcy50cmlidXRlLmN1cnJlbnQuY29sbGVjdGlvbikge1xuICAgICAgICAgICAgaWZyYW1lID0gdGhpcy50cmlidXRlLmN1cnJlbnQuY29sbGVjdGlvbi5pZnJhbWU7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoIWlmcmFtZSkge1xuICAgICAgICAgICAgcmV0dXJuIGRvY3VtZW50O1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIGlmcmFtZS5jb250ZW50V2luZG93LmRvY3VtZW50O1xuICAgIH1cblxuICAgIHBvc2l0aW9uTWVudUF0Q2FyZXQoc2Nyb2xsVG8pIHtcbiAgICAgICAgbGV0IGNvbnRleHQgPSB0aGlzLnRyaWJ1dGUuY3VycmVudCxcbiAgICAgICAgICAgIGNvb3JkaW5hdGVzO1xuXG4gICAgICAgIGxldCBpbmZvID0gdGhpcy5nZXRUcmlnZ2VySW5mbyhcbiAgICAgICAgICAgIGZhbHNlLFxuICAgICAgICAgICAgdGhpcy50cmlidXRlLmhhc1RyYWlsaW5nU3BhY2UsXG4gICAgICAgICAgICB0cnVlLFxuICAgICAgICAgICAgdGhpcy50cmlidXRlLmFsbG93U3BhY2VzLFxuICAgICAgICAgICAgdGhpcy50cmlidXRlLmF1dG9jb21wbGV0ZU1vZGUsXG4gICAgICAgICk7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBpbmZvICE9PSBcInVuZGVmaW5lZFwiKSB7XG4gICAgICAgICAgICBpZiAoIXRoaXMudHJpYnV0ZS5wb3NpdGlvbk1lbnUpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnRyaWJ1dGUubWVudS5zdHlsZS5jc3NUZXh0ID0gYGRpc3BsYXk6IGJsb2NrO2A7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoIXRoaXMuaXNDb250ZW50RWRpdGFibGUoY29udGV4dC5lbGVtZW50KSkge1xuICAgICAgICAgICAgICAgIGNvb3JkaW5hdGVzID0gdGhpcy5nZXRUZXh0QXJlYU9ySW5wdXRVbmRlcmxpbmVQb3NpdGlvbihcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50cmlidXRlLmN1cnJlbnQuZWxlbWVudCxcbiAgICAgICAgICAgICAgICAgICAgaW5mby5tZW50aW9uUG9zaXRpb24sXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgY29vcmRpbmF0ZXMgPSB0aGlzLmdldENvbnRlbnRFZGl0YWJsZUNhcmV0UG9zaXRpb24oaW5mby5tZW50aW9uUG9zaXRpb24pO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLnRyaWJ1dGUubWVudS5zdHlsZS5jc3NUZXh0ID0gYHRvcDogJHtjb29yZGluYXRlcy50b3B9cHg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGVmdDogJHtjb29yZGluYXRlcy5sZWZ0fXB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJpZ2h0OiAke2Nvb3JkaW5hdGVzLnJpZ2h0fXB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvdHRvbTogJHtjb29yZGluYXRlcy5ib3R0b219cHg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHotaW5kZXg6IDEwMDAwO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO2A7XG5cbiAgICAgICAgICAgIGlmIChjb29yZGluYXRlcy5sZWZ0ID09PSBcImF1dG9cIikge1xuICAgICAgICAgICAgICAgIHRoaXMudHJpYnV0ZS5tZW51LnN0eWxlLmxlZnQgPSBcImF1dG9cIjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKGNvb3JkaW5hdGVzLnRvcCA9PT0gXCJhdXRvXCIpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnRyaWJ1dGUubWVudS5zdHlsZS50b3AgPSBcImF1dG9cIjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHNjcm9sbFRvKSB0aGlzLnNjcm9sbEludG9WaWV3KCk7XG5cbiAgICAgICAgICAgIHdpbmRvdy5zZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgICAgICBsZXQgbWVudURpbWVuc2lvbnMgPSB7XG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiB0aGlzLnRyaWJ1dGUubWVudS5vZmZzZXRXaWR0aCxcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiB0aGlzLnRyaWJ1dGUubWVudS5vZmZzZXRIZWlnaHQsXG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICBsZXQgbWVudUlzT2ZmU2NyZWVuID0gdGhpcy5pc01lbnVPZmZTY3JlZW4oY29vcmRpbmF0ZXMsIG1lbnVEaW1lbnNpb25zKTtcblxuICAgICAgICAgICAgICAgIGxldCBtZW51SXNPZmZTY3JlZW5Ib3Jpem9udGFsbHkgPVxuICAgICAgICAgICAgICAgICAgICB3aW5kb3cuaW5uZXJXaWR0aCA+IG1lbnVEaW1lbnNpb25zLndpZHRoICYmIChtZW51SXNPZmZTY3JlZW4ubGVmdCB8fCBtZW51SXNPZmZTY3JlZW4ucmlnaHQpO1xuICAgICAgICAgICAgICAgIGxldCBtZW51SXNPZmZTY3JlZW5WZXJ0aWNhbGx5ID1cbiAgICAgICAgICAgICAgICAgICAgd2luZG93LmlubmVySGVpZ2h0ID4gbWVudURpbWVuc2lvbnMuaGVpZ2h0ICYmIChtZW51SXNPZmZTY3JlZW4udG9wIHx8IG1lbnVJc09mZlNjcmVlbi5ib3R0b20pO1xuICAgICAgICAgICAgICAgIGlmIChtZW51SXNPZmZTY3JlZW5Ib3Jpem9udGFsbHkgfHwgbWVudUlzT2ZmU2NyZWVuVmVydGljYWxseSkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnRyaWJ1dGUubWVudS5zdHlsZS5jc3NUZXh0ID0gXCJkaXNwbGF5OiBub25lXCI7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMucG9zaXRpb25NZW51QXRDYXJldChzY3JvbGxUbyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSwgMCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLnRyaWJ1dGUubWVudS5zdHlsZS5jc3NUZXh0ID0gXCJkaXNwbGF5OiBub25lXCI7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBzZWxlY3RFbGVtZW50KHRhcmdldEVsZW1lbnQsIHBhdGgsIG9mZnNldCkge1xuICAgICAgICBsZXQgcmFuZ2U7XG4gICAgICAgIGxldCBlbGVtID0gdGFyZ2V0RWxlbWVudDtcblxuICAgICAgICBpZiAocGF0aCkge1xuICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBwYXRoLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgZWxlbSA9IGVsZW0uY2hpbGROb2Rlc1twYXRoW2ldXTtcbiAgICAgICAgICAgICAgICBpZiAoZWxlbSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgd2hpbGUgKGVsZW0ubGVuZ3RoIDwgb2Zmc2V0KSB7XG4gICAgICAgICAgICAgICAgICAgIG9mZnNldCAtPSBlbGVtLmxlbmd0aDtcbiAgICAgICAgICAgICAgICAgICAgZWxlbSA9IGVsZW0ubmV4dFNpYmxpbmc7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmIChlbGVtLmNoaWxkTm9kZXMubGVuZ3RoID09PSAwICYmICFlbGVtLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICBlbGVtID0gZWxlbS5wcmV2aW91c1NpYmxpbmc7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGxldCBzZWwgPSB0aGlzLmdldFdpbmRvd1NlbGVjdGlvbigpO1xuXG4gICAgICAgIHJhbmdlID0gdGhpcy5nZXREb2N1bWVudCgpLmNyZWF0ZVJhbmdlKCk7XG4gICAgICAgIHJhbmdlLnNldFN0YXJ0KGVsZW0sIG9mZnNldCk7XG4gICAgICAgIHJhbmdlLnNldEVuZChlbGVtLCBvZmZzZXQpO1xuICAgICAgICByYW5nZS5jb2xsYXBzZSh0cnVlKTtcblxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgc2VsLnJlbW92ZUFsbFJhbmdlcygpO1xuICAgICAgICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lXG4gICAgICAgIH0gY2F0Y2ggKGVycm9yKSB7fVxuXG4gICAgICAgIHNlbC5hZGRSYW5nZShyYW5nZSk7XG4gICAgICAgIHRhcmdldEVsZW1lbnQuZm9jdXMoKTtcbiAgICB9XG5cbiAgICByZXBsYWNlVHJpZ2dlclRleHQodGV4dCwgcmVxdWlyZUxlYWRpbmdTcGFjZSwgaGFzVHJhaWxpbmdTcGFjZSwgb3JpZ2luYWxFdmVudCwgaXRlbSkge1xuICAgICAgICBsZXQgaW5mbyA9IHRoaXMuZ2V0VHJpZ2dlckluZm8oXG4gICAgICAgICAgICB0cnVlLFxuICAgICAgICAgICAgaGFzVHJhaWxpbmdTcGFjZSxcbiAgICAgICAgICAgIHJlcXVpcmVMZWFkaW5nU3BhY2UsXG4gICAgICAgICAgICB0aGlzLnRyaWJ1dGUuYWxsb3dTcGFjZXMsXG4gICAgICAgICAgICB0aGlzLnRyaWJ1dGUuYXV0b2NvbXBsZXRlTW9kZSxcbiAgICAgICAgKTtcblxuICAgICAgICBpZiAoaW5mbyAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBsZXQgY29udGV4dCA9IHRoaXMudHJpYnV0ZS5jdXJyZW50O1xuICAgICAgICAgICAgbGV0IHJlcGxhY2VFdmVudCA9IG5ldyBDdXN0b21FdmVudChcInRyaWJ1dGUtcmVwbGFjZWRcIiwge1xuICAgICAgICAgICAgICAgIGRldGFpbDoge1xuICAgICAgICAgICAgICAgICAgICBpdGVtOiBpdGVtLFxuICAgICAgICAgICAgICAgICAgICBpbnN0YW5jZTogY29udGV4dCxcbiAgICAgICAgICAgICAgICAgICAgY29udGV4dDogaW5mbyxcbiAgICAgICAgICAgICAgICAgICAgZXZlbnQ6IG9yaWdpbmFsRXZlbnQsXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBpZiAoIXRoaXMuaXNDb250ZW50RWRpdGFibGUoY29udGV4dC5lbGVtZW50KSkge1xuICAgICAgICAgICAgICAgIGxldCBteUZpZWxkID0gdGhpcy50cmlidXRlLmN1cnJlbnQuZWxlbWVudDtcbiAgICAgICAgICAgICAgICBsZXQgdGV4dFN1ZmZpeCA9XG4gICAgICAgICAgICAgICAgICAgIHR5cGVvZiB0aGlzLnRyaWJ1dGUucmVwbGFjZVRleHRTdWZmaXggPT0gXCJzdHJpbmdcIiA/IHRoaXMudHJpYnV0ZS5yZXBsYWNlVGV4dFN1ZmZpeCA6IFwiIFwiO1xuICAgICAgICAgICAgICAgIHRleHQgKz0gdGV4dFN1ZmZpeDtcbiAgICAgICAgICAgICAgICBsZXQgc3RhcnRQb3MgPSBpbmZvLm1lbnRpb25Qb3NpdGlvbjtcbiAgICAgICAgICAgICAgICBsZXQgZW5kUG9zID0gaW5mby5tZW50aW9uUG9zaXRpb24gKyBpbmZvLm1lbnRpb25UZXh0Lmxlbmd0aCArIHRleHRTdWZmaXgubGVuZ3RoO1xuICAgICAgICAgICAgICAgIG15RmllbGQudmFsdWUgPVxuICAgICAgICAgICAgICAgICAgICBteUZpZWxkLnZhbHVlLnN1YnN0cmluZygwLCBzdGFydFBvcykgKyB0ZXh0ICsgbXlGaWVsZC52YWx1ZS5zdWJzdHJpbmcoZW5kUG9zLCBteUZpZWxkLnZhbHVlLmxlbmd0aCk7XG4gICAgICAgICAgICAgICAgbXlGaWVsZC5zZWxlY3Rpb25TdGFydCA9IHN0YXJ0UG9zICsgdGV4dC5sZW5ndGg7XG4gICAgICAgICAgICAgICAgbXlGaWVsZC5zZWxlY3Rpb25FbmQgPSBzdGFydFBvcyArIHRleHQubGVuZ3RoO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAvLyBhZGQgYSBzcGFjZSB0byB0aGUgZW5kIG9mIHRoZSBwYXN0ZWQgdGV4dFxuICAgICAgICAgICAgICAgIGxldCB0ZXh0U3VmZml4ID1cbiAgICAgICAgICAgICAgICAgICAgdHlwZW9mIHRoaXMudHJpYnV0ZS5yZXBsYWNlVGV4dFN1ZmZpeCA9PSBcInN0cmluZ1wiID8gdGhpcy50cmlidXRlLnJlcGxhY2VUZXh0U3VmZml4IDogXCJcXHhBMFwiO1xuXG4gICAgICAgICAgICAgICAgaWYgKG9yaWdpbmFsRXZlbnQua2V5Q29kZSA9PT0gMTg4KSB7XG4gICAgICAgICAgICAgICAgICAgIHRleHRTdWZmaXggPSBcIixcIiArIHRleHRTdWZmaXg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHRleHQgKz0gdGV4dFN1ZmZpeDtcbiAgICAgICAgICAgICAgICB0aGlzLnBhc3RlSHRtbChcbiAgICAgICAgICAgICAgICAgICAgdGV4dCxcbiAgICAgICAgICAgICAgICAgICAgaW5mby5tZW50aW9uUG9zaXRpb24sXG4gICAgICAgICAgICAgICAgICAgIGluZm8ubWVudGlvblBvc2l0aW9uICsgaW5mby5tZW50aW9uVGV4dC5sZW5ndGggKyAhdGhpcy50cmlidXRlLmF1dG9jb21wbGV0ZU1vZGUsXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgY29udGV4dC5lbGVtZW50LmRpc3BhdGNoRXZlbnQocmVwbGFjZUV2ZW50KTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHBhc3RlSHRtbChodG1sLCBzdGFydFBvcywgZW5kUG9zKSB7XG4gICAgICAgIGxldCByYW5nZSwgc2VsLCBzZWxFbDtcbiAgICAgICAgc2VsID0gdGhpcy5nZXRXaW5kb3dTZWxlY3Rpb24oKTtcbiAgICAgICAgY29uc3QgeyBhbmNob3JPZmZzZXQsIGFuY2hvck5vZGUgfSA9IHNlbDtcbiAgICAgICAgaWYgKGFuY2hvck5vZGUuY2hpbGROb2RlcyAmJiBhbmNob3JOb2RlLmNoaWxkTm9kZXNbYW5jaG9yT2Zmc2V0XSkge1xuICAgICAgICAgICAgY29uc3QgY2hpbGQgPSBhbmNob3JOb2RlLmNoaWxkTm9kZXNbYW5jaG9yT2Zmc2V0XTtcbiAgICAgICAgICAgIGlmIChjaGlsZC5ub2RlVHlwZSA9PT0gTm9kZS5URVhUX05PREUpIHtcbiAgICAgICAgICAgICAgICBzZWxFbCA9IGNoaWxkO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChhbmNob3JPZmZzZXQgPiAwICYmIGNoaWxkLm5vZGVUeXBlID09PSBOb2RlLkVMRU1FTlRfTk9ERSkge1xuICAgICAgICAgICAgICAgIHNlbEVsID0gYW5jaG9yTm9kZS5jaGlsZE5vZGVzW2FuY2hvck9mZnNldCAtIDFdO1xuICAgICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgc2VsRWwgPSBzZWwuYW5jaG9yTm9kZTtcbiAgICAgICAgfVxuICAgICAgICByYW5nZSA9IHRoaXMuZ2V0RG9jdW1lbnQoKS5jcmVhdGVSYW5nZSgpO1xuICAgICAgICByYW5nZS5zZXRTdGFydChzZWxFbCwgc3RhcnRQb3MpO1xuICAgICAgICByYW5nZS5zZXRFbmQoc2VsRWwsIGVuZFBvcyk7XG4gICAgICAgIHJhbmdlLmRlbGV0ZUNvbnRlbnRzKCk7XG5cbiAgICAgICAgbGV0IGVsID0gdGhpcy5nZXREb2N1bWVudCgpLmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7XG4gICAgICAgIGVsLmlubmVySFRNTCA9IGh0bWw7XG4gICAgICAgIGxldCBmcmFnID0gdGhpcy5nZXREb2N1bWVudCgpLmNyZWF0ZURvY3VtZW50RnJhZ21lbnQoKSxcbiAgICAgICAgICAgIG5vZGUsXG4gICAgICAgICAgICBsYXN0Tm9kZTtcbiAgICAgICAgd2hpbGUgKChub2RlID0gZWwuZmlyc3RDaGlsZCkpIHtcbiAgICAgICAgICAgIGxhc3ROb2RlID0gZnJhZy5hcHBlbmRDaGlsZChub2RlKTtcbiAgICAgICAgfVxuICAgICAgICByYW5nZS5pbnNlcnROb2RlKGZyYWcpO1xuXG4gICAgICAgIC8vIFByZXNlcnZlIHRoZSBzZWxlY3Rpb25cbiAgICAgICAgaWYgKGxhc3ROb2RlKSB7XG4gICAgICAgICAgICByYW5nZSA9IHJhbmdlLmNsb25lUmFuZ2UoKTtcbiAgICAgICAgICAgIHJhbmdlLnNldFN0YXJ0QWZ0ZXIobGFzdE5vZGUpO1xuICAgICAgICAgICAgcmFuZ2UuY29sbGFwc2UodHJ1ZSk7XG4gICAgICAgICAgICBzZWwucmVtb3ZlQWxsUmFuZ2VzKCk7XG4gICAgICAgICAgICBzZWwuYWRkUmFuZ2UocmFuZ2UpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgZ2V0V2luZG93U2VsZWN0aW9uKCkge1xuICAgICAgICBpZiAodGhpcy50cmlidXRlLmNvbGxlY3Rpb24uaWZyYW1lKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy50cmlidXRlLmNvbGxlY3Rpb24uaWZyYW1lLmNvbnRlbnRXaW5kb3cuZ2V0U2VsZWN0aW9uKCk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gd2luZG93LmdldFNlbGVjdGlvbigpO1xuICAgIH1cblxuICAgIGdldE5vZGVQb3NpdGlvbkluUGFyZW50KGVsZW1lbnQpIHtcbiAgICAgICAgaWYgKGVsZW1lbnQucGFyZW50Tm9kZSA9PT0gbnVsbCkge1xuICAgICAgICAgICAgcmV0dXJuIDA7XG4gICAgICAgIH1cblxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGVsZW1lbnQucGFyZW50Tm9kZS5jaGlsZE5vZGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBsZXQgbm9kZSA9IGVsZW1lbnQucGFyZW50Tm9kZS5jaGlsZE5vZGVzW2ldO1xuXG4gICAgICAgICAgICBpZiAobm9kZSA9PT0gZWxlbWVudCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lXG4gICAgZ2V0Q29udGVudEVkaXRhYmxlU2VsZWN0ZWRQYXRoKGN0eCkge1xuICAgICAgICBsZXQgc2VsID0gdGhpcy5nZXRXaW5kb3dTZWxlY3Rpb24oKTtcbiAgICAgICAgbGV0IHNlbGVjdGVkID0gc2VsLmFuY2hvck5vZGU7XG4gICAgICAgIGxldCBwYXRoID0gW107XG4gICAgICAgIGxldCBvZmZzZXQ7XG5cbiAgICAgICAgaWYgKHNlbGVjdGVkICE9IG51bGwpIHtcbiAgICAgICAgICAgIGxldCBpO1xuICAgICAgICAgICAgbGV0IGNlID0gc2VsZWN0ZWQuY29udGVudEVkaXRhYmxlO1xuICAgICAgICAgICAgd2hpbGUgKHNlbGVjdGVkICE9PSBudWxsICYmIGNlICE9PSBcInRydWVcIikge1xuICAgICAgICAgICAgICAgIGkgPSB0aGlzLmdldE5vZGVQb3NpdGlvbkluUGFyZW50KHNlbGVjdGVkKTtcbiAgICAgICAgICAgICAgICBwYXRoLnB1c2goaSk7XG4gICAgICAgICAgICAgICAgc2VsZWN0ZWQgPSBzZWxlY3RlZC5wYXJlbnROb2RlO1xuICAgICAgICAgICAgICAgIGlmIChzZWxlY3RlZCAhPT0gbnVsbCkge1xuICAgICAgICAgICAgICAgICAgICBjZSA9IHNlbGVjdGVkLmNvbnRlbnRFZGl0YWJsZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBwYXRoLnJldmVyc2UoKTtcblxuICAgICAgICAgICAgLy8gZ2V0UmFuZ2VBdCBtYXkgbm90IGV4aXN0LCBuZWVkIGFsdGVybmF0aXZlXG4gICAgICAgICAgICBvZmZzZXQgPSBzZWwuZ2V0UmFuZ2VBdCgwKS5zdGFydE9mZnNldDtcblxuICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICBzZWxlY3RlZDogc2VsZWN0ZWQsXG4gICAgICAgICAgICAgICAgcGF0aDogcGF0aCxcbiAgICAgICAgICAgICAgICBvZmZzZXQ6IG9mZnNldCxcbiAgICAgICAgICAgIH07XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBnZXRUZXh0UHJlY2VkaW5nQ3VycmVudFNlbGVjdGlvbigpIHtcbiAgICAgICAgbGV0IGNvbnRleHQgPSB0aGlzLnRyaWJ1dGUuY3VycmVudCxcbiAgICAgICAgICAgIHRleHQgPSBcIlwiO1xuXG4gICAgICAgIGlmICghdGhpcy5pc0NvbnRlbnRFZGl0YWJsZShjb250ZXh0LmVsZW1lbnQpKSB7XG4gICAgICAgICAgICBsZXQgdGV4dENvbXBvbmVudCA9IHRoaXMudHJpYnV0ZS5jdXJyZW50LmVsZW1lbnQ7XG4gICAgICAgICAgICBpZiAodGV4dENvbXBvbmVudCkge1xuICAgICAgICAgICAgICAgIGxldCBzdGFydFBvcyA9IHRleHRDb21wb25lbnQuc2VsZWN0aW9uU3RhcnQ7XG4gICAgICAgICAgICAgICAgaWYgKHRleHRDb21wb25lbnQudmFsdWUgJiYgc3RhcnRQb3MgPj0gMCkge1xuICAgICAgICAgICAgICAgICAgICB0ZXh0ID0gdGV4dENvbXBvbmVudC52YWx1ZS5zdWJzdHJpbmcoMCwgc3RhcnRQb3MpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGxldCBzZWxlY3RlZEVsZW0gPSB0aGlzLmdldFdpbmRvd1NlbGVjdGlvbigpLmFuY2hvck5vZGU7XG5cbiAgICAgICAgICAgIGlmIChzZWxlY3RlZEVsZW0gIT0gbnVsbCkge1xuICAgICAgICAgICAgICAgIGNvbnN0IGFuY2hvck9mZnNldCA9IHRoaXMuZ2V0V2luZG93U2VsZWN0aW9uKCkuYW5jaG9yT2Zmc2V0O1xuICAgICAgICAgICAgICAgIGlmIChzZWxlY3RlZEVsZW0uY2hpbGROb2RlcyAmJiBzZWxlY3RlZEVsZW0uY2hpbGROb2Rlc1thbmNob3JPZmZzZXRdKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGNoaWxkID0gc2VsZWN0ZWRFbGVtLmNoaWxkTm9kZXNbYW5jaG9yT2Zmc2V0XTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGNoaWxkLm5vZGVUeXBlID09PSBOb2RlLlRFWFRfTk9ERSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGV4dCA9IGNoaWxkLnRleHRDb250ZW50O1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGFuY2hvck9mZnNldCA+IDAgJiYgY2hpbGQubm9kZVR5cGUgPT09IE5vZGUuRUxFTUVOVF9OT0RFKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZXh0ICs9IHNlbGVjdGVkRWxlbS5jaGlsZE5vZGVzW2FuY2hvck9mZnNldCAtIDFdLnRleHRDb250ZW50O1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgbGV0IHdvcmtpbmdOb2RlQ29udGVudCA9IHNlbGVjdGVkRWxlbS50ZXh0Q29udGVudDtcbiAgICAgICAgICAgICAgICAgICAgbGV0IHNlbGVjdFN0YXJ0T2Zmc2V0ID0gdGhpcy5nZXRXaW5kb3dTZWxlY3Rpb24oKS5nZXRSYW5nZUF0KDApLnN0YXJ0T2Zmc2V0O1xuXG4gICAgICAgICAgICAgICAgICAgIGlmICh3b3JraW5nTm9kZUNvbnRlbnQgJiYgc2VsZWN0U3RhcnRPZmZzZXQgPj0gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGV4dCA9IHdvcmtpbmdOb2RlQ29udGVudC5zdWJzdHJpbmcoMCwgc2VsZWN0U3RhcnRPZmZzZXQpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0ZXh0O1xuICAgIH1cblxuICAgIGdldExhc3RXb3JkSW5UZXh0KHRleHQpIHtcbiAgICAgICAgdGV4dCA9IHRleHQucmVwbGFjZSgvXFx1MDBBMC9nLCBcIiBcIik7IC8vIGh0dHBzOi8vc3RhY2tvdmVyZmxvdy5jb20vcXVlc3Rpb25zLzI5ODUwNDA3L2hvdy1kby1pLXJlcGxhY2UtdW5pY29kZS1jaGFyYWN0ZXItdTAwYTAtd2l0aC1hLXNwYWNlLWluLWphdmFzY3JpcHRcbiAgICAgICAgbGV0IHdvcmRzQXJyYXkgPSB0ZXh0LnNwbGl0KFwiIFwiKTtcbiAgICAgICAgbGV0IHdvcmxkc0NvdW50ID0gd29yZHNBcnJheS5sZW5ndGggLSAxO1xuICAgICAgICByZXR1cm4gd29yZHNBcnJheVt3b3JsZHNDb3VudF0udHJpbSgpO1xuICAgIH1cblxuICAgIGdldFRyaWdnZXJJbmZvKG1lbnVBbHJlYWR5QWN0aXZlLCBoYXNUcmFpbGluZ1NwYWNlLCByZXF1aXJlTGVhZGluZ1NwYWNlLCBhbGxvd1NwYWNlcywgaXNBdXRvY29tcGxldGUpIHtcbiAgICAgICAgbGV0IGN0eCA9IHRoaXMudHJpYnV0ZS5jdXJyZW50O1xuICAgICAgICBsZXQgc2VsZWN0ZWQsIHBhdGgsIG9mZnNldDtcblxuICAgICAgICBpZiAoIXRoaXMuaXNDb250ZW50RWRpdGFibGUoY3R4LmVsZW1lbnQpKSB7XG4gICAgICAgICAgICBzZWxlY3RlZCA9IHRoaXMudHJpYnV0ZS5jdXJyZW50LmVsZW1lbnQ7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBsZXQgc2VsZWN0aW9uSW5mbyA9IHRoaXMuZ2V0Q29udGVudEVkaXRhYmxlU2VsZWN0ZWRQYXRoKGN0eCk7XG5cbiAgICAgICAgICAgIGlmIChzZWxlY3Rpb25JbmZvKSB7XG4gICAgICAgICAgICAgICAgc2VsZWN0ZWQgPSBzZWxlY3Rpb25JbmZvLnNlbGVjdGVkO1xuICAgICAgICAgICAgICAgIHBhdGggPSBzZWxlY3Rpb25JbmZvLnBhdGg7XG4gICAgICAgICAgICAgICAgb2Zmc2V0ID0gc2VsZWN0aW9uSW5mby5vZmZzZXQ7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBsZXQgZWZmZWN0aXZlUmFuZ2UgPSB0aGlzLmdldFRleHRQcmVjZWRpbmdDdXJyZW50U2VsZWN0aW9uKCk7XG4gICAgICAgIGxldCBsYXN0V29yZE9mRWZmZWN0aXZlUmFuZ2UgPSB0aGlzLmdldExhc3RXb3JkSW5UZXh0KGVmZmVjdGl2ZVJhbmdlKTtcblxuICAgICAgICBpZiAoaXNBdXRvY29tcGxldGUpIHtcbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgbWVudGlvblBvc2l0aW9uOiBlZmZlY3RpdmVSYW5nZS5sZW5ndGggLSBsYXN0V29yZE9mRWZmZWN0aXZlUmFuZ2UubGVuZ3RoLFxuICAgICAgICAgICAgICAgIG1lbnRpb25UZXh0OiBsYXN0V29yZE9mRWZmZWN0aXZlUmFuZ2UsXG4gICAgICAgICAgICAgICAgbWVudGlvblNlbGVjdGVkRWxlbWVudDogc2VsZWN0ZWQsXG4gICAgICAgICAgICAgICAgbWVudGlvblNlbGVjdGVkUGF0aDogcGF0aCxcbiAgICAgICAgICAgICAgICBtZW50aW9uU2VsZWN0ZWRPZmZzZXQ6IG9mZnNldCxcbiAgICAgICAgICAgIH07XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoZWZmZWN0aXZlUmFuZ2UgIT09IHVuZGVmaW5lZCAmJiBlZmZlY3RpdmVSYW5nZSAhPT0gbnVsbCkge1xuICAgICAgICAgICAgbGV0IG1vc3RSZWNlbnRUcmlnZ2VyQ2hhclBvcyA9IC0xO1xuICAgICAgICAgICAgbGV0IHRyaWdnZXJDaGFyO1xuXG4gICAgICAgICAgICB0aGlzLnRyaWJ1dGUuY29sbGVjdGlvbi5mb3JFYWNoKGNvbmZpZyA9PiB7XG4gICAgICAgICAgICAgICAgbGV0IGMgPSBjb25maWcudHJpZ2dlcjtcbiAgICAgICAgICAgICAgICBsZXQgaWR4ID0gY29uZmlnLnJlcXVpcmVMZWFkaW5nU3BhY2VcbiAgICAgICAgICAgICAgICAgICAgPyB0aGlzLmxhc3RJbmRleFdpdGhMZWFkaW5nU3BhY2UoZWZmZWN0aXZlUmFuZ2UsIGMpXG4gICAgICAgICAgICAgICAgICAgIDogZWZmZWN0aXZlUmFuZ2UubGFzdEluZGV4T2YoYyk7XG5cbiAgICAgICAgICAgICAgICBpZiAoaWR4ID4gbW9zdFJlY2VudFRyaWdnZXJDaGFyUG9zKSB7XG4gICAgICAgICAgICAgICAgICAgIG1vc3RSZWNlbnRUcmlnZ2VyQ2hhclBvcyA9IGlkeDtcbiAgICAgICAgICAgICAgICAgICAgdHJpZ2dlckNoYXIgPSBjO1xuICAgICAgICAgICAgICAgICAgICByZXF1aXJlTGVhZGluZ1NwYWNlID0gY29uZmlnLnJlcXVpcmVMZWFkaW5nU3BhY2U7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIGlmIChcbiAgICAgICAgICAgICAgICBtb3N0UmVjZW50VHJpZ2dlckNoYXJQb3MgPj0gMCAmJlxuICAgICAgICAgICAgICAgIChtb3N0UmVjZW50VHJpZ2dlckNoYXJQb3MgPT09IDAgfHxcbiAgICAgICAgICAgICAgICAgICAgIXJlcXVpcmVMZWFkaW5nU3BhY2UgfHxcbiAgICAgICAgICAgICAgICAgICAgL1tcXHhBMFxcc10vZy50ZXN0KGVmZmVjdGl2ZVJhbmdlLnN1YnN0cmluZyhtb3N0UmVjZW50VHJpZ2dlckNoYXJQb3MgLSAxLCBtb3N0UmVjZW50VHJpZ2dlckNoYXJQb3MpKSlcbiAgICAgICAgICAgICkge1xuICAgICAgICAgICAgICAgIGxldCBjdXJyZW50VHJpZ2dlclNuaXBwZXQgPSBlZmZlY3RpdmVSYW5nZS5zdWJzdHJpbmcoXG4gICAgICAgICAgICAgICAgICAgIG1vc3RSZWNlbnRUcmlnZ2VyQ2hhclBvcyArIDEsXG4gICAgICAgICAgICAgICAgICAgIGVmZmVjdGl2ZVJhbmdlLmxlbmd0aCxcbiAgICAgICAgICAgICAgICApO1xuXG4gICAgICAgICAgICAgICAgdHJpZ2dlckNoYXIgPSBlZmZlY3RpdmVSYW5nZS5zdWJzdHJpbmcobW9zdFJlY2VudFRyaWdnZXJDaGFyUG9zLCBtb3N0UmVjZW50VHJpZ2dlckNoYXJQb3MgKyAxKTtcbiAgICAgICAgICAgICAgICBsZXQgZmlyc3RTbmlwcGV0Q2hhciA9IGN1cnJlbnRUcmlnZ2VyU25pcHBldC5zdWJzdHJpbmcoMCwgMSk7XG4gICAgICAgICAgICAgICAgbGV0IGxlYWRpbmdTcGFjZSA9XG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRUcmlnZ2VyU25pcHBldC5sZW5ndGggPiAwICYmIChmaXJzdFNuaXBwZXRDaGFyID09PSBcIiBcIiB8fCBmaXJzdFNuaXBwZXRDaGFyID09PSBcIlxceEEwXCIpO1xuICAgICAgICAgICAgICAgIGlmIChoYXNUcmFpbGluZ1NwYWNlKSB7XG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRUcmlnZ2VyU25pcHBldCA9IGN1cnJlbnRUcmlnZ2VyU25pcHBldC50cmltKCk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgbGV0IHJlZ2V4ID0gYWxsb3dTcGFjZXMgPyAvW15cXFMgXS9nIDogL1tcXHhBMFxcc10vZztcblxuICAgICAgICAgICAgICAgIHRoaXMudHJpYnV0ZS5oYXNUcmFpbGluZ1NwYWNlID0gcmVnZXgudGVzdChjdXJyZW50VHJpZ2dlclNuaXBwZXQpO1xuXG4gICAgICAgICAgICAgICAgaWYgKCFsZWFkaW5nU3BhY2UgJiYgKG1lbnVBbHJlYWR5QWN0aXZlIHx8ICFyZWdleC50ZXN0KGN1cnJlbnRUcmlnZ2VyU25pcHBldCkpKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBtZW50aW9uUG9zaXRpb246IG1vc3RSZWNlbnRUcmlnZ2VyQ2hhclBvcyxcbiAgICAgICAgICAgICAgICAgICAgICAgIG1lbnRpb25UZXh0OiBjdXJyZW50VHJpZ2dlclNuaXBwZXQsXG4gICAgICAgICAgICAgICAgICAgICAgICBtZW50aW9uU2VsZWN0ZWRFbGVtZW50OiBzZWxlY3RlZCxcbiAgICAgICAgICAgICAgICAgICAgICAgIG1lbnRpb25TZWxlY3RlZFBhdGg6IHBhdGgsXG4gICAgICAgICAgICAgICAgICAgICAgICBtZW50aW9uU2VsZWN0ZWRPZmZzZXQ6IG9mZnNldCxcbiAgICAgICAgICAgICAgICAgICAgICAgIG1lbnRpb25UcmlnZ2VyQ2hhcjogdHJpZ2dlckNoYXIsXG4gICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgbGFzdEluZGV4V2l0aExlYWRpbmdTcGFjZShzdHIsIGNoYXIpIHtcbiAgICAgICAgbGV0IHJldmVyc2VkU3RyID0gc3RyXG4gICAgICAgICAgICAuc3BsaXQoXCJcIilcbiAgICAgICAgICAgIC5yZXZlcnNlKClcbiAgICAgICAgICAgIC5qb2luKFwiXCIpO1xuICAgICAgICBsZXQgaW5kZXggPSAtMTtcblxuICAgICAgICBmb3IgKGxldCBjaWR4ID0gMCwgbGVuID0gc3RyLmxlbmd0aDsgY2lkeCA8IGxlbjsgY2lkeCsrKSB7XG4gICAgICAgICAgICBsZXQgZmlyc3RDaGFyID0gY2lkeCA9PT0gc3RyLmxlbmd0aCAtIDE7XG4gICAgICAgICAgICBsZXQgbGVhZGluZ1NwYWNlID0gL1xccy8udGVzdChyZXZlcnNlZFN0cltjaWR4ICsgMV0pO1xuICAgICAgICAgICAgbGV0IG1hdGNoID0gY2hhciA9PT0gcmV2ZXJzZWRTdHJbY2lkeF07XG5cbiAgICAgICAgICAgIGlmIChtYXRjaCAmJiAoZmlyc3RDaGFyIHx8IGxlYWRpbmdTcGFjZSkpIHtcbiAgICAgICAgICAgICAgICBpbmRleCA9IHN0ci5sZW5ndGggLSAxIC0gY2lkeDtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBpbmRleDtcbiAgICB9XG5cbiAgICBpc0NvbnRlbnRFZGl0YWJsZShlbGVtZW50KSB7XG4gICAgICAgIGlmICghZWxlbWVudCkge1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBlbGVtZW50Lm5vZGVOYW1lICE9PSBcIklOUFVUXCIgJiYgZWxlbWVudC5ub2RlTmFtZSAhPT0gXCJURVhUQVJFQVwiO1xuICAgIH1cblxuICAgIGlzTWVudU9mZlNjcmVlbihjb29yZGluYXRlcywgbWVudURpbWVuc2lvbnMpIHtcbiAgICAgICAgbGV0IHdpbmRvd1dpZHRoID0gd2luZG93LmlubmVyV2lkdGg7XG4gICAgICAgIGxldCB3aW5kb3dIZWlnaHQgPSB3aW5kb3cuaW5uZXJIZWlnaHQ7XG4gICAgICAgIGxldCBkb2MgPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQ7XG4gICAgICAgIGxldCB3aW5kb3dMZWZ0ID0gKHdpbmRvdy5wYWdlWE9mZnNldCB8fCBkb2Muc2Nyb2xsTGVmdCkgLSAoZG9jLmNsaWVudExlZnQgfHwgMCk7XG4gICAgICAgIGxldCB3aW5kb3dUb3AgPSAod2luZG93LnBhZ2VZT2Zmc2V0IHx8IGRvYy5zY3JvbGxUb3ApIC0gKGRvYy5jbGllbnRUb3AgfHwgMCk7XG5cbiAgICAgICAgbGV0IG1lbnVUb3AgPVxuICAgICAgICAgICAgdHlwZW9mIGNvb3JkaW5hdGVzLnRvcCA9PT0gXCJudW1iZXJcIlxuICAgICAgICAgICAgICAgID8gY29vcmRpbmF0ZXMudG9wXG4gICAgICAgICAgICAgICAgOiB3aW5kb3dUb3AgKyB3aW5kb3dIZWlnaHQgLSBjb29yZGluYXRlcy5ib3R0b20gLSBtZW51RGltZW5zaW9ucy5oZWlnaHQ7XG4gICAgICAgIGxldCBtZW51UmlnaHQgPVxuICAgICAgICAgICAgdHlwZW9mIGNvb3JkaW5hdGVzLnJpZ2h0ID09PSBcIm51bWJlclwiID8gY29vcmRpbmF0ZXMucmlnaHQgOiBjb29yZGluYXRlcy5sZWZ0ICsgbWVudURpbWVuc2lvbnMud2lkdGg7XG4gICAgICAgIGxldCBtZW51Qm90dG9tID1cbiAgICAgICAgICAgIHR5cGVvZiBjb29yZGluYXRlcy5ib3R0b20gPT09IFwibnVtYmVyXCIgPyBjb29yZGluYXRlcy5ib3R0b20gOiBjb29yZGluYXRlcy50b3AgKyBtZW51RGltZW5zaW9ucy5oZWlnaHQ7XG4gICAgICAgIGxldCBtZW51TGVmdCA9XG4gICAgICAgICAgICB0eXBlb2YgY29vcmRpbmF0ZXMubGVmdCA9PT0gXCJudW1iZXJcIlxuICAgICAgICAgICAgICAgID8gY29vcmRpbmF0ZXMubGVmdFxuICAgICAgICAgICAgICAgIDogd2luZG93TGVmdCArIHdpbmRvd1dpZHRoIC0gY29vcmRpbmF0ZXMucmlnaHQgLSBtZW51RGltZW5zaW9ucy53aWR0aDtcblxuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgdG9wOiBtZW51VG9wIDwgTWF0aC5mbG9vcih3aW5kb3dUb3ApLFxuICAgICAgICAgICAgcmlnaHQ6IG1lbnVSaWdodCA+IE1hdGguY2VpbCh3aW5kb3dMZWZ0ICsgd2luZG93V2lkdGgpLFxuICAgICAgICAgICAgYm90dG9tOiBtZW51Qm90dG9tID4gTWF0aC5jZWlsKHdpbmRvd1RvcCArIHdpbmRvd0hlaWdodCksXG4gICAgICAgICAgICBsZWZ0OiBtZW51TGVmdCA8IE1hdGguZmxvb3Iod2luZG93TGVmdCksXG4gICAgICAgIH07XG4gICAgfVxuXG4gICAgZ2V0TWVudURpbWVuc2lvbnMoKSB7XG4gICAgICAgIC8vIFdpZHRoIG9mIHRoZSBtZW51IGRlcGVuZHMgb2YgaXRzIGNvbnRlbnRzIGFuZCBwb3NpdGlvblxuICAgICAgICAvLyBXZSBtdXN0IGNoZWNrIHdoYXQgaXRzIHdpZHRoIHdvdWxkIGJlIHdpdGhvdXQgYW55IG9ic3RydWN0aW9uXG4gICAgICAgIC8vIFRoaXMgd2F5LCB3ZSBjYW4gYWNoaWV2ZSBnb29kIHBvc2l0aW9uaW5nIGZvciBmbGlwcGluZyB0aGUgbWVudVxuICAgICAgICBsZXQgZGltZW5zaW9ucyA9IHtcbiAgICAgICAgICAgIHdpZHRoOiBudWxsLFxuICAgICAgICAgICAgaGVpZ2h0OiBudWxsLFxuICAgICAgICB9O1xuXG4gICAgICAgIHRoaXMudHJpYnV0ZS5tZW51LnN0eWxlLmNzc1RleHQgPSBgdG9wOiAwcHg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZWZ0OiAwcHg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB6SW5kZXg6IDEwMDAwO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2aXNpYmlsaXR5OyBoaWRkZW47YDtcbiAgICAgICAgZGltZW5zaW9ucy53aWR0aCA9IHRoaXMudHJpYnV0ZS5tZW51Lm9mZnNldFdpZHRoO1xuICAgICAgICBkaW1lbnNpb25zLmhlaWdodCA9IHRoaXMudHJpYnV0ZS5tZW51Lm9mZnNldEhlaWdodDtcblxuICAgICAgICB0aGlzLnRyaWJ1dGUubWVudS5zdHlsZS5jc3NUZXh0ID0gYGRpc3BsYXk6IG5vbmU7YDtcblxuICAgICAgICByZXR1cm4gZGltZW5zaW9ucztcbiAgICB9XG5cbiAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmVcbiAgICBnZXRUZXh0QXJlYU9ySW5wdXRVbmRlcmxpbmVQb3NpdGlvbihlbGVtZW50LCBwb3NpdGlvbiwgZmxpcHBlZCkge1xuICAgICAgICBsZXQgcHJvcGVydGllcyA9IFtcbiAgICAgICAgICAgIFwiZGlyZWN0aW9uXCIsXG4gICAgICAgICAgICBcImJveFNpemluZ1wiLFxuICAgICAgICAgICAgXCJ3aWR0aFwiLFxuICAgICAgICAgICAgXCJoZWlnaHRcIixcbiAgICAgICAgICAgIFwib3ZlcmZsb3dYXCIsXG4gICAgICAgICAgICBcIm92ZXJmbG93WVwiLFxuICAgICAgICAgICAgXCJib3JkZXJUb3BXaWR0aFwiLFxuICAgICAgICAgICAgXCJib3JkZXJSaWdodFdpZHRoXCIsXG4gICAgICAgICAgICBcImJvcmRlckJvdHRvbVdpZHRoXCIsXG4gICAgICAgICAgICBcImJvcmRlckxlZnRXaWR0aFwiLFxuICAgICAgICAgICAgXCJwYWRkaW5nVG9wXCIsXG4gICAgICAgICAgICBcInBhZGRpbmdSaWdodFwiLFxuICAgICAgICAgICAgXCJwYWRkaW5nQm90dG9tXCIsXG4gICAgICAgICAgICBcInBhZGRpbmdMZWZ0XCIsXG4gICAgICAgICAgICBcImZvbnRTdHlsZVwiLFxuICAgICAgICAgICAgXCJmb250VmFyaWFudFwiLFxuICAgICAgICAgICAgXCJmb250V2VpZ2h0XCIsXG4gICAgICAgICAgICBcImZvbnRTdHJldGNoXCIsXG4gICAgICAgICAgICBcImZvbnRTaXplXCIsXG4gICAgICAgICAgICBcImZvbnRTaXplQWRqdXN0XCIsXG4gICAgICAgICAgICBcImxpbmVIZWlnaHRcIixcbiAgICAgICAgICAgIFwiZm9udEZhbWlseVwiLFxuICAgICAgICAgICAgXCJ0ZXh0QWxpZ25cIixcbiAgICAgICAgICAgIFwidGV4dFRyYW5zZm9ybVwiLFxuICAgICAgICAgICAgXCJ0ZXh0SW5kZW50XCIsXG4gICAgICAgICAgICBcInRleHREZWNvcmF0aW9uXCIsXG4gICAgICAgICAgICBcImxldHRlclNwYWNpbmdcIixcbiAgICAgICAgICAgIFwid29yZFNwYWNpbmdcIixcbiAgICAgICAgXTtcblxuICAgICAgICBsZXQgaXNGaXJlZm94ID0gd2luZG93Lm1veklubmVyU2NyZWVuWCAhPT0gbnVsbDtcblxuICAgICAgICBsZXQgZGl2ID0gdGhpcy5nZXREb2N1bWVudCgpLmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7XG4gICAgICAgIGRpdi5pZCA9IFwiaW5wdXQtdGV4dGFyZWEtY2FyZXQtcG9zaXRpb24tbWlycm9yLWRpdlwiO1xuICAgICAgICB0aGlzLmdldERvY3VtZW50KCkuYm9keS5hcHBlbmRDaGlsZChkaXYpO1xuXG4gICAgICAgIGxldCBzdHlsZSA9IGRpdi5zdHlsZTtcbiAgICAgICAgbGV0IGNvbXB1dGVkID0gd2luZG93LmdldENvbXB1dGVkU3R5bGUgPyBnZXRDb21wdXRlZFN0eWxlKGVsZW1lbnQpIDogZWxlbWVudC5jdXJyZW50U3R5bGU7XG5cbiAgICAgICAgc3R5bGUud2hpdGVTcGFjZSA9IFwicHJlLXdyYXBcIjtcbiAgICAgICAgaWYgKGVsZW1lbnQubm9kZU5hbWUgIT09IFwiSU5QVVRcIikge1xuICAgICAgICAgICAgc3R5bGUud29yZFdyYXAgPSBcImJyZWFrLXdvcmRcIjtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIHBvc2l0aW9uIG9mZi1zY3JlZW5cbiAgICAgICAgc3R5bGUucG9zaXRpb24gPSBcImFic29sdXRlXCI7XG4gICAgICAgIHN0eWxlLnZpc2liaWxpdHkgPSBcImhpZGRlblwiO1xuXG4gICAgICAgIC8vIHRyYW5zZmVyIHRoZSBlbGVtZW50J3MgcHJvcGVydGllcyB0byB0aGUgZGl2XG4gICAgICAgIHByb3BlcnRpZXMuZm9yRWFjaChwcm9wID0+IHtcbiAgICAgICAgICAgIHN0eWxlW3Byb3BdID0gY29tcHV0ZWRbcHJvcF07XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGlmIChpc0ZpcmVmb3gpIHtcbiAgICAgICAgICAgIHN0eWxlLndpZHRoID0gYCR7cGFyc2VJbnQoY29tcHV0ZWQud2lkdGgpIC0gMn1weGA7XG4gICAgICAgICAgICBpZiAoZWxlbWVudC5zY3JvbGxIZWlnaHQgPiBwYXJzZUludChjb21wdXRlZC5oZWlnaHQpKSBzdHlsZS5vdmVyZmxvd1kgPSBcInNjcm9sbFwiO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgc3R5bGUub3ZlcmZsb3cgPSBcImhpZGRlblwiO1xuICAgICAgICB9XG5cbiAgICAgICAgZGl2LnRleHRDb250ZW50ID0gZWxlbWVudC52YWx1ZS5zdWJzdHJpbmcoMCwgcG9zaXRpb24pO1xuXG4gICAgICAgIGlmIChlbGVtZW50Lm5vZGVOYW1lID09PSBcIklOUFVUXCIpIHtcbiAgICAgICAgICAgIGRpdi50ZXh0Q29udGVudCA9IGRpdi50ZXh0Q29udGVudC5yZXBsYWNlKC9cXHMvZywgXCLCoFwiKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGxldCBzcGFuID0gdGhpcy5nZXREb2N1bWVudCgpLmNyZWF0ZUVsZW1lbnQoXCJzcGFuXCIpO1xuICAgICAgICBzcGFuLnRleHRDb250ZW50ID0gZWxlbWVudC52YWx1ZS5zdWJzdHJpbmcocG9zaXRpb24pIHx8IFwiLlwiO1xuICAgICAgICBkaXYuYXBwZW5kQ2hpbGQoc3Bhbik7XG5cbiAgICAgICAgbGV0IHJlY3QgPSBlbGVtZW50LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xuICAgICAgICBsZXQgZG9jID0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50O1xuICAgICAgICBsZXQgd2luZG93TGVmdCA9ICh3aW5kb3cucGFnZVhPZmZzZXQgfHwgZG9jLnNjcm9sbExlZnQpIC0gKGRvYy5jbGllbnRMZWZ0IHx8IDApO1xuICAgICAgICBsZXQgd2luZG93VG9wID0gKHdpbmRvdy5wYWdlWU9mZnNldCB8fCBkb2Muc2Nyb2xsVG9wKSAtIChkb2MuY2xpZW50VG9wIHx8IDApO1xuXG4gICAgICAgIGxldCBjb29yZGluYXRlcyA9IHtcbiAgICAgICAgICAgIHRvcDpcbiAgICAgICAgICAgICAgICByZWN0LnRvcCArXG4gICAgICAgICAgICAgICAgd2luZG93VG9wICtcbiAgICAgICAgICAgICAgICBzcGFuLm9mZnNldFRvcCArXG4gICAgICAgICAgICAgICAgcGFyc2VJbnQoY29tcHV0ZWQuYm9yZGVyVG9wV2lkdGgpICtcbiAgICAgICAgICAgICAgICBwYXJzZUludChjb21wdXRlZC5mb250U2l6ZSkgLVxuICAgICAgICAgICAgICAgIGVsZW1lbnQuc2Nyb2xsVG9wLFxuICAgICAgICAgICAgbGVmdDogcmVjdC5sZWZ0ICsgd2luZG93TGVmdCArIHNwYW4ub2Zmc2V0TGVmdCArIHBhcnNlSW50KGNvbXB1dGVkLmJvcmRlckxlZnRXaWR0aCksXG4gICAgICAgIH07XG5cbiAgICAgICAgbGV0IHdpbmRvd1dpZHRoID0gd2luZG93LmlubmVyV2lkdGg7XG4gICAgICAgIGxldCB3aW5kb3dIZWlnaHQgPSB3aW5kb3cuaW5uZXJIZWlnaHQ7XG5cbiAgICAgICAgbGV0IG1lbnVEaW1lbnNpb25zID0gdGhpcy5nZXRNZW51RGltZW5zaW9ucygpO1xuICAgICAgICBsZXQgbWVudUlzT2ZmU2NyZWVuID0gdGhpcy5pc01lbnVPZmZTY3JlZW4oY29vcmRpbmF0ZXMsIG1lbnVEaW1lbnNpb25zKTtcblxuICAgICAgICBpZiAobWVudUlzT2ZmU2NyZWVuLnJpZ2h0KSB7XG4gICAgICAgICAgICBjb29yZGluYXRlcy5yaWdodCA9IHdpbmRvd1dpZHRoIC0gY29vcmRpbmF0ZXMubGVmdDtcbiAgICAgICAgICAgIGNvb3JkaW5hdGVzLmxlZnQgPSBcImF1dG9cIjtcbiAgICAgICAgfVxuXG4gICAgICAgIGxldCBwYXJlbnRIZWlnaHQgPSB0aGlzLnRyaWJ1dGUubWVudUNvbnRhaW5lclxuICAgICAgICAgICAgPyB0aGlzLnRyaWJ1dGUubWVudUNvbnRhaW5lci5vZmZzZXRIZWlnaHRcbiAgICAgICAgICAgIDogdGhpcy5nZXREb2N1bWVudCgpLmJvZHkub2Zmc2V0SGVpZ2h0O1xuXG4gICAgICAgIGlmIChtZW51SXNPZmZTY3JlZW4uYm90dG9tKSB7XG4gICAgICAgICAgICBsZXQgcGFyZW50UmVjdCA9IHRoaXMudHJpYnV0ZS5tZW51Q29udGFpbmVyXG4gICAgICAgICAgICAgICAgPyB0aGlzLnRyaWJ1dGUubWVudUNvbnRhaW5lci5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKVxuICAgICAgICAgICAgICAgIDogdGhpcy5nZXREb2N1bWVudCgpLmJvZHkuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG4gICAgICAgICAgICBsZXQgc2Nyb2xsU3RpbGxBdmFpbGFibGUgPSBwYXJlbnRIZWlnaHQgLSAod2luZG93SGVpZ2h0IC0gcGFyZW50UmVjdC50b3ApO1xuXG4gICAgICAgICAgICBjb29yZGluYXRlcy5ib3R0b20gPSBzY3JvbGxTdGlsbEF2YWlsYWJsZSArICh3aW5kb3dIZWlnaHQgLSByZWN0LnRvcCAtIHNwYW4ub2Zmc2V0VG9wKTtcbiAgICAgICAgICAgIGNvb3JkaW5hdGVzLnRvcCA9IFwiYXV0b1wiO1xuICAgICAgICB9XG5cbiAgICAgICAgbWVudUlzT2ZmU2NyZWVuID0gdGhpcy5pc01lbnVPZmZTY3JlZW4oY29vcmRpbmF0ZXMsIG1lbnVEaW1lbnNpb25zKTtcbiAgICAgICAgaWYgKG1lbnVJc09mZlNjcmVlbi5sZWZ0KSB7XG4gICAgICAgICAgICBjb29yZGluYXRlcy5sZWZ0ID1cbiAgICAgICAgICAgICAgICB3aW5kb3dXaWR0aCA+IG1lbnVEaW1lbnNpb25zLndpZHRoID8gd2luZG93TGVmdCArIHdpbmRvd1dpZHRoIC0gbWVudURpbWVuc2lvbnMud2lkdGggOiB3aW5kb3dMZWZ0O1xuICAgICAgICAgICAgZGVsZXRlIGNvb3JkaW5hdGVzLnJpZ2h0O1xuICAgICAgICB9XG4gICAgICAgIGlmIChtZW51SXNPZmZTY3JlZW4udG9wKSB7XG4gICAgICAgICAgICBjb29yZGluYXRlcy50b3AgPVxuICAgICAgICAgICAgICAgIHdpbmRvd0hlaWdodCA+IG1lbnVEaW1lbnNpb25zLmhlaWdodCA/IHdpbmRvd1RvcCArIHdpbmRvd0hlaWdodCAtIG1lbnVEaW1lbnNpb25zLmhlaWdodCA6IHdpbmRvd1RvcDtcbiAgICAgICAgICAgIGRlbGV0ZSBjb29yZGluYXRlcy5ib3R0b207XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmdldERvY3VtZW50KCkuYm9keS5yZW1vdmVDaGlsZChkaXYpO1xuICAgICAgICByZXR1cm4gY29vcmRpbmF0ZXM7XG4gICAgfVxuXG4gICAgZ2V0Q29udGVudEVkaXRhYmxlQ2FyZXRQb3NpdGlvbihzZWxlY3RlZE5vZGVQb3NpdGlvbikge1xuICAgICAgICBsZXQgbWFya2VyVGV4dENoYXIgPSBcIu+7v1wiO1xuICAgICAgICBsZXQgbWFya2VyRWwsXG4gICAgICAgICAgICBtYXJrZXJJZCA9IGBzZWxfJHtuZXcgRGF0ZSgpLmdldFRpbWUoKX1fJHtNYXRoLnJhbmRvbSgpXG4gICAgICAgICAgICAgICAgLnRvU3RyaW5nKClcbiAgICAgICAgICAgICAgICAuc3Vic3RyKDIpfWA7XG4gICAgICAgIGxldCByYW5nZTtcbiAgICAgICAgbGV0IHNlbCA9IHRoaXMuZ2V0V2luZG93U2VsZWN0aW9uKCk7XG4gICAgICAgIGxldCBwcmV2UmFuZ2UgPSBzZWwuZ2V0UmFuZ2VBdCgwKTtcblxuICAgICAgICByYW5nZSA9IHRoaXMuZ2V0RG9jdW1lbnQoKS5jcmVhdGVSYW5nZSgpO1xuICAgICAgICByYW5nZS5zZXRTdGFydChzZWwuYW5jaG9yTm9kZSwgc2VsZWN0ZWROb2RlUG9zaXRpb24pO1xuICAgICAgICByYW5nZS5zZXRFbmQoc2VsLmFuY2hvck5vZGUsIHNlbGVjdGVkTm9kZVBvc2l0aW9uKTtcblxuICAgICAgICByYW5nZS5jb2xsYXBzZShmYWxzZSk7XG5cbiAgICAgICAgLy8gQ3JlYXRlIHRoZSBtYXJrZXIgZWxlbWVudCBjb250YWluaW5nIGEgc2luZ2xlIGludmlzaWJsZSBjaGFyYWN0ZXIgdXNpbmcgRE9NIG1ldGhvZHMgYW5kIGluc2VydCBpdFxuICAgICAgICBtYXJrZXJFbCA9IHRoaXMuZ2V0RG9jdW1lbnQoKS5jcmVhdGVFbGVtZW50KFwic3BhblwiKTtcbiAgICAgICAgbWFya2VyRWwuaWQgPSBtYXJrZXJJZDtcblxuICAgICAgICBtYXJrZXJFbC5hcHBlbmRDaGlsZCh0aGlzLmdldERvY3VtZW50KCkuY3JlYXRlVGV4dE5vZGUobWFya2VyVGV4dENoYXIpKTtcbiAgICAgICAgcmFuZ2UuaW5zZXJ0Tm9kZShtYXJrZXJFbCk7XG4gICAgICAgIHNlbC5yZW1vdmVBbGxSYW5nZXMoKTtcbiAgICAgICAgc2VsLmFkZFJhbmdlKHByZXZSYW5nZSk7XG5cbiAgICAgICAgbGV0IHJlY3QgPSBtYXJrZXJFbC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcbiAgICAgICAgbGV0IGRvYyA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudDtcbiAgICAgICAgbGV0IHdpbmRvd0xlZnQgPSAod2luZG93LnBhZ2VYT2Zmc2V0IHx8IGRvYy5zY3JvbGxMZWZ0KSAtIChkb2MuY2xpZW50TGVmdCB8fCAwKTtcbiAgICAgICAgbGV0IHdpbmRvd1RvcCA9ICh3aW5kb3cucGFnZVlPZmZzZXQgfHwgZG9jLnNjcm9sbFRvcCkgLSAoZG9jLmNsaWVudFRvcCB8fCAwKTtcbiAgICAgICAgbGV0IGNvb3JkaW5hdGVzID0ge1xuICAgICAgICAgICAgbGVmdDogcmVjdC5sZWZ0ICsgd2luZG93TGVmdCxcbiAgICAgICAgICAgIHRvcDogcmVjdC50b3AgKyBtYXJrZXJFbC5vZmZzZXRIZWlnaHQgKyB3aW5kb3dUb3AsXG4gICAgICAgIH07XG4gICAgICAgIGxldCB3aW5kb3dXaWR0aCA9IHdpbmRvdy5pbm5lcldpZHRoO1xuICAgICAgICBsZXQgd2luZG93SGVpZ2h0ID0gd2luZG93LmlubmVySGVpZ2h0O1xuXG4gICAgICAgIGxldCBtZW51RGltZW5zaW9ucyA9IHRoaXMuZ2V0TWVudURpbWVuc2lvbnMoKTtcbiAgICAgICAgbGV0IG1lbnVJc09mZlNjcmVlbiA9IHRoaXMuaXNNZW51T2ZmU2NyZWVuKGNvb3JkaW5hdGVzLCBtZW51RGltZW5zaW9ucyk7XG5cbiAgICAgICAgaWYgKG1lbnVJc09mZlNjcmVlbi5yaWdodCkge1xuICAgICAgICAgICAgY29vcmRpbmF0ZXMubGVmdCA9IFwiYXV0b1wiO1xuICAgICAgICAgICAgY29vcmRpbmF0ZXMucmlnaHQgPSB3aW5kb3dXaWR0aCAtIHJlY3QubGVmdCAtIHdpbmRvd0xlZnQ7XG4gICAgICAgIH1cblxuICAgICAgICBsZXQgcGFyZW50SGVpZ2h0ID0gdGhpcy50cmlidXRlLm1lbnVDb250YWluZXJcbiAgICAgICAgICAgID8gdGhpcy50cmlidXRlLm1lbnVDb250YWluZXIub2Zmc2V0SGVpZ2h0XG4gICAgICAgICAgICA6IHRoaXMuZ2V0RG9jdW1lbnQoKS5ib2R5Lm9mZnNldEhlaWdodDtcblxuICAgICAgICBpZiAobWVudUlzT2ZmU2NyZWVuLmJvdHRvbSkge1xuICAgICAgICAgICAgbGV0IHBhcmVudFJlY3QgPSB0aGlzLnRyaWJ1dGUubWVudUNvbnRhaW5lclxuICAgICAgICAgICAgICAgID8gdGhpcy50cmlidXRlLm1lbnVDb250YWluZXIuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KClcbiAgICAgICAgICAgICAgICA6IHRoaXMuZ2V0RG9jdW1lbnQoKS5ib2R5LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xuICAgICAgICAgICAgbGV0IHNjcm9sbFN0aWxsQXZhaWxhYmxlID0gcGFyZW50SGVpZ2h0IC0gKHdpbmRvd0hlaWdodCAtIHBhcmVudFJlY3QudG9wKTtcblxuICAgICAgICAgICAgY29vcmRpbmF0ZXMudG9wID0gXCJhdXRvXCI7XG4gICAgICAgICAgICBjb29yZGluYXRlcy5ib3R0b20gPSBzY3JvbGxTdGlsbEF2YWlsYWJsZSArICh3aW5kb3dIZWlnaHQgLSByZWN0LnRvcCk7XG4gICAgICAgIH1cblxuICAgICAgICBtZW51SXNPZmZTY3JlZW4gPSB0aGlzLmlzTWVudU9mZlNjcmVlbihjb29yZGluYXRlcywgbWVudURpbWVuc2lvbnMpO1xuICAgICAgICBpZiAobWVudUlzT2ZmU2NyZWVuLmxlZnQpIHtcbiAgICAgICAgICAgIGNvb3JkaW5hdGVzLmxlZnQgPVxuICAgICAgICAgICAgICAgIHdpbmRvd1dpZHRoID4gbWVudURpbWVuc2lvbnMud2lkdGggPyB3aW5kb3dMZWZ0ICsgd2luZG93V2lkdGggLSBtZW51RGltZW5zaW9ucy53aWR0aCA6IHdpbmRvd0xlZnQ7XG4gICAgICAgICAgICBkZWxldGUgY29vcmRpbmF0ZXMucmlnaHQ7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKG1lbnVJc09mZlNjcmVlbi50b3ApIHtcbiAgICAgICAgICAgIGNvb3JkaW5hdGVzLnRvcCA9XG4gICAgICAgICAgICAgICAgd2luZG93SGVpZ2h0ID4gbWVudURpbWVuc2lvbnMuaGVpZ2h0ID8gd2luZG93VG9wICsgd2luZG93SGVpZ2h0IC0gbWVudURpbWVuc2lvbnMuaGVpZ2h0IDogd2luZG93VG9wO1xuICAgICAgICAgICAgZGVsZXRlIGNvb3JkaW5hdGVzLmJvdHRvbTtcbiAgICAgICAgfVxuXG4gICAgICAgIG1hcmtlckVsLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQobWFya2VyRWwpO1xuICAgICAgICByZXR1cm4gY29vcmRpbmF0ZXM7XG4gICAgfVxuXG4gICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lXG4gICAgc2Nyb2xsSW50b1ZpZXcoZWxlbSkge1xuICAgICAgICBsZXQgcmVhc29uYWJsZUJ1ZmZlciA9IDIwLFxuICAgICAgICAgICAgY2xpZW50UmVjdDtcbiAgICAgICAgbGV0IG1heFNjcm9sbERpc3BsYWNlbWVudCA9IDEwMDtcbiAgICAgICAgbGV0IGUgPSB0aGlzLm1lbnU7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBlID09PSBcInVuZGVmaW5lZFwiKSByZXR1cm47XG5cbiAgICAgICAgd2hpbGUgKGNsaWVudFJlY3QgPT09IHVuZGVmaW5lZCB8fCBjbGllbnRSZWN0LmhlaWdodCA9PT0gMCkge1xuICAgICAgICAgICAgY2xpZW50UmVjdCA9IGUuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG5cbiAgICAgICAgICAgIGlmIChjbGllbnRSZWN0LmhlaWdodCA9PT0gMCkge1xuICAgICAgICAgICAgICAgIGUgPSBlLmNoaWxkTm9kZXNbMF07XG4gICAgICAgICAgICAgICAgaWYgKGUgPT09IHVuZGVmaW5lZCB8fCAhZS5nZXRCb3VuZGluZ0NsaWVudFJlY3QpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGxldCBlbGVtVG9wID0gY2xpZW50UmVjdC50b3A7XG4gICAgICAgIGxldCBlbGVtQm90dG9tID0gZWxlbVRvcCArIGNsaWVudFJlY3QuaGVpZ2h0O1xuXG4gICAgICAgIGlmIChlbGVtVG9wIDwgMCkge1xuICAgICAgICAgICAgd2luZG93LnNjcm9sbFRvKDAsIHdpbmRvdy5wYWdlWU9mZnNldCArIGNsaWVudFJlY3QudG9wIC0gcmVhc29uYWJsZUJ1ZmZlcik7XG4gICAgICAgIH0gZWxzZSBpZiAoZWxlbUJvdHRvbSA+IHdpbmRvdy5pbm5lckhlaWdodCkge1xuICAgICAgICAgICAgbGV0IG1heFkgPSB3aW5kb3cucGFnZVlPZmZzZXQgKyBjbGllbnRSZWN0LnRvcCAtIHJlYXNvbmFibGVCdWZmZXI7XG5cbiAgICAgICAgICAgIGlmIChtYXhZIC0gd2luZG93LnBhZ2VZT2Zmc2V0ID4gbWF4U2Nyb2xsRGlzcGxhY2VtZW50KSB7XG4gICAgICAgICAgICAgICAgbWF4WSA9IHdpbmRvdy5wYWdlWU9mZnNldCArIG1heFNjcm9sbERpc3BsYWNlbWVudDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgbGV0IHRhcmdldFkgPSB3aW5kb3cucGFnZVlPZmZzZXQgLSAod2luZG93LmlubmVySGVpZ2h0IC0gZWxlbUJvdHRvbSk7XG5cbiAgICAgICAgICAgIGlmICh0YXJnZXRZID4gbWF4WSkge1xuICAgICAgICAgICAgICAgIHRhcmdldFkgPSBtYXhZO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB3aW5kb3cuc2Nyb2xsVG8oMCwgdGFyZ2V0WSk7XG4gICAgICAgIH1cbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IFRyaWJ1dGVSYW5nZTtcbiIsIi8vIFRoYW5rcyB0byBodHRwczovL2dpdGh1Yi5jb20vbWF0dHlvcmsvZnV6enlcbmNsYXNzIFRyaWJ1dGVTZWFyY2gge1xuICAgIGNvbnN0cnVjdG9yKHRyaWJ1dGUpIHtcbiAgICAgICAgdGhpcy50cmlidXRlID0gdHJpYnV0ZTtcbiAgICAgICAgdGhpcy50cmlidXRlLnNlYXJjaCA9IHRoaXM7XG4gICAgfVxuXG4gICAgc2ltcGxlRmlsdGVyKHBhdHRlcm4sIGFycmF5KSB7XG4gICAgICAgIHJldHVybiBhcnJheS5maWx0ZXIoc3RyaW5nID0+IHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnRlc3QocGF0dGVybiwgc3RyaW5nKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgdGVzdChwYXR0ZXJuLCBzdHJpbmcpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubWF0Y2gocGF0dGVybiwgc3RyaW5nKSAhPT0gbnVsbDtcbiAgICB9XG5cbiAgICBtYXRjaChwYXR0ZXJuLCBzdHJpbmcsIG9wdHMpIHtcbiAgICAgICAgb3B0cyA9IG9wdHMgfHwge307XG4gICAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZVxuICAgICAgICBsZXQgcGF0dGVybklkeCA9IDAsXG4gICAgICAgICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmVcbiAgICAgICAgICAgIHJlc3VsdCA9IFtdLFxuICAgICAgICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lXG4gICAgICAgICAgICBsZW4gPSBzdHJpbmcubGVuZ3RoLFxuICAgICAgICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lXG4gICAgICAgICAgICB0b3RhbFNjb3JlID0gMCxcbiAgICAgICAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZVxuICAgICAgICAgICAgY3VyclNjb3JlID0gMCxcbiAgICAgICAgICAgIHByZSA9IG9wdHMucHJlIHx8IFwiXCIsXG4gICAgICAgICAgICBwb3N0ID0gb3B0cy5wb3N0IHx8IFwiXCIsXG4gICAgICAgICAgICBjb21wYXJlU3RyaW5nID0gKG9wdHMuY2FzZVNlbnNpdGl2ZSAmJiBzdHJpbmcpIHx8IHN0cmluZy50b0xvd2VyQ2FzZSgpLFxuICAgICAgICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lXG4gICAgICAgICAgICBjaCxcbiAgICAgICAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZVxuICAgICAgICAgICAgY29tcGFyZUNoYXI7XG5cbiAgICAgICAgcGF0dGVybiA9IChvcHRzLmNhc2VTZW5zaXRpdmUgJiYgcGF0dGVybikgfHwgcGF0dGVybi50b0xvd2VyQ2FzZSgpO1xuXG4gICAgICAgIGxldCBwYXR0ZXJuQ2FjaGUgPSB0aGlzLnRyYXZlcnNlKGNvbXBhcmVTdHJpbmcsIHBhdHRlcm4sIDAsIDAsIFtdKTtcbiAgICAgICAgaWYgKCFwYXR0ZXJuQ2FjaGUpIHtcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIHJlbmRlcmVkOiB0aGlzLnJlbmRlcihzdHJpbmcsIHBhdHRlcm5DYWNoZS5jYWNoZSwgcHJlLCBwb3N0KSxcbiAgICAgICAgICAgIHNjb3JlOiBwYXR0ZXJuQ2FjaGUuc2NvcmUsXG4gICAgICAgIH07XG4gICAgfVxuXG4gICAgdHJhdmVyc2Uoc3RyaW5nLCBwYXR0ZXJuLCBzdHJpbmdJbmRleCwgcGF0dGVybkluZGV4LCBwYXR0ZXJuQ2FjaGUpIHtcbiAgICAgICAgLy8gaWYgdGhlIHBhdHRlcm4gc2VhcmNoIGF0IGVuZFxuICAgICAgICBpZiAocGF0dGVybi5sZW5ndGggPT09IHBhdHRlcm5JbmRleCkge1xuICAgICAgICAgICAgLy8gY2FsY3VsYXRlIHNjb3JlIGFuZCBjb3B5IHRoZSBjYWNoZSBjb250YWluaW5nIHRoZSBpbmRpY2VzIHdoZXJlIGl0J3MgZm91bmRcbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgc2NvcmU6IHRoaXMuY2FsY3VsYXRlU2NvcmUocGF0dGVybkNhY2hlKSxcbiAgICAgICAgICAgICAgICBjYWNoZTogcGF0dGVybkNhY2hlLnNsaWNlKCksXG4gICAgICAgICAgICB9O1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gaWYgc3RyaW5nIGF0IGVuZCBvciByZW1haW5pbmcgcGF0dGVybiA+IHJlbWFpbmluZyBzdHJpbmdcbiAgICAgICAgaWYgKHN0cmluZy5sZW5ndGggPT09IHN0cmluZ0luZGV4IHx8IHBhdHRlcm4ubGVuZ3RoIC0gcGF0dGVybkluZGV4ID4gc3RyaW5nLmxlbmd0aCAtIHN0cmluZ0luZGV4KSB7XG4gICAgICAgICAgICByZXR1cm4gdW5kZWZpbmVkO1xuICAgICAgICB9XG5cbiAgICAgICAgbGV0IGMgPSBwYXR0ZXJuW3BhdHRlcm5JbmRleF07XG4gICAgICAgIGxldCBpbmRleCA9IHN0cmluZy5pbmRleE9mKGMsIHN0cmluZ0luZGV4KTtcbiAgICAgICAgbGV0IGJlc3QsIHRlbXA7XG5cbiAgICAgICAgd2hpbGUgKGluZGV4ID4gLTEpIHtcbiAgICAgICAgICAgIHBhdHRlcm5DYWNoZS5wdXNoKGluZGV4KTtcbiAgICAgICAgICAgIHRlbXAgPSB0aGlzLnRyYXZlcnNlKHN0cmluZywgcGF0dGVybiwgaW5kZXggKyAxLCBwYXR0ZXJuSW5kZXggKyAxLCBwYXR0ZXJuQ2FjaGUpO1xuICAgICAgICAgICAgcGF0dGVybkNhY2hlLnBvcCgpO1xuXG4gICAgICAgICAgICAvLyBpZiBkb3duc3RyZWFtIHRyYXZlcnNhbCBmYWlsZWQsIHJldHVybiBiZXN0IGFuc3dlciBzbyBmYXJcbiAgICAgICAgICAgIGlmICghdGVtcCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBiZXN0O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoIWJlc3QgfHwgYmVzdC5zY29yZSA8IHRlbXAuc2NvcmUpIHtcbiAgICAgICAgICAgICAgICBiZXN0ID0gdGVtcDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaW5kZXggPSBzdHJpbmcuaW5kZXhPZihjLCBpbmRleCArIDEpO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIGJlc3Q7XG4gICAgfVxuXG4gICAgY2FsY3VsYXRlU2NvcmUocGF0dGVybkNhY2hlKSB7XG4gICAgICAgIGxldCBzY29yZSA9IDA7XG4gICAgICAgIGxldCB0ZW1wID0gMTtcblxuICAgICAgICBwYXR0ZXJuQ2FjaGUuZm9yRWFjaCgoaW5kZXgsIGkpID0+IHtcbiAgICAgICAgICAgIGlmIChpID4gMCkge1xuICAgICAgICAgICAgICAgIGlmIChwYXR0ZXJuQ2FjaGVbaSAtIDFdICsgMSA9PT0gaW5kZXgpIHtcbiAgICAgICAgICAgICAgICAgICAgdGVtcCArPSB0ZW1wICsgMTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB0ZW1wID0gMTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHNjb3JlICs9IHRlbXA7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHJldHVybiBzY29yZTtcbiAgICB9XG5cbiAgICByZW5kZXIoc3RyaW5nLCBpbmRpY2VzLCBwcmUsIHBvc3QpIHtcbiAgICAgICAgdmFyIHJlbmRlcmVkID0gc3RyaW5nLnN1YnN0cmluZygwLCBpbmRpY2VzWzBdKTtcblxuICAgICAgICBpbmRpY2VzLmZvckVhY2goKGluZGV4LCBpKSA9PiB7XG4gICAgICAgICAgICByZW5kZXJlZCArPVxuICAgICAgICAgICAgICAgIHByZSArXG4gICAgICAgICAgICAgICAgc3RyaW5nW2luZGV4XSArXG4gICAgICAgICAgICAgICAgcG9zdCArXG4gICAgICAgICAgICAgICAgc3RyaW5nLnN1YnN0cmluZyhpbmRleCArIDEsIGluZGljZXNbaSArIDFdID8gaW5kaWNlc1tpICsgMV0gOiBzdHJpbmcubGVuZ3RoKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgcmV0dXJuIHJlbmRlcmVkO1xuICAgIH1cblxuICAgIGZpbHRlcihwYXR0ZXJuLCBhcnIsIG9wdHMpIHtcbiAgICAgICAgb3B0cyA9IG9wdHMgfHwge307XG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICBhcnJcbiAgICAgICAgICAgICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmVcbiAgICAgICAgICAgICAgICAucmVkdWNlKChwcmV2LCBlbGVtZW50LCBpZHgsIGFycikgPT4ge1xuICAgICAgICAgICAgICAgICAgICBsZXQgc3RyID0gZWxlbWVudDtcblxuICAgICAgICAgICAgICAgICAgICBpZiAob3B0cy5leHRyYWN0KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzdHIgPSBvcHRzLmV4dHJhY3QoZWxlbWVudCk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghc3RyKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gdGFrZSBjYXJlIG9mIHVuZGVmaW5lZHMgLyBudWxscyAvIGV0Yy5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdHIgPSBcIlwiO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgbGV0IHJlbmRlcmVkID0gdGhpcy5tYXRjaChwYXR0ZXJuLCBzdHIsIG9wdHMpO1xuXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZW5kZXJlZCAhPSBudWxsKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBwcmV2W3ByZXYubGVuZ3RoXSA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdHJpbmc6IHJlbmRlcmVkLnJlbmRlcmVkLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3JlOiByZW5kZXJlZC5zY29yZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbmRleDogaWR4LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9yaWdpbmFsOiBlbGVtZW50LFxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBwcmV2O1xuICAgICAgICAgICAgICAgIH0sIFtdKVxuXG4gICAgICAgICAgICAgICAgLnNvcnQoKGEsIGIpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgbGV0IGNvbXBhcmUgPSBiLnNjb3JlIC0gYS5zY29yZTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGNvbXBhcmUpIHJldHVybiBjb21wYXJlO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gYS5pbmRleCAtIGIuaW5kZXg7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgKTtcbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IFRyaWJ1dGVTZWFyY2g7XG4iLCIvKipcbiAqIFRyaWJ1dGUuanNcbiAqIE5hdGl2ZSBFUzYgSmF2YVNjcmlwdCBAbWVudGlvbiBQbHVnaW5cbiAqKi9cblxuaW1wb3J0IFRyaWJ1dGUgZnJvbSBcIi4vVHJpYnV0ZVwiO1xuXG5leHBvcnQgZGVmYXVsdCBUcmlidXRlO1xuIiwiaWYgKCFBcnJheS5wcm90b3R5cGUuZmluZCkge1xuICAgIEFycmF5LnByb3RvdHlwZS5maW5kID0gZnVuY3Rpb24ocHJlZGljYXRlKSB7XG4gICAgICAgIGlmICh0aGlzID09PSBudWxsKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQXJyYXkucHJvdG90eXBlLmZpbmQgY2FsbGVkIG9uIG51bGwgb3IgdW5kZWZpbmVkXCIpO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0eXBlb2YgcHJlZGljYXRlICE9PSBcImZ1bmN0aW9uXCIpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoXCJwcmVkaWNhdGUgbXVzdCBiZSBhIGZ1bmN0aW9uXCIpO1xuICAgICAgICB9XG4gICAgICAgIHZhciBsaXN0ID0gT2JqZWN0KHRoaXMpO1xuICAgICAgICB2YXIgbGVuZ3RoID0gbGlzdC5sZW5ndGggPj4+IDA7XG4gICAgICAgIHZhciB0aGlzQXJnID0gYXJndW1lbnRzWzFdO1xuICAgICAgICB2YXIgdmFsdWU7XG5cbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgdmFsdWUgPSBsaXN0W2ldO1xuICAgICAgICAgICAgaWYgKHByZWRpY2F0ZS5jYWxsKHRoaXNBcmcsIHZhbHVlLCBpLCBsaXN0KSkge1xuICAgICAgICAgICAgICAgIHJldHVybiB2YWx1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdW5kZWZpbmVkO1xuICAgIH07XG59XG5cbmlmICh3aW5kb3cgJiYgdHlwZW9mIHdpbmRvdy5DdXN0b21FdmVudCAhPT0gXCJmdW5jdGlvblwiKSB7XG4gICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lXG4gICAgZnVuY3Rpb24gQ3VzdG9tRXZlbnQoZXZlbnQsIHBhcmFtcykge1xuICAgICAgICBwYXJhbXMgPSBwYXJhbXMgfHwge1xuICAgICAgICAgICAgYnViYmxlczogZmFsc2UsXG4gICAgICAgICAgICBjYW5jZWxhYmxlOiBmYWxzZSxcbiAgICAgICAgICAgIGRldGFpbDogdW5kZWZpbmVkLFxuICAgICAgICB9O1xuICAgICAgICB2YXIgZXZ0ID0gZG9jdW1lbnQuY3JlYXRlRXZlbnQoXCJDdXN0b21FdmVudFwiKTtcbiAgICAgICAgZXZ0LmluaXRDdXN0b21FdmVudChldmVudCwgcGFyYW1zLmJ1YmJsZXMsIHBhcmFtcy5jYW5jZWxhYmxlLCBwYXJhbXMuZGV0YWlsKTtcbiAgICAgICAgcmV0dXJuIGV2dDtcbiAgICB9XG5cbiAgICBpZiAodHlwZW9mIHdpbmRvdy5FdmVudCAhPT0gXCJ1bmRlZmluZWRcIikge1xuICAgICAgICBDdXN0b21FdmVudC5wcm90b3R5cGUgPSB3aW5kb3cuRXZlbnQucHJvdG90eXBlO1xuICAgIH1cblxuICAgIHdpbmRvdy5DdXN0b21FdmVudCA9IEN1c3RvbUV2ZW50O1xufVxuIl19
